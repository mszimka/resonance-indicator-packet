qmake build.pro
make -j8 2>build.err
make
mkdir -p run/libs
find libs -name "*.so*" -exec cp {} run/libs \;
cp build/qt4/unix/apps/resonance/indicator/release/dest/indicator run

echo "#!/bin/bash

mount -t nfs4 -o timeo=20,retrans=2 nfsvers=42 192.1.1.1:/ /opt/server &

echo \"core\" > /proc/sys/kernel/core_pattern
cat /proc/sys/kernel/core_pattern
ulimit -c unlimited
ulimit -a | grep core

export OWS_READ_ONLY=false
export OWS_DEPLOYMENT=false
export LD_LIBRARY_PATH=\`pwd\`/libs

X &
restart=1
while [ \$restart -ne 0 ]
do
    DISPLAY=0:0 ./indicator 2>indicator.err
    restart=\$?
    if [ \$restart -ne 0 ];
    then
        gdb indicator core -x gdb.cmd
        dir=\"\$(date '+%Y-%m-%d_%H:%M:%S.crash')\"
        mkdir \$dir
        mv indicator.gdb \$dir
        mv indicator.log \$dir
        mv indicator.err \$dir
    fi
    sleep 1
done

apcupsd -o
poweroff
" > run/start.sh
chmod +x run/start.sh

echo "set logging file indicator.gdb
set confirm off
set logging overwrite on
set logging redirect on
set logging on

echo *****thread 1 backtrace*****\n
thread apply 1 bt

echo \n*****frame 0*****\n
frame 0
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 1*****\n
frame 1
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 2*****\n
frame 2
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 3*****\n
frame 3
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 4*****\n
frame 4
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 5*****\n
frame 5
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 6*****\n
frame 6
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 7*****\n
frame 7
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 8*****\n
frame 8
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 9*****\n
frame 9
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo *****thread 2 backtrace*****\n
thread apply 2 bt

echo \n*****frame 0*****\n
frame 0
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 1*****\n
frame 1
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 2*****\n
frame 2
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 3*****\n
frame 3
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 4*****\n
frame 4
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 5*****\n
frame 5
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 6*****\n
frame 6
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 7*****\n
frame 7
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 8*****\n
frame 8
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

echo \n*****frame 9*****\n
frame 9
echo \n-----locals-----\n
info locals
echo \n-----args-----\n
info args

quit
" > run/gdb.cmd
