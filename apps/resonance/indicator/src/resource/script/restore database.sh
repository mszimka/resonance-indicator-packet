#!/bin/bash

set -e

DB_PATH=${1}

echo "Checking of the parameters..."
if [ ${#} != 1 ] || [ -z "\$DB_PATH" ]
then
    echo "Wrong number, or meanings of the parametrers. Exit!"
    exit 1
fi

# Останавливаем программу проигрыша или любую другую, которая использует базу #############################
PS=`ps aux | grep bsl | grep -v grep | awk '{print $2}'`
for P in "$PS" ; do
        echo "$P"
done

if [ "$P" != "" ]
then
   echo "Stop programs, using database."
   killall -9 bsl
fi

#############################################################################################################
# 1) в папке /home должен лежать скрипт для создания индексов
# 2) Пути в Убунту и Оле к папкам Postgres
echo "Create new database and restore the backup..."
sudo -i chmod 777 ${DB_PATH}
sudo -u postgres /usr/local/pgsql/bin/psql -Upostgres -dpostgres -p5432 -c "DROP DATABASE IF EXISTS new_db;"
sudo -u postgres /usr/local/pgsql/bin/psql -Upostgres -dpostgres -p5432 -c "DROP DATABASE IF EXISTS rezonans_db;"
sudo -u postgres /usr/local/pgsql/bin/pg_restore -Upostgres -dpostgres -p5432 -hlocalhost -C -v ${DB_PATH}
sudo -u postgres /usr/local/pgsql/bin/psql -Upostgres -dpostgres -p5432 -c "ALTER DATABASE new_db RENAME TO rezonans_db;"
echo "Create indexes..."
sudo -u postgres /usr/local/pgsql/bin/psql -Upostgres -drezonans_db -p5432 -f /home/cr_index.sql

#  Запускаем программу проигрыша после распаковки бэкапа ###################################################
echo "Run connection to database."
sudo -i /home/SQL
echo "Success."
exit 0
