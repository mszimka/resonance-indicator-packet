source="/opt/qt/5.7.1/5.7/gcc_64/bin"
$source/lconvert -i indicator.en.ts -i gui.en.ts -i tools.en.ts -i communication.en.ts -i plugins.en.ts -o english.ts
$source/lrelease english.ts english.qm

$source/lconvert -i indicator.fr.ts -i gui.fr.ts -i tools.fr.ts -i communication.fr.ts -i plugins.fr.ts -i qt_fr.ts -o french.ts
$source/lrelease french.ts french.qm
