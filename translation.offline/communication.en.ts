<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name>InterfaceController</name>
    <message>
        <location filename="../libs/communication/src/controller/interface_controller.cpp" line="95"/>
        <source>получено %1 %2, отправлено %3 %4</source>
        <translation>received %1 %2, transmitted %3 %4</translation>
    </message>
</context>
<context>
    <name>InterfaceIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="91"/>
        <source>Мб</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="94"/>
        <source>Кб</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="96"/>
        <source>байт</source>
        <translation>B</translation>
    </message>
</context>
<context>
    <name>TcpController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="41"/>
        <source>подключение к %1:%2...</source>
        <translation>connection to %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="54"/>
        <source>соединение с %1:%2 не установлено</source>
        <translation>connection to %1:%2 not established</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="57"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connection to %1:%2 broken</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="62"/>
        <source> по ошибке &apos;%1&apos;</source>
        <translation>by mistake %1</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="70"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connection to %1:%2 OK</translation>
    </message>
</context>
<context>
    <name>TcpIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="27"/>
        <source>Удаленный хост</source>
        <translation>Remote host</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="36"/>
        <source>Удаленный порт</source>
        <translation>Remote port</translation>
    </message>
</context>
<context>
    <name>TcpServerController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="34"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="84"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="103"/>
        <source>ожидание соединения по %1:%2...</source>
        <translation>connection waiting via %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="50"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connection to %1:%2 OK</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="73"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connectionh to %1:%2 broken </translation>
    </message>
</context>
<context>
    <name>UdpController</name>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="27"/>
        <source>привязка к %1:%2 выполнена</source>
        <translation>binding to %1:%2 performed</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="29"/>
        <source>ошибка привязки к %1:%2 - %3</source>
        <translation>binding error to%1:%2 - %3 </translation>
    </message>
</context>
</TS>
