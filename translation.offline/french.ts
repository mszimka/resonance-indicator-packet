<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="ru">
<context>
    <name>Data::Target</name>
    <message>
        <location filename="../apps/resonance/indicator/src/data/target_data.cpp" line="105"/>
        <source>трасса %1 сброшена по таймауту</source>
        <translation>trajectoire %1 est  remise à zéro pour dépassement de temps imparti</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Bar</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="78"/>
        <source>Назад</source>
        <translation>En avant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="80"/>
        <source>Вперед</source>
        <translation>En arrière</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="82"/>
        <source>Домой</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="90"/>
        <source>Общее
состояние</source>
        <translation>État général</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="91"/>
        <source>Состояние
УМ</source>
        <translation>État de l&apos;amplificateur d&apos;émission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="93"/>
        <source>Краткое состояние
приемников</source>
        <translation>État des récepteurs (en bref)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="95"/>
        <source>Полное состояние
приемников</source>
        <translation>État des récepteurs (complet)</translation>
    </message>
</context>
<context>
    <name>Diagnostic::BaseScene</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/base_diagnostic_scene.cpp" line="53"/>
        <source>Сектор%1</source>
        <translation>Secteur%1</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::AnalogGeneral</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="19"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="33"/>
        <source>ЦОС%1</source>
        <translation>TNS%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="44"/>
        <source>УМ%1</source>
        <translation>AE%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="49"/>
        <source>Приемник%1 (f1)</source>
        <translation>Récepteur%1 (f1)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="52"/>
        <source>Антенна</source>
        <translation>Antenne</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="55"/>
        <source>Антенна Az</source>
        <translation>Antenne Az</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="60"/>
        <source>Приемник%1 (f2)</source>
        <translation>Récepteur%1 (f2)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="66"/>
        <source>Антенна E</source>
        <translation>Antenne Gis</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Antenna::Calibration</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="58"/>
        <source>Канал</source>
        <translation>Voie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="59"/>
        <source>До калибровки</source>
        <translation>Avant étalonnage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="60"/>
        <source>После калибровки</source>
        <translation>Après étalonnage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>Шум, дБ</source>
        <translation>Bruit, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Амплитуда, дБ</source>
        <translation>Amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Фаза, гр</source>
        <translation>Phase, degrés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>СКО ампл., дБ</source>
        <translatorcomment>для СКО можно использовать сокращение EMQ</translatorcomment>
        <translation>Erreur moyenne quadratique de l&apos;amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>СКО фазы, гр</source>
        <translatorcomment>EMQ</translatorcomment>
        <translation>Erreur moyenne quadratique de la phase, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>АФЮС ампл., дБ</source>
        <translatorcomment>OASA</translatorcomment>
        <translation>Oscillateur  autonome de signal d&apos;ajustement d&apos;amplitude, dB </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>АФЮС фаза., гр</source>
        <translatorcomment>OASA</translatorcomment>
        <translation>Oscillateur autonome de signal d&apos;ajustement de phase, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон ампл., дБ</source>
        <translation>Mesure-étalon d&apos;amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон фазы., гр</source>
        <translation>Mesure-étalon de phase, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка ампл., дБ</source>
        <translation>Correction amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка фазы., гр</source>
        <translation>Correction phase, degré</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Antenna::Requester</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="57"/>
        <source>Направление</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="67"/>
        <source>Тип</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="69"/>
        <source>Азимутальная</source>
        <translatorcomment>в зависимости от рода определяемого существительного, может быть и Azimutale</translatorcomment>
        <translation>Azimutal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="69"/>
        <source>Угломестная</source>
        <translatorcomment>должно стоять после определяемого существительного</translatorcomment>
        <translation>de site</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="71"/>
        <source>Контроль по</source>
        <translation>Contôle par</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="73"/>
        <source>ИС</source>
        <translatorcomment>IS</translatorcomment>
        <translation>Impulsion simulée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="73"/>
        <source>АФЮС</source>
        <translatorcomment>OASA</translatorcomment>
        <translation>Oscillateur  autonome de signal d&apos;ajustement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="75"/>
        <source>Частота, МГц</source>
        <translation>Fréquence, MHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="106"/>
        <source>Рассчитать</source>
        <translation>Calculer</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::BaseGeneral</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="80"/>
        <source>РМО1</source>
        <translation>PTO1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="81"/>
        <source>РМО2</source>
        <translation>PTO2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="82"/>
        <source>ВРМО</source>
        <translation>Console déportée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="91"/>
        <source>Сервер</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="94"/>
        <source>ИБП1</source>
        <translation>Onduleur1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="95"/>
        <source>ИБП2</source>
        <translation>Onduleur2</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::CommandPostCommunication</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="87"/>
        <source>Сопряжение с КП%1</source>
        <translatorcomment>PC = poste de commandement</translatorcomment>
        <translation>Interfaçage avec le PC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="102"/>
        <source>Состояние АПД</source>
        <translatorcomment>État des ETD</translatorcomment>
        <translation>État des équipements de transmission des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="103"/>
        <source>Готовность</source>
        <translation>Disponibilité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="106"/>
        <source>Физическое
соединение</source>
        <translation>Connexion physique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="107"/>
        <source>Логическое
соединение</source>
        <translation>Connexion logique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="94"/>
        <source>Состояние связи</source>
        <translation>État de liaison</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="95"/>
        <source>Командир</source>
        <translation>Commandant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="96"/>
        <source>Инициализация
передачи</source>
        <translation>Initialisation de transmission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="97"/>
        <source>Прием
сообщений</source>
        <translation>Réception des messages</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="170"/>
        <source>Синхронизация
времени</source>
        <translation>Synchronisation du temps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="99"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="152"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="172"/>
        <source>Доставка
сообщений</source>
        <translation>Acheminement des communications</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="105"/>
        <source>Прием
данных</source>
        <translation>Réception des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="104"/>
        <source>Передача
данных</source>
        <translation>Transmission des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="157"/>
        <source>Тип протокола: %1</source>
        <translation>Type de protocole: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="158"/>
        <source>Принято пакетов: %1</source>
        <translation>Paquets récus : %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="159"/>
        <source>Ошибок приема: %1</source>
        <translation>Erreurs de réception: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="160"/>
        <source>Выдано пакетов: %1</source>
        <translation>Paquets transmis: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="161"/>
        <source>Ошибок выдачи: %1</source>
        <translation>Erreurs de transmission: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="162"/>
        <source>Просрочено пакетов: %1</source>
        <translation>Paquets surannés: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="163"/>
        <source>Задержка: %1 мс</source>
        <translation>Délai: %1 мс</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::General</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="37"/>
        <source>АПД1</source>
        <translation>ETD1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="66"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="38"/>
        <source>АПД2</source>
        <translation>ETD2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="73"/>
        <source>Приемник%1</source>
        <translation>Récepteur%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="55"/>
        <source>УМ%1</source>
        <translation>AE%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="50"/>
        <source>Антенна</source>
        <translation>Antenne</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="64"/>
        <source>Антенна Az</source>
        <translation>Antenne Az</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="67"/>
        <source>Антенна E</source>
        <translation>Antenne Gis</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/russia_general_diagnostic_explorer.cpp" line="24"/>
        <source>Сообщения об отказах</source>
        <translation>Message de défauts</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="30"/>
        <source>РМО1</source>
        <translation>PTO1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="31"/>
        <source>РМО2</source>
        <translation>PTO2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="32"/>
        <source>ВРМО</source>
        <translation>Console déportée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="33"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="31"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="34"/>
        <source>Сервер</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="32"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="35"/>
        <source>ИБП1</source>
        <translation>Onduleur1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="36"/>
        <source>ИБП2</source>
        <translation>Onduleur2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="39"/>
        <source>АПД3</source>
        <translation>ETD3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="48"/>
        <source>ЦОС%1</source>
        <translation>TNS%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="50"/>
        <source>Эквивалент</source>
        <translation>Équivalent</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="58"/>
        <source>Приемник%1 (f1)</source>
        <translation>Récepteur%1 (f1)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="61"/>
        <source>Приемник%1 (f2)</source>
        <translation>Récepteur%1 (f2)</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Reciever::Calibration</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="141"/>
        <source>Приемник f1</source>
        <translation>Récepteur f1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="85"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="142"/>
        <source>Приемник f2</source>
        <translation>Récepteur f2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="87"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="146"/>
        <source>Азимутальные каналы</source>
        <translation>Voies azimutales</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="147"/>
        <source>Угломестные каналы</source>
        <translation>Voies en site</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <source>Внутр. шум дБ</source>
        <translation>Bruit propre, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <source>Внутр. ампл. дБ</source>
        <translation>Amplitude interne, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <source>Внутр. фаза гр</source>
        <translation>Phase propre, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="92"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="151"/>
        <source>Внеш. шум дБ</source>
        <translation>Bruit ambiant, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="92"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="151"/>
        <source>Внеш. ампл. дБ</source>
        <translation>Amplitude externe, dB</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Reciever::Tech</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="73"/>
        <source>Мнемосхема шкафа приемника сектора %1 РЭМ 3 5ЦП1-01</source>
        <translation>Diagramme mnémonique du rack du récepteur du secteur %1 РЭМ 3 5ЦП1-01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="95"/>
        <source>АА%1
5ЦП101Б%2</source>
        <translatorcomment>смысл сокращения &quot;АА&quot; НЕ ЯСЕН - необходимо УТОЧНИТЬ!</translatorcomment>
        <translation>АА%1
5ЦП101Б%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="99"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="100"/>
        <source>Плата управления АА2</source>
        <translation>Carte de commande AA2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="104"/>
        <source>АА%1
Узел питания %2</source>
        <translatorcomment>AA - ???</translatorcomment>
        <translation>АА%1
Bloc d&apos;alimentation %2</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Transmitter</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="94"/>
        <source>Состояние усилителя мощности сектора %1</source>
        <translation>État de l&apos;amplificateur d&apos;émission pour le secteur %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="103"/>
        <source>Техническое состояние ТЭЗ УМ</source>
        <translatorcomment>AE = amplificateur d&apos;émission</translatorcomment>
        <translation>État technique du bloc de réserve de l&apos;AE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>5Ц300Я01</source>
        <translation>5Ц300Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>БКУ 5Ц301Б03</source>
        <translatorcomment>BCC = Bloc de côntrole et commande</translatorcomment>
        <translation>BCC 5Ц301Б03</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>БУМ 5Ц301Б01 (верх)</source>
        <translatorcomment>BAE =Bloc d&apos;amplification d&apos;émission</translatorcomment>
        <translation>BAE 5Ц301Б01 (haut)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>БУМ 5Ц301Б01 (низ)</source>
        <translatorcomment>BAE =Bloc d&apos;amplification d&apos;émission</translatorcomment>
        <translation>BAE 5Ц301Б01 (bas)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>БСДНО 5Ц301Б02</source>
        <translatorcomment>BDSCD= Bloc de division et sommation du coupleur directif </translatorcomment>
        <translation>BDSCD 5Ц301Б02</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>БП 5Ц301Б05</source>
        <translation>БП 5Ц301Б05</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>БФГ 5Ц301Б04</source>
        <translatorcomment>BFAH =  Bloc de filtre antiharmonique</translatorcomment>
        <translation>BFAH 5Ц301Б04</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>5Ц301Я01</source>
        <translation>5Ц301Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="109"/>
        <source>Узел распределения пит.</source>
        <translation>Unité de distribution alim.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="109"/>
        <source>Узел воздушного охл.</source>
        <translation>Unité de refroidissement par air.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="113"/>
        <source>%1 канал</source>
        <translatorcomment>возможный вариант - canal </translatorcomment>
        <translation>%1 voie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="117"/>
        <source>%1А%2</source>
        <translation>%1А%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>1А8</source>
        <translation>1А8</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>1А11</source>
        <translation>1A11</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>1А7</source>
        <translation>1A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>2А7</source>
        <translation>2A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="128"/>
        <source>Функциональное состояние УМ</source>
        <translation>État fonctionnel de l&apos;AE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="129"/>
        <source>Шкаф 1</source>
        <translation>Armoire 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="130"/>
        <source>Шкаф 2</source>
        <translation>Armoire 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Управление</source>
        <translation>Commande</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Питание</source>
        <translation>Alimentation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Излучение ЗС1</source>
        <translatorcomment>ID = Impulsion de départ</translatorcomment>
        <translation>Émission de l&apos;ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Излучение ЗС2</source>
        <translation>Émission de l&apos;ID2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>Калибр. ЗС1</source>
        <translation>Étallonage de l&apos;ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>Калибр. ЗС2</source>
        <translation>Étallonage de l&apos;ID2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>Нагрузка</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>ПЭП</source>
        <translation>Commutation de sous-gammes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="135"/>
        <source>Фаза ЗС1</source>
        <translation>Phase de l&apos;ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="135"/>
        <source>Фаза ЗС2</source>
        <translation>Phase de l&apos;ID2</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::VrlState</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="58"/>
        <source>Состояние ВРЛ</source>
        <translation>État de IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="60"/>
        <source>Источники питания</source>
        <translation>Alimentateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="61"/>
        <source>Панель
 автоматических
 выключателей</source>
        <translation>Tableau
 des disjoncteurs 
automatiques</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="61"/>
        <source>Вентиляторы</source>
        <translation>Ventilateurs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="95"/>
        <source>41М1.6
(6110)</source>
        <translation>41М1.6
(6110)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="97"/>
        <source>KIR-1A</source>
        <translation>KIR-1A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="42"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="62"/>
        <source>СВ КР-02
 Комплект 1</source>
        <translation>Calculatrice spéciale КR-02
 Jeu 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="43"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="62"/>
        <source>СВ КР-02
 Комплект 2</source>
        <translation>Calculatrice spéciale КR-02
 Jeu 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>124ПП02
 Комплект 1</source>
        <translation>124ПП02
 Jeu 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>124ПП02
 Комплект 2</source>
        <translation>124ПП02
 Jeu 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>ЗГ-1
 Комплект 1</source>
        <translatorcomment>OP = oscillateur pilote</translatorcomment>
        <translation>OP-1
 Jeu 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="64"/>
        <source>ЗГ-2
 Комплект 2</source>
        <translatorcomment>OP = oscillateur pilote</translatorcomment>
        <translation>OP-2
Jeu 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="67"/>
        <source>ВУМ %1</source>
        <translatorcomment>APT = amplificateur d&apos;émission terminal</translatorcomment>
        <translation>APT %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="70"/>
        <source>Перекл.
 СВЧ</source>
        <translation>Commutateur
 HF</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Scene</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/algeria_diagnostic_scene.cpp" line="21"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/egypt_diagnostic_scene.cpp" line="31"/>
        <source>Сектор%1</source>
        <translation>Secteur%1</translation>
    </message>
</context>
<context>
    <name>Graphics::Controller::Vrl::Manip</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="15"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="16"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="35"/>
        <source>Манип</source>
        <translation>Interr. manuel</translation>
    </message>
</context>
<context>
    <name>Graphics::SceneItem::Grid::SphericalLabel::Text</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/grid/spherical_grid_label_scene_item.cpp" line="29"/>
        <source>Внимание!</source>
        <translation>Attention !</translation>
    </message>
</context>
<context>
    <name>Graphics::View</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="292"/>
        <source>ОТКАЗ</source>
        <translation>REFUS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="295"/>
        <source>ГОТОВ К БОЕВОЙ РАБОТЕ</source>
        <translation>PRÊT AU FONCTIONNEMENT OPÉRATIONNEL</translation>
    </message>
</context>
<context>
    <name>Gui::Dialog::GroundObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="53"/>
        <source>Применить</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="55"/>
        <source>Отмена</source>
        <translation>Annulation</translation>
    </message>
</context>
<context>
    <name>Gui::Dialog::Monitoring</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="38"/>
        <source>Вставьте USB диск...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="239"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="307"/>
        <source>Нет данных</source>
        <translation type="unfinished">Données non disponibles</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="93"/>
        <source>Ошибка монтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="96"/>
        <source>Ошибка копирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="104"/>
        <source>Ошибка размонтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="119"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="200"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="214"/>
        <source>Обработка</source>
        <translation type="unfinished">Traitement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="170"/>
        <source>Подготовка файлов...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="175"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="301"/>
        <source>скриншоты</source>
        <translation type="unfinished">captures d&apos;écran</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="176"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="303"/>
        <source>переговоры</source>
        <translation type="unfinished">conversations</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="177"/>
        <source>базы данных</source>
        <translation type="unfinished">bases de données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="243"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="308"/>
        <source>Выбрать все</source>
        <translation type="unfinished">Sélectionner tout</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="244"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="309"/>
        <source>Отменить все</source>
        <translation type="unfinished">Annuler tout</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="245"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="310"/>
        <source>Обновить</source>
        <translation type="unfinished">Rafraîchir</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="260"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="317"/>
        <source>За время</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="265"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="315"/>
        <source>От</source>
        <translation type="unfinished">de</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="266"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="316"/>
        <source>До</source>
        <translation type="unfinished">à</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="275"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="311"/>
        <source>Печать</source>
        <translation type="unfinished">Imprimer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="277"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="312"/>
        <source>Удалить</source>
        <translation type="unfinished">Supprimer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="279"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="313"/>
        <source>Копировать на USB диск</source>
        <translation type="unfinished">Copier sur le disque USB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="282"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="314"/>
        <source>Выход</source>
        <translation type="unfinished">Sortie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="305"/>
        <source>база данных</source>
        <translation type="unfinished">base de données</translation>
    </message>
</context>
<context>
    <name>Gui::Info</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="92"/>
        <source>АЗ: %1</source>
        <translatorcomment>AA = Acquisition automatique</translatorcomment>
        <translation>AA: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="93"/>
        <source>АС: %1</source>
        <translatorcomment>PA = poursuite autoimatique</translatorcomment>
        <translation>PA: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="94"/>
        <source>КТ: %1</source>
        <translatorcomment>PCd = point de coordonnées</translatorcomment>
        <translation>PCd: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="95"/>
        <source>РЕГ: %1</source>
        <translation>ENREG: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="100"/>
        <source>ИЗЛ:</source>
        <translation>EMISSION:</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="106"/>
        <source>КТ%1: %2</source>
        <translation>PCd%1: %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="116"/>
        <source>НзО: %1</source>
        <translatorcomment>ObT = Objet terrestre</translatorcomment>
        <translation>ObT: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="107"/>
        <source>БАЗ: %1</source>
        <translatorcomment>SAA = Suppression de l&apos;acquisition automatique</translatorcomment>
        <translation>SAA: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="131"/>
        <source>СЗИ: %1</source>
        <translatorcomment>Secteur de suppression d&apos;émission</translatorcomment>
        <translation>SSE: %1</translation>
    </message>
</context>
<context>
    <name>Gui::Tabs::Function::Registration</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="322"/>
        <source>От</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="324"/>
        <source>До</source>
        <translation>à</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="334"/>
        <source>Точка падения баллистической цели</source>
        <translation>Point d&apos;impact de cible ballistique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="336"/>
        <source>Экстраполяционные точки</source>
        <translation>Points d&apos;extrapolation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="341"/>
        <source>Выход комплекса HLCP</source>
        <translation>Sortie du complexe  HLCP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="343"/>
        <source>Контроль функционирования</source>
        <translation>Contrôle du fonctionnement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="344"/>
        <source>Координатные точки</source>
        <translation>Points de coordonnées</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="345"/>
        <source>ЭТ с выхода ВОИ</source>
        <translatorcomment>PE = points d&apos;extrapolation</translatorcomment>
        <translation>PE depuis la sortie du traitement secondaire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="350"/>
        <source>Запросы на опознавание РЛО RBS</source>
        <translation>Demandes de l&apos;identification radar RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="351"/>
        <source>Отметка опознавания РЛО RBS</source>
        <translation>Marquer de l&apos;identification radar RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="353"/>
        <source>Команды оператора</source>
        <translation>Commandes de l&apos;opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="361"/>
        <source>Обмен с КСА</source>
        <translation>Échanges avec les automatismes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="363"/>
        <source>Плотность помех по частоте</source>
        <translation>Densité des brouillages sur la fréquence</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="83"/>
        <source>Восстановление БД</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="375"/>
        <source>Анализ БД</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="122"/>
        <source>Ошибка монтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="125"/>
        <source>Ошибка копирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="133"/>
        <source>Ошибка размонтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="186"/>
        <source>Выберите архив БД</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="186"/>
        <source>Архивы БД (*backup)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="193"/>
        <source>Распаковка архива БД...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="80"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="162"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="384"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="460"/>
        <source>Воспроизведение</source>
        <translation>Lecture</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="81"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="387"/>
        <source>Следующий обзор</source>
        <translation>Balayage suivant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="82"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="390"/>
        <source>Стоп</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="278"/>
        <source>Внимание!</source>
        <translation>Attention !</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="278"/>
        <source>Время начала больше времени окончания</source>
        <translation>Temps du début est supérieur du temps de la fin</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="287"/>
        <source>Ошибка</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="287"/>
        <source>Не выбрано ни одной таблицы данных</source>
        <translation>Aucune table de données n&apos;est choisie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="338"/>
        <source>Выход комплекса Фундамент1,2</source>
        <translation>Sortie du complexe Fondation 1,2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="347"/>
        <source>Запросы на опознавание РЛО MkXA</source>
        <translatorcomment>Не  MkXII ?</translatorcomment>
        <translation>Demandes de l&apos;identification radar MkXA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="348"/>
        <source>Отметка опознавания РЛО MkXA</source>
        <translation>Marquer de l&apos;identification MkXA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="355"/>
        <source>Запросы на опознавание Пароль</source>
        <translation>Demande de l&apos;identification Mot de passe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="356"/>
        <source>Отметка опознавания Пароль</source>
        <translation>Marquer de l&apos;identification Mot de passe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="374"/>
        <source>Востановить БД</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="417"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="451"/>
        <source>Пауза</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="442"/>
        <source>Продолжить</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="478"/>
        <source>Предупреждение</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="479"/>
        <source>Допустимый интервал времени от %1 до %2</source>
        <translation>Domaine de temps admissible de %1 à %2</translation>
    </message>
</context>
<context>
    <name>Gui::Tabs::Route</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="134"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="308"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="462"/>
        <source>Ед.№</source>
        <translation>No uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="135"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="309"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="463"/>
        <source>К</source>
        <translatorcomment>?????</translatorcomment>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="138"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="312"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="466"/>
        <source>РКЦ</source>
        <translatorcomment>ICC</translatorcomment>
        <translation>Identification de classe de la cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="139"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="467"/>
        <source>Ампл.дБ
Код обн.</source>
        <translation>Ampl.dB
Code raffr.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="144"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="318"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="472"/>
        <source>№
№
t,мм:сс</source>
        <translation>№
№
t,mm:ss</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="495"/>
        <source>Все</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="154"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="496"/>
        <source>Селекция ИКО</source>
        <translation>Sélection de l&apos;indicateur panoramique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="155"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="507"/>
        <source>Класс</source>
        <translation>Classe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="162"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="526"/>
        <source>Источник</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="548"/>
        <source>Коды ВРЛ</source>
        <translation>Codes IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="553"/>
        <source>Не отв.</source>
        <translation>Pas de réps.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="577"/>
        <source>Критерий</source>
        <translation>Critère</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>Новые</source>
        <translatorcomment>или Nouvelles (если определяемое существительное - женского рода)</translatorcomment>
        <translation>Nouveaux</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>Навед</source>
        <translatorcomment>если имеется в виду &quot;наведение&quot;.</translatorcomment>
        <translation>Guidage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>Цель</source>
        <translation>Cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>КП</source>
        <translatorcomment>если имеется в виду &quot;командный пункт&quot;</translatorcomment>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="582"/>
        <source>Запрет</source>
        <translation>Interdiction</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="173"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="593"/>
        <source>Азимут</source>
        <translation>Azimut</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="174"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="602"/>
        <source>Дальность</source>
        <translation>Portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="179"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="412"/>
        <source>Формуляры</source>
        <translation>Formulaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="413"/>
        <source>Наведение</source>
        <translation>Guidage</translation>
    </message>
</context>
<context>
    <name>Gui::Tabs::Settings::General</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="431"/>
        <source>Метрическая</source>
        <translation>Métrique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="431"/>
        <source>Англо-американская</source>
        <translation>Anglo-américaine</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="213"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="525"/>
        <source>Полярные</source>
        <translation>Polaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="213"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="526"/>
        <source>Декартовы</source>
        <translation>Cartésiennes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="213"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="527"/>
        <source>Географические</source>
        <translation>Géographiques</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="217"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="412"/>
        <source>Язык интерфейса</source>
        <translation>Langue de l&apos;interface</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="217"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="429"/>
        <source>Система измерения</source>
        <translation>Système de mesure</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="218"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="435"/>
        <source>Высота</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="218"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="435"/>
        <source>Дальность</source>
        <translation>Distance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="218"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="435"/>
        <source>Скорость</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="523"/>
        <source>Координаты маркера</source>
        <translation>Coordonnées du marqueur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="293"/>
        <source>Графика</source>
        <translation>Graphique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="309"/>
        <source>Сетка</source>
        <translation>Grille</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="317"/>
        <source>КТ</source>
        <translatorcomment>PCd</translatorcomment>
        <translation>Point de coordonnées</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="327"/>
        <source>ЭТ</source>
        <translatorcomment>PEx</translatorcomment>
        <translation>Point d&apos;extrapolation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="337"/>
        <source>Статика</source>
        <translation>Statique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="347"/>
        <source>Карты</source>
        <translation>Cartes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="269"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="294"/>
        <source>Длина траекторий</source>
        <translation>Longueur des trajectoires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="270"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="307"/>
        <source>Яркость</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="208"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="362"/>
        <source>Непрозрачность</source>
        <translation>Opacité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="364"/>
        <source>Формуляры</source>
        <translation>Formulaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="529"/>
        <source>Сетка ПВО</source>
        <translation>Grille DCA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="530"/>
        <source>WGS-84</source>
        <translation>WGS-84</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="531"/>
        <source>ПЗ-90</source>
        <translatorcomment>????</translatorcomment>
        <translation>ПЗ-90</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="534"/>
        <source>Эллипсоид</source>
        <translation>Ellipsoïde</translation>
    </message>
</context>
<context>
    <name>Gui::Widget::DateTime</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="49"/>
        <source>Дата</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="52"/>
        <source>Время</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="59"/>
        <source>Применить</source>
        <translation>Appliquer</translation>
    </message>
</context>
<context>
    <name>Gui::Widget::GroundObjectCoordinate</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="36"/>
        <source>Широта</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="38"/>
        <source>Долгота</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="42"/>
        <source>Применить</source>
        <translation>Aplliquer</translation>
    </message>
</context>
<context>
    <name>Gui::Widget::Zz</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/zz_widget_gui.cpp" line="23"/>
        <source>Применить</source>
        <translation>Appliquer</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="95"/>
        <source>Рабочее место оператора</source>
        <translation>Poste de travail de l&apos;opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="137"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="533"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1682"/>
        <source>ИКО</source>
        <translatorcomment>Indicateur panoramique</translatorcomment>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="140"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="533"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="732"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1682"/>
        <source>АФК</source>
        <translatorcomment>Contrôle de fonctionnement automatisé</translatorcomment>
        <translation>CFA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="408"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1154"/>
        <source>Сетка ПВО</source>
        <translation>Grille DCA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="498"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1122"/>
        <source>Полярные координаты</source>
        <translation>Coordonnées polaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="498"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1123"/>
        <source>Декартовы координаты</source>
        <translation>Coordonnées cartésiennes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="498"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1124"/>
        <source>Географические координаты</source>
        <translation>Coordonnées géographiques</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="504"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="734"/>
        <source>КИ</source>
        <translatorcomment>Indicateur de contrôle</translatorcomment>
        <translation>IC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="506"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="744"/>
        <source>КТ</source>
        <translatorcomment>Point de coordonnées</translatorcomment>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="506"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="753"/>
        <source>ЭТ</source>
        <translatorcomment>Point d&apos;extrapolation</translatorcomment>
        <translation>PEx</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="508"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="764"/>
        <source>Луч</source>
        <translation>Faisceau</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="510"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="766"/>
        <source>След</source>
        <translation>Trace</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="510"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="767"/>
        <source>ФОРМ</source>
        <translatorcomment>если имеется в виду ФОРМУЛЯР</translatorcomment>
        <translation>FICHE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="514"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="781"/>
        <source>Объекты</source>
        <translation>Objets</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="516"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1144"/>
        <source>Статика</source>
        <translation>Statique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="517"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1189"/>
        <source>Карты</source>
        <translation>Cartes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1148"/>
        <source>Сетка АД</source>
        <translatorcomment>Azimut de portée</translatorcomment>
        <translation>Grille AzP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1163"/>
        <source>Сектора обзора</source>
        <translation>Secteurs de balayage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1164"/>
        <source>Рубежи</source>
        <translation>Limites</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1165"/>
        <source>Зоны отбора</source>
        <translation>Zones de sélection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="520"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1166"/>
        <source>Наземные объекты</source>
        <translation>Objets terrestres</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="520"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1167"/>
        <source>Бланки автозахвата</source>
        <translation>Fiche de l&apos;acquisition automatique de cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="522"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1172"/>
        <source>Сектора ЗИ ВРЛ</source>
        <translatorcomment>Interdiction d&apos;émission</translatorcomment>
        <translation>Secteurs IE IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="524"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1175"/>
        <source>Точки падения БЦ</source>
        <translatorcomment>Cible balistique</translatorcomment>
        <translation>Points d&apos;impact de la CB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="530"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="783"/>
        <source>Селекция</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="536"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="538"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="814"/>
        <source>КП%1</source>
        <translation>PCdt%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="538"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="684"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="803"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1366"/>
        <source>КП: ?</source>
        <translation>PCdt: ?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="540"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="916"/>
        <source>Мирное время</source>
        <translation>Temps de paix</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="540"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="923"/>
        <source>Военное время</source>
        <translation>Temps de guerre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="545"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="792"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1462"/>
        <source>МВ</source>
        <translation>TP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="547"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1462"/>
        <source>ВВ</source>
        <translation>TG</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="611"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="788"/>
        <source>ПРК</source>
        <translatorcomment>Recherche de voie redondante</translatorcomment>
        <translation>RVR</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="823"/>
        <source>ТЗИ</source>
        <translatorcomment>Table de signes de l&apos;afficahge</translatorcomment>
        <translation>TSA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="558"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="905"/>
        <source>В центр</source>
        <translation>Vers le centre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="567"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="736"/>
        <source>ИС</source>
        <translatorcomment>Impulsion de simulation</translatorcomment>
        <translation>ISim</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="568"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="738"/>
        <source>В/Ч</source>
        <translatorcomment>Détachement militaire</translatorcomment>
        <translation>DMil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1245"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>Режим</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1274"/>
        <source>Номер</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="961"/>
        <source>По скорости</source>
        <translation>Par vitesse</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="982"/>
        <source>По высоте</source>
        <translation>Par altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="598"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1017"/>
        <source>По классу</source>
        <translation>Par classe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="600"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1030"/>
        <source>По ОГП</source>
        <translation>Par nationalité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="600"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1039"/>
        <source>По источникам</source>
        <translation>Par sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="608"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1253"/>
        <source>Короткий</source>
        <translation>Bref</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="609"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1259"/>
        <source>Сокращенный</source>
        <translation>Abrégé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="612"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1266"/>
        <source>Номер цели</source>
        <translation>Numéro de cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="614"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1298"/>
        <source>Расстановка</source>
        <translation>Disposition</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="836"/>
        <source>2 секунды</source>
        <translation>2 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="836"/>
        <source>5 секунд</source>
        <translation>5 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="836"/>
        <source>10 секунд</source>
        <translation>10 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="837"/>
        <source>30 секунд</source>
        <translation>30 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="837"/>
        <source>1 минута</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="617"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="837"/>
        <source>5 минут</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1280"/>
        <source>Машинный</source>
        <translatorcomment>???</translatorcomment>
        <translation>Machine</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1280"/>
        <source>Единый</source>
        <translation>Uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1282"/>
        <source>КП1</source>
        <translation>PC1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1282"/>
        <source>КП2</source>
        <translation>PC2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="692"/>
        <source>Управление</source>
        <translation>Gestion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="795"/>
        <source>Манип</source>
        <translation>Manu</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="895"/>
        <source>9999.9 кфт</source>
        <translatorcomment>??????</translatorcomment>
        <translation>9999.9 кфт</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="952"/>
        <source>По номерам</source>
        <translation>par numéros</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1120"/>
        <source>-1200.99 км, -1200.99 км</source>
        <translation>-1200.99 km, -1200.99 km</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1126"/>
        <source>Координаты сетки ПВО</source>
        <translation>Coordonnées de la grille DCA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>ОО-VII</source>
        <translation>IG-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>ИО-VII</source>
        <translation>II-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>ИО-3-VII</source>
        <translation>II-3-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1284"/>
        <source>Оператор</source>
        <translation>Opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1284"/>
        <source>КП</source>
        <translatorcomment>Poste de commandement</translatorcomment>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1284"/>
        <source>Универсальный</source>
        <translation>Universel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1554"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1570"/>
        <source>Менее </source>
        <translation>Moins de</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1565"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1587"/>
        <source>Более </source>
        <translation>Plus de</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manage_graphics_controller.cpp" line="15"/>
        <source>ЗИ%1</source>
        <translatorcomment>Interdiction d&apos;émission</translatorcomment>
        <translation>IE%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="190"/>
        <source>тчк</source>
        <translation>point</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="190"/>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="237"/>
        <source>наз</source>
        <translation>désign</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="20"/>
        <source>Номер</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="21"/>
        <source>Единый номер</source>
        <translation>Numéro uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="23"/>
        <source>Номер от КП1</source>
        <translation>Numéro par PC1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="24"/>
        <source>Номер от КП2</source>
        <translation>Numéro par PC2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="26"/>
        <source>Номер борта</source>
        <translation>Numéro de série</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="28"/>
        <source>Дальность</source>
        <translation>Portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="29"/>
        <source>Азимут</source>
        <translation>Azimut</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="30"/>
        <source>Высота измер.</source>
        <translation>Altitude mesuré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="32"/>
        <source>Высота баром.</source>
        <translation>Altitude barométr.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="34"/>
        <source>Курс</source>
        <translation>Cap</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="35"/>
        <source>Скорость</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="36"/>
        <source>Класс</source>
        <translation>Classe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="38"/>
        <source>ОГП</source>
        <translatorcomment>Identification de nationalité</translatorcomment>
        <translation>IdN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="41"/>
        <source>Запас топлива</source>
        <translation>Capacité de combustible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="43"/>
        <source>Выдача на КП</source>
        <translation>Fourniture au PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="45"/>
        <source>Источники</source>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="48"/>
        <source>Код 1</source>
        <translation>Code 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="49"/>
        <source>Код 2</source>
        <translation>Code 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="52"/>
        <source>Код 3A</source>
        <translation>Code 3A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="55"/>
        <source>Код S</source>
        <translation>Code S</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="16"/>
        <source>Сервер не подключен</source>
        <translation>Serveur n&apos;est pas connecté</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="17"/>
        <source>Действие невозможно для режима воспроизведения информации</source>
        <translation>Cette action est impossible en mode de lecture des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/process_tools.cpp" line="8"/>
        <source>Невозможно прочитать директорию &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/process_tools.cpp" line="12"/>
        <source>Директория &apos;%1&apos; только для чтения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="341"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="387"/>
        <source>фт</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="343"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="391"/>
        <source>кфт</source>
        <translation>sq ft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="345"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="393"/>
        <source>км</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="347"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="395"/>
        <source>м</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="349"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="397"/>
        <source>ММ</source>
        <translation>NM</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="370"/>
        <source>фт/ч</source>
        <translation>ft/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="372"/>
        <source>фт/с</source>
        <translation>ft/sec</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="374"/>
        <source>км/ч</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="376"/>
        <source>м/с</source>
        <translation>m/sec</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="378"/>
        <source>ММ/ч</source>
        <translation>NM/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="389"/>
        <source>гм</source>
        <translation>hm</translation>
    </message>
</context>
<context>
    <name>Thread::CopyFile</name>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="44"/>
        <source>невозможно прочитать каталог &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="52"/>
        <source>Невозможно открыть файл &apos;%1&apos; для записи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="53"/>
        <source>Копирование файла &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="84"/>
        <source>Ошибка CRC для файла &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Thread::RemoveFile</name>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/remove_file_thread.cpp" line="26"/>
        <source>Удаление файла &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools::Manager::ContextMenu::Area</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="95"/>
        <source>Координаты курсора</source>
        <translation>Coordonnées du curseur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="106"/>
        <source>Полярный</source>
        <translation>Polaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="107"/>
        <source>Прямоугольный</source>
        <translation>Rectangulaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="101"/>
        <source>Измерить расстояние</source>
        <translation>Mesurer la distance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="108"/>
        <source>Бланки автозахвата</source>
        <translation>Suppression de l&apos;acquisition automatique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="114"/>
        <source>Распоряжение ТР</source>
        <translatorcomment>CT = Conversation téléphonique</translatorcomment>
        <translation>Demande de CT</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::ContextMenu::GroundObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="63"/>
        <source>Редактировать</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="81"/>
        <source>Наведение</source>
        <translation>Guidage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="91"/>
        <source>Координаты</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="101"/>
        <source>Удалить</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::ContextMenu::Target</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="144"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="152"/>
        <source>Ошибка</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="144"/>
        <source>Единый номер занят</source>
        <translation>Numéro uni est occupé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="150"/>
        <source>(Р)</source>
        <translatorcomment>????</translatorcomment>
        <translation>(P)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="152"/>
        <source>Номер оператора занят</source>
        <translation>Numéro de l&apos;opérateur est occupé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="242"/>
        <source>Добавить в ТЗИ</source>
        <translatorcomment>Visualisation alpha-numérique</translatorcomment>
        <translation>Ajouter à la table VAN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="243"/>
        <source>Удалить из ТЗИ</source>
        <translatorcomment>Visualisation alpha-numérique</translatorcomment>
        <translation>Rayer de la table VAN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="270"/>
        <source>Наведение</source>
        <translation>Guidage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="282"/>
        <source>Полный формуляр</source>
        <translation>Fiche complet</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="288"/>
        <source>Назначить класс цели</source>
        <translation>Attribuer une classe de cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Самолет</source>
        <translation>Avion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Вертолет</source>
        <translation>Hélicoptère</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Крылатая ракета</source>
        <translation>Missile de croisière</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Гиперзвуковая крылатая ракета</source>
        <translation>Missile de croisière hypersonique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="292"/>
        <source>Аэростат</source>
        <translation>Aérostat</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="292"/>
        <source>Баллистическая цель</source>
        <translation>Cible ballistique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="383"/>
        <source>Распоряжение ТР</source>
        <translatorcomment>CT = Conversation téléphonique</translatorcomment>
        <translation>Demande de CT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="440"/>
        <source>Назначить госпринадлежность</source>
        <translation>Attribuer une nationalité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="444"/>
        <source>Свой ИО</source>
        <translatorcomment>Identification individuelle</translatorcomment>
        <translation>II &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="444"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="446"/>
        <source>Свой ОО</source>
        <translatorcomment>Identification générale</translatorcomment>
        <translation>IG &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="444"/>
        <source>Свой RBS</source>
        <translation>RBS &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="446"/>
        <source>Свой ГО</source>
        <translatorcomment>Identification Nationalité - если ГО = &quot;государственное опознавание&quot;</translatorcomment>
        <translation>IN &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="448"/>
        <source>Чужой</source>
        <translation>Ennemi</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="461"/>
        <source>Уточнить ГП</source>
        <translation>Préciser la Nationalité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="304"/>
        <source>Передача на КП</source>
        <translation>Transmission vers le PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="319"/>
        <source>КП %1</source>
        <translation>PC %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="336"/>
        <source>Траектория</source>
        <translation>Trajectoire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="349"/>
        <source>Назначить номер</source>
        <translation>Attribuer un numéro</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="358"/>
        <source>Оператора</source>
        <translation>de l&apos;Opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="364"/>
        <source>Единый</source>
        <translation>Uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="370"/>
        <source>Применить</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="410"/>
        <source>Разъединить</source>
        <translation>Déconnecter</translation>
        <extra-auto>submenu = _menu-&gt;addMenu(tr(&quot;Приоритет источника&quot;));</extra-auto>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="415"/>
        <source>Объединить</source>
        <translatorcomment>в значении &quot;соединить/объединить попарно&quot;</translatorcomment>
        <translation>Coupler</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="397"/>
        <source>Сбросить</source>
        <translatorcomment>в значении &quot;сбросить входящий вызов&quot;</translatorcomment>
        <translation>Refuser</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::Shortcut</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="44"/>
        <source>Выбор файла стиля</source>
        <translation>Choix du fichier de style</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="44"/>
        <source>Файлы стилей(*.qss)</source>
        <translation>Fichier de style(*.qss)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="58"/>
        <source>Рабочее место оператора</source>
        <translation>Poste de travail de l&apos;opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="59"/>
        <source>Версия сборки: %1</source>
        <translation>Version de l&apos;assemblage: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="69"/>
        <source>Доступные щрифты</source>
        <translation>Caractères disponibles</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::Usb</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/usb_manager_tools.cpp" line="89"/>
        <source>операция прервана</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools::Translator</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="51"/>
        <source>МУ</source>
        <translatorcomment>Commande Locale - Местное Управление</translatorcomment>
        <translation>CL</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="49"/>
        <source>ЦУ</source>
        <translatorcomment>Commande centrale</translatorcomment>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="51"/>
        <source>ВКЛ</source>
        <translatorcomment>Это самое распространенное сокращение</translatorcomment>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="51"/>
        <source>ВЫКЛ</source>
        <translatorcomment>Это самое распространенное сокращение</translatorcomment>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="53"/>
        <source>АНТ</source>
        <translatorcomment>Если это &quot;Антенна&quot;</translatorcomment>
        <translation>ANT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="53"/>
        <source>ЭКВ</source>
        <translatorcomment>Если это &quot;эквивалент&quot;</translatorcomment>
        <translation>EQV</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="55"/>
        <source>ПЭП1</source>
        <translatorcomment>Commutation de sous-gammes</translatorcomment>
        <translation>CSG1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="55"/>
        <source>ПЭП2</source>
        <translatorcomment>Commutation de sous-gammes</translatorcomment>
        <translation>CSG2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="57"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="57"/>
        <source>180</source>
        <translation>180</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="105"/>
        <source>РЛС</source>
        <translation>Radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="107"/>
        <source>ВРЛ</source>
        <translatorcomment>Это привычное англ. сокращение</translatorcomment>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="67"/>
        <source>%1(КП)</source>
        <translation>%1(PC)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="69"/>
        <source>%1(Р)</source>
        <translatorcomment>Радар???</translatorcomment>
        <translation>%1(R)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="73"/>
        <source>Т</source>
        <translatorcomment>????</translatorcomment>
        <translation>Т</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="83"/>
        <source>%1 (%2 дБ)</source>
        <translation>%1 (%2 dB)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="88"/>
        <source>Антенна</source>
        <translation>Antenne</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="88"/>
        <source>Эквивалент</source>
        <translation>Équivalent</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="124"/>
        <source>Произведено включение РЛС
Проверьте время</source>
        <translation>Radar est mis en fonction
Vérifier le temps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="131"/>
        <source>В РЭУ 5Ц3-Е сектор %1 температура не в норме</source>
        <translation>Température anormale dans le DRE 5Ц3-Е secteur %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="137"/>
        <source>В РЭК 1 температура не в норме</source>
        <translation>Température anormale dans le DRE1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="139"/>
        <source>ЛИРА температура не в норме</source>
        <translation>Température anormale dans LYRA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="155"/>
        <source>Тревога</source>
        <translation>Alarme</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="162"/>
        <source>Приемник %1 сектора %2</source>
        <translation>Récepteur %1 du secteur %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="246"/>
        <source>Не определен</source>
        <translation>Non défini</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="246"/>
        <source>Х</source>
        <translatorcomment>???</translatorcomment>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="248"/>
        <source>Свой ОО</source>
        <translation>IG &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="248"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="254"/>
        <source>П</source>
        <translatorcomment>?????</translatorcomment>
        <translation>П</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="251"/>
        <source>Свой ГО</source>
        <translation>IN &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="251"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="256"/>
        <source>В</source>
        <translatorcomment>?????</translatorcomment>
        <translation>В</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="254"/>
        <source>Свой ИО</source>
        <translation>IG &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="259"/>
        <source>Чужой</source>
        <translation>Ennemi</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="259"/>
        <source>Ч</source>
        <translatorcomment>?????</translatorcomment>
        <translation>Ч</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="167"/>
        <source>Не распознан</source>
        <translation>Non identifié</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="143"/>
        <source>Ошибка авторизации: &apos;пользователь не существует&apos;</source>
        <translation>Erreur d&apos;autorisation: &apos;utilisateur inexistant&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="145"/>
        <source>Ошибка авторизации: &apos;неверный пароль&apos;</source>
        <translation>Erreur d&apos;autorisation: &apos;mot de passe incorrect&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="147"/>
        <source>Вы вошли как &apos;гость&apos;</source>
        <translation>Vous êtes entré comme &apos;visiteur&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="149"/>
        <source>Вы вошли как &apos;командир&apos;</source>
        <translation>Vous êtes entré comme &apos;commandant&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="151"/>
        <source>Вы вошли как &apos;%1&apos;</source>
        <translation>Vous êtes entré comme &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="169"/>
        <source>Самолет</source>
        <translation>Avion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="169"/>
        <source>C</source>
        <translatorcomment>??????</translatorcomment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="171"/>
        <source>Вертолет</source>
        <translation>Hélicoptère</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="173"/>
        <source>Крылатая ракета</source>
        <translation>Missile de croisière</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="173"/>
        <source>КР</source>
        <translation>MC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="175"/>
        <source>Гиперзвуковая крылатая ракета</source>
        <translation>Missile de croisière hypersonique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="175"/>
        <source>ГЗ</source>
        <translatorcomment>если имеется виду &quot;Гиперзвуковой&quot;</translatorcomment>
        <translation>HS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="177"/>
        <source>Аэростат</source>
        <translation>Aérostat</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="177"/>
        <source>А</source>
        <translatorcomment>???</translatorcomment>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="179"/>
        <source>Баллистическая цель</source>
        <translation>Cible balistique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="179"/>
        <source>БЦ</source>
        <translation>CB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="184"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="216"/>
        <source>КП: </source>
        <translation>PC: </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="233"/>
        <source>KIR-1A</source>
        <translation>KIR-1A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="235"/>
        <source>41М1.6
(6110)</source>
        <translation>41М1.6
(6110)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="238"/>
        <source>КД используются</source>
        <translation>CE sont utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="238"/>
        <source>КД не используются</source>
        <translation>CE ne sont pas utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="239"/>
        <source>КП используются</source>
        <translation>CC sont utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="239"/>
        <source>КП не используются</source>
        <translation>CC ne sont pas utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="256"/>
        <source>Гражданский</source>
        <translation>Civil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="205"/>
        <source>АФК</source>
        <translation>CAF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="207"/>
        <source>ПРК</source>
        <translation>RVR</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="208"/>
        <source>РЕЖ: %1</source>
        <translation>MODE: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="214"/>
        <source>КП%1: </source>
        <translation>PC%1: </translation>
    </message>
</context>
<context>
    <name>Glass</name>
    <message>
        <location filename="../libs/gui/src/glass.cpp" line="13"/>
        <location filename="../libs/gui/src/glass.cpp" line="15"/>
        <source>Отмена</source>
        <translation>Annulation</translation>
    </message>
    <message>
        <location filename="../libs/gui/src/glass.cpp" line="39"/>
        <source>Обработка...</source>
        <translation>Traitement...</translation>
    </message>
</context>
<context>
    <name>GuiDisabler</name>
    <message>
        <location filename="../libs/gui/src/gui_disabler.cpp" line="34"/>
        <location filename="../libs/gui/src/gui_disabler.cpp" line="38"/>
        <source>Внимание</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CodogrammFilterDialog</name>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_dialog.cpp" line="11"/>
        <source>Фильтр кодограмм</source>
        <translation>Filtre de codogrammes</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_dialog.cpp" line="48"/>
        <source>Применить</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_dialog.cpp" line="51"/>
        <source>Отмена</source>
        <translation>Annulation</translation>
    </message>
</context>
<context>
    <name>CodogrammFilterWidget</name>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="30"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="120"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="125"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="129"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="297"/>
        <source>Предупреждение</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="125"/>
        <source>Фильтр некорректный</source>
        <translation>Filtre incorrect</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="30"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="129"/>
        <source>Фильтр уже существует</source>
        <translation>Filtre exitant</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="52"/>
        <source>Выражение</source>
        <translation>Expression</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="58"/>
        <source>Все И</source>
        <translation>Tous ET</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="62"/>
        <source>Все ИЛИ</source>
        <translation>Tous OU</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="76"/>
        <source>Поле</source>
        <translation>Champ</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="80"/>
        <source>Оператор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="86"/>
        <source>Значение</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="95"/>
        <source>Добавить фильтр</source>
        <translation>Ajouter un filtre</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="99"/>
        <source>Обновить фильтр</source>
        <translation>Rafraîchir le filtre</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="103"/>
        <source>Удалить фильтр</source>
        <translation>Supprimer le filtre</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="107"/>
        <source>Clear</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="120"/>
        <source>Фильтр не выбран</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="298"/>
        <source>Выражение содержит несуществующий фильтр</source>
        <translation>Expression contient un filtre inexistant</translation>
    </message>
</context>
<context>
    <name>Explorer</name>
    <message>
        <location filename="../libs/tools/src/explorer.cpp" line="41"/>
        <source>Формуляр</source>
        <translation>Fiche</translation>
    </message>
</context>
<context>
    <name>InterfaceController</name>
    <message>
        <location filename="../libs/communication/src/controller/interface_controller.cpp" line="95"/>
        <source>получено %1 %2, отправлено %3 %4</source>
        <translation>reçu %1 %2, transmis %3 %4</translation>
    </message>
</context>
<context>
    <name>InterfaceIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="91"/>
        <source>Мб</source>
        <translatorcomment>возможно использование англ.сокращения Mb</translatorcomment>
        <translation>Mo</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="94"/>
        <source>Кб</source>
        <translatorcomment>возможно использование англ. сокращения Kb</translatorcomment>
        <translation>Ko</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="96"/>
        <source>байт</source>
        <translatorcomment>или - byte</translatorcomment>
        <translation>octet</translation>
    </message>
</context>
<context>
    <name>TcpController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="41"/>
        <source>подключение к %1:%2...</source>
        <translatorcomment>в зависимости от идущего дальше слова, может быть &quot;connexion au&quot;</translatorcomment>
        <translation>connexion à %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="54"/>
        <source>соединение с %1:%2 не установлено</source>
        <translation>connexion avec %1:%2 n&apos;est pas établie</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="57"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connexion avec %1:%2 est rompue</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="62"/>
        <source> по ошибке &apos;%1&apos;</source>
        <translation> par erreur &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="70"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connexion avec %1:%2 est établie</translation>
    </message>
</context>
<context>
    <name>TcpIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="27"/>
        <source>Удаленный хост</source>
        <translation>hébergeur à distance</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="36"/>
        <source>Удаленный порт</source>
        <translation>Port distant</translation>
    </message>
</context>
<context>
    <name>TcpServerController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="34"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="84"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="103"/>
        <source>ожидание соединения по %1:%2...</source>
        <translation>attente de la connection par %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="50"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connexion avec %1:%2 est établie</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="73"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connexion avec %1:%2 est rompue</translation>
    </message>
</context>
<context>
    <name>UdpController</name>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="27"/>
        <source>привязка к %1:%2 выполнена</source>
        <translatorcomment>rattachement - в случае, например, &quot;привязки к координатам&quot; (тогда предлог также принимает форму множественного числа - aux), &quot;привязка данных&quot; будет bindage des données</translatorcomment>
        <translation>rattachement à %1:%2 est fait</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="29"/>
        <source>ошибка привязки к %1:%2 - %3</source>
        <translatorcomment>или - aux (если дальше идут существительные во мн.ч.)</translatorcomment>
        <translation>erreur de rattachement à %1:%2 - %3</translation>
    </message>
</context>
<context>
    <name>LogReader</name>
    <message>
        <location filename="../plugins/resonance/src/processing/log_reader/log_reader.cpp" line="34"/>
        <source>неверное контрольное слово &apos;%1&apos; для заголовка блока</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvrlkServerReader</name>
    <message>
        <location filename="../plugins/resonance/src/processing/mvrlk_server_reader/mvrlk_server_reader.cpp" line="69"/>
        <source>ошибка: кодограмма неизвестного типа &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/mvrlk_server_reader/mvrlk_server_reader.cpp" line="75"/>
        <source>ошибка: некорректный размер кодограммы &apos;%1&apos; для типа &apos;%2&apos;; ожидается &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/mvrlk_server_reader/mvrlk_server_reader.cpp" line="85"/>
        <source>кодограмма прочитана успешно: тип = &apos;%1&apos;, размер = &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerReader</name>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="172"/>
        <source>неизвестный тип кодограммы &apos;%1(0x%2)&apos;; размер &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="185"/>
        <source>неправильный размер кодограммы &apos;%1&apos; для типа &apos;%2(0x%3)&apos; - ожидается &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="199"/>
        <source>неверный размер кодограммы &apos;%1&apos; для типа &apos;%2(0x%3)&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="217"/>
        <source>неверное окончание кодограммы для типа &apos;%1(0x%2)&apos; размера &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="223"/>
        <source>кодограмма тип &apos;%1(0x%2)&apos;; размер &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerReflyCommand</name>
    <message>
        <location filename="../plugins/resonance/src/data/rmo_server_refly_command/rmo_server_refly_command.cpp" line="73"/>
        <source>Команда переоблета</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerReflyTime</name>
    <message>
        <location filename="../plugins/resonance/src/data/rmo_server_refly_time/rmo_server_refly_time.cpp" line="43"/>
        <source>Текущее время переоблета</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerSzi</name>
    <message>
        <location filename="../plugins/resonance/src/data/rmo_server_szi/rmo_server_szi.cpp" line="42"/>
        <source>Сектор запрета излучения</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoApdState</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="45"/>
        <source>Ошибка субмодуля &quot;Кодек КП&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="46"/>
        <source>Поляна</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="46"/>
        <source>Поляна (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="46"/>
        <source>Фундамент</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="47"/>
        <source>Астерикс (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="58"/>
        <source>АПД не используется</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="59"/>
        <source>Связь не устанвлена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="59"/>
        <source>Связь установлена частично</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="60"/>
        <source>Связь установлена</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoCounter</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_counter/server_rmo_counter.cpp" line="56"/>
        <source>Счетчик обзоров</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoCpConnectionState</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="42"/>
        <source>Ошибка субмодуля &quot;Кодек КП&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="44"/>
        <source>Поляна</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="46"/>
        <source>Поляна (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="48"/>
        <source>Фундамент</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="50"/>
        <source>Астерикс (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="100"/>
        <source>Состояние связи с КП</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoInfo</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_info/server_rmo_info.cpp" line="65"/>
        <source>Итоговая справка</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoMvrlkState</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_mvrlk_state/server_rmo_mvrlk_state.cpp" line="140"/>
        <source>Состояние ВРЛ</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoPeleng</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_peleng/server_rmo_peleng.cpp" line="99"/>
        <source>Пеленг</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>LTR</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Guide d&apos;utilisation</translation>
    </message>
    <message>
        <source>Don&apos;t Save</source>
        <translation>Ne pas enregistrer</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>Rejeter les changements</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>Ou&amp;i</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>Oui à &amp;tout</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>Non à to&amp;ut</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>Tout enregistrer</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>Réessayer</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>Ignorer</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Restaurer les paramètres par défaut</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>Fermer sans enregistrer</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <source>&amp;Show this message again</source>
        <translation>&amp;Afficher ce message à l&apos;avenir</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Debug Message:</source>
        <translation>Message de débogage:</translation>
    </message>
    <message>
        <source>Warning:</source>
        <translation>Avertissement:</translation>
    </message>
    <message>
        <source>Fatal Error:</source>
        <translation>Erreur fatale:</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <source>Enter a value:</source>
        <translation>Saisissez une valeur:</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation>Utiliser</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;A propos de Qt&lt;/h3&gt;&lt;p&gt;Ce programme utilise Qt version %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation>A propos de Qt</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Guide d&apos;utilisation</translation>
    </message>
    <message>
        <source>Show Details...</source>
        <translation>Afficher les détails</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation>Masquer les détails</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>QSoftKeyManager</name>
    <message>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <source>Done</source>
        <translation>Terminer</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;Rétablir</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Co&amp;uper</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Copier</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>Copier l&apos;adresse du l&amp;ien</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>Co&amp;ller</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Tout sélectionner</translation>
    </message>
</context>
</TS>
