<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name>CodogrammFilterDialog</name>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_dialog.cpp" line="11"/>
        <source>Фильтр кодограмм</source>
        <translation>Codogram filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_dialog.cpp" line="48"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_dialog.cpp" line="51"/>
        <source>Отмена</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>CodogrammFilterWidget</name>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="52"/>
        <source>Выражение</source>
        <translation>Expression</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="58"/>
        <source>Все И</source>
        <translation>All and</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="62"/>
        <source>Все ИЛИ</source>
        <translation>All or</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="76"/>
        <source>Поле</source>
        <translation>Field</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="80"/>
        <source>Оператор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="86"/>
        <source>Значение</source>
        <translation>Value</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="95"/>
        <source>Добавить фильтр</source>
        <translation>Add filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="99"/>
        <source>Обновить фильтр</source>
        <translation>Update filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="103"/>
        <source>Удалить фильтр</source>
        <translation>Delete filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="107"/>
        <source>Clear</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="120"/>
        <source>Фильтр не выбран</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="30"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="120"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="125"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="129"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="297"/>
        <source>Предупреждение</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="125"/>
        <source>Фильтр некорректный</source>
        <translation>Incorrect filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="30"/>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="129"/>
        <source>Фильтр уже существует</source>
        <translation>Filter already exists</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/codogramm_filter_widget.cpp" line="298"/>
        <source>Выражение содержит несуществующий фильтр</source>
        <translation>Expression contains non-existent filter</translation>
    </message>
</context>
<context>
    <name>Explorer</name>
    <message>
        <location filename="../libs/tools/src/explorer.cpp" line="41"/>
        <source>Формуляр</source>
        <translation>Explorer</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="345"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="393"/>
        <source>км</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="347"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="395"/>
        <source>м</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="349"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="397"/>
        <source>ММ</source>
        <translation>MM</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="343"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="391"/>
        <source>кфт</source>
        <translation>kft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="341"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="387"/>
        <source>фт</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="376"/>
        <source>м/с</source>
        <translation>m/s</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="374"/>
        <source>км/ч</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="378"/>
        <source>ММ/ч</source>
        <translation>MM/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="372"/>
        <source>фт/с</source>
        <translation>ft/s</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="370"/>
        <source>фт/ч</source>
        <translation>ft/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="389"/>
        <source>гм</source>
        <translation>hm</translation>
    </message>
</context>
</TS>
