<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="ru">
<context>
    <name>InterfaceController</name>
    <message>
        <location filename="../libs/communication/src/controller/interface_controller.cpp" line="95"/>
        <source>получено %1 %2, отправлено %3 %4</source>
        <translation>reçu %1 %2, transmis %3 %4</translation>
    </message>
</context>
<context>
    <name>InterfaceIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="91"/>
        <source>Мб</source>
        <translatorcomment>возможно использование англ.сокращения Mb</translatorcomment>
        <translation>Mo</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="94"/>
        <source>Кб</source>
        <translatorcomment>возможно использование англ. сокращения Kb</translatorcomment>
        <translation>Ko</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="96"/>
        <source>байт</source>
        <translatorcomment>или - byte</translatorcomment>
        <translation>octet</translation>
    </message>
</context>
<context>
    <name>TcpController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="41"/>
        <source>подключение к %1:%2...</source>
        <translatorcomment>в зависимости от идущего дальше слова, может быть &quot;connexion au&quot;</translatorcomment>
        <translation>connexion à %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="54"/>
        <source>соединение с %1:%2 не установлено</source>
        <translation>connexion avec %1:%2 n&apos;est pas établie</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="57"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connexion avec %1:%2 est rompue</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="62"/>
        <source> по ошибке &apos;%1&apos;</source>
        <translation> par erreur &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="70"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connexion avec %1:%2 est établie</translation>
    </message>
</context>
<context>
    <name>TcpIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="27"/>
        <source>Удаленный хост</source>
        <translation>hébergeur à distance</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="36"/>
        <source>Удаленный порт</source>
        <translation>Port distant</translation>
    </message>
</context>
<context>
    <name>TcpServerController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="34"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="84"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="103"/>
        <source>ожидание соединения по %1:%2...</source>
        <translation>attente de la connection par %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="50"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connexion avec %1:%2 est établie</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="73"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connexion avec %1:%2 est rompue</translation>
    </message>
</context>
<context>
    <name>UdpController</name>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="27"/>
        <source>привязка к %1:%2 выполнена</source>
        <translatorcomment>rattachement - в случае, например, &quot;привязки к координатам&quot; (тогда предлог также принимает форму множественного числа - aux), &quot;привязка данных&quot; будет bindage des données</translatorcomment>
        <translation>rattachement à %1:%2 est fait</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="29"/>
        <source>ошибка привязки к %1:%2 - %3</source>
        <translatorcomment>или - aux (если дальше идут существительные во мн.ч.)</translatorcomment>
        <translation>erreur de rattachement à %1:%2 - %3</translation>
    </message>
</context>
</TS>
