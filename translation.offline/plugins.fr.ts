<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="ru">
<context>
    <name>LogReader</name>
    <message>
        <location filename="../plugins/resonance/src/processing/log_reader/log_reader.cpp" line="34"/>
        <source>неверное контрольное слово &apos;%1&apos; для заголовка блока</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MvrlkServerReader</name>
    <message>
        <location filename="../plugins/resonance/src/processing/mvrlk_server_reader/mvrlk_server_reader.cpp" line="69"/>
        <source>ошибка: кодограмма неизвестного типа &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/mvrlk_server_reader/mvrlk_server_reader.cpp" line="75"/>
        <source>ошибка: некорректный размер кодограммы &apos;%1&apos; для типа &apos;%2&apos;; ожидается &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/mvrlk_server_reader/mvrlk_server_reader.cpp" line="85"/>
        <source>кодограмма прочитана успешно: тип = &apos;%1&apos;, размер = &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerReader</name>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="172"/>
        <source>неизвестный тип кодограммы &apos;%1(0x%2)&apos;; размер &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="185"/>
        <source>неправильный размер кодограммы &apos;%1&apos; для типа &apos;%2(0x%3)&apos; - ожидается &apos;%4&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="199"/>
        <source>неверный размер кодограммы &apos;%1&apos; для типа &apos;%2(0x%3)&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="217"/>
        <source>неверное окончание кодограммы для типа &apos;%1(0x%2)&apos; размера &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/processing/rmo_server_reader/rmo_server_reader.cpp" line="223"/>
        <source>кодограмма тип &apos;%1(0x%2)&apos;; размер &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerReflyCommand</name>
    <message>
        <location filename="../plugins/resonance/src/data/rmo_server_refly_command/rmo_server_refly_command.cpp" line="73"/>
        <source>Команда переоблета</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerReflyTime</name>
    <message>
        <location filename="../plugins/resonance/src/data/rmo_server_refly_time/rmo_server_refly_time.cpp" line="43"/>
        <source>Текущее время переоблета</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RmoServerSzi</name>
    <message>
        <location filename="../plugins/resonance/src/data/rmo_server_szi/rmo_server_szi.cpp" line="42"/>
        <source>Сектор запрета излучения</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoApdState</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="45"/>
        <source>Ошибка субмодуля &quot;Кодек КП&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="46"/>
        <source>Поляна</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="46"/>
        <source>Поляна (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="46"/>
        <source>Фундамент</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="47"/>
        <source>Астерикс (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="58"/>
        <source>АПД не используется</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="59"/>
        <source>Связь не устанвлена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="59"/>
        <source>Связь установлена частично</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_apd_state/server_rmo_apd_state.cpp" line="60"/>
        <source>Связь установлена</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoCounter</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_counter/server_rmo_counter.cpp" line="56"/>
        <source>Счетчик обзоров</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoCpConnectionState</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="42"/>
        <source>Ошибка субмодуля &quot;Кодек КП&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="44"/>
        <source>Поляна</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="46"/>
        <source>Поляна (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="48"/>
        <source>Фундамент</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="50"/>
        <source>Астерикс (HLCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_cp_connection_state/server_rmo_cp_connection_state.cpp" line="100"/>
        <source>Состояние связи с КП</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoInfo</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_info/server_rmo_info.cpp" line="65"/>
        <source>Итоговая справка</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoMvrlkState</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_mvrlk_state/server_rmo_mvrlk_state.cpp" line="140"/>
        <source>Состояние ВРЛ</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ServerRmoPeleng</name>
    <message>
        <location filename="../plugins/resonance/src/data/server_rmo_peleng/server_rmo_peleng.cpp" line="99"/>
        <source>Пеленг</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
