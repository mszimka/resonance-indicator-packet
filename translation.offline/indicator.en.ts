<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name>Data::Target</name>
    <message>
        <location filename="../apps/resonance/indicator/src/data/target_data.cpp" line="105"/>
        <source>трасса %1 сброшена по таймауту</source>
        <translation>track %1 is timed out</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Bar</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="78"/>
        <source>Назад</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="80"/>
        <source>Вперед</source>
        <translation>Forward</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="82"/>
        <source>Домой</source>
        <translation>Main menu</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="90"/>
        <source>Общее
состояние</source>
        <translation>General state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="91"/>
        <source>Состояние
УМ</source>
        <translation>PA status</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="93"/>
        <source>Краткое состояние
приемников</source>
        <translation>Receiver state summary</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="95"/>
        <source>Полное состояние
приемников</source>
        <translation>Receiver channels parameters</translation>
    </message>
</context>
<context>
    <name>Diagnostic::BaseScene</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/base_diagnostic_scene.cpp" line="53"/>
        <source>Сектор%1</source>
        <translation>Sector%1</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::AnalogGeneral</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="19"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="33"/>
        <source>ЦОС%1</source>
        <translation>DSP%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="44"/>
        <source>УМ%1</source>
        <translation>PA%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="49"/>
        <source>Приемник%1 (f1)</source>
        <translation>Receiver %1(f1)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="52"/>
        <source>Антенна</source>
        <translation>Antenna</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="55"/>
        <source>Антенна Az</source>
        <translation>Antenna Az</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="60"/>
        <source>Приемник%1 (f2)</source>
        <translation>Receiver%1 (f2)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="66"/>
        <source>Антенна E</source>
        <translation>Antenna E</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Antenna::Calibration</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="58"/>
        <source>Канал</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="59"/>
        <source>До калибровки</source>
        <translation>Before calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="60"/>
        <source>После калибровки</source>
        <translation>After calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>Шум, дБ</source>
        <translation>Noise, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Амплитуда, дБ</source>
        <translation>Amplitude,dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Фаза, гр</source>
        <translation>Phase, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>СКО ампл., дБ</source>
        <translation>Ampl. RMSE, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>СКО фазы, гр</source>
        <translation>Phase RMSE, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>АФЮС ампл., дБ</source>
        <translation>Ampl. IASS, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>АФЮС фаза., гр</source>
        <translation>Phase. IASS, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон ампл., дБ</source>
        <translation>Ampl. standard. dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон фазы., гр</source>
        <translation>Phase standard, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка ампл., дБ</source>
        <translation>Ampl. correction, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка фазы., гр</source>
        <translation>Phase correction, deg</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Antenna::Requester</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="57"/>
        <source>Направление</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="67"/>
        <source>Тип</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="69"/>
        <source>Азимутальная</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="69"/>
        <source>Угломестная</source>
        <translation>Elevation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="71"/>
        <source>Контроль по</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="73"/>
        <source>ИС</source>
        <translation>SS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="73"/>
        <source>АФЮС</source>
        <translation>IASS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="75"/>
        <source>Частота, МГц</source>
        <translation>Frequency, MHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="106"/>
        <source>Рассчитать</source>
        <translation>Calculate</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::BaseGeneral</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="91"/>
        <source>Сервер</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="94"/>
        <source>ИБП1</source>
        <translation>UPS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="95"/>
        <source>ИБП2</source>
        <translation>UPS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="80"/>
        <source>РМО1</source>
        <translation>OWS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="81"/>
        <source>РМО2</source>
        <translation>OWS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="82"/>
        <source>ВРМО</source>
        <translation>ROWS</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::CommandPostCommunication</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="87"/>
        <source>Сопряжение с КП%1</source>
        <translation>Interface with CC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="94"/>
        <source>Состояние связи</source>
        <translation>Communication state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="102"/>
        <source>Состояние АПД</source>
        <translation>DTE state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="95"/>
        <source>Командир</source>
        <translation>Commander</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="96"/>
        <source>Инициализация
передачи</source>
        <translation>Transmission
initialization</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="97"/>
        <source>Прием
сообщений</source>
        <translation>Message
receiving</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="103"/>
        <source>Готовность</source>
        <translation>Ready</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="106"/>
        <source>Физическое
соединение</source>
        <translation>Physical
connection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="107"/>
        <source>Логическое
соединение</source>
        <translation>Logical
connection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="99"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="152"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="172"/>
        <source>Доставка
сообщений</source>
        <translation>Message
delivery</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="105"/>
        <source>Прием
данных</source>
        <translation>Data
receiving</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="104"/>
        <source>Передача
данных</source>
        <translation>Data
delivery</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="157"/>
        <source>Тип протокола: %1</source>
        <translation>Protocol type:%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="158"/>
        <source>Принято пакетов: %1</source>
        <translation>RX packets: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="159"/>
        <source>Ошибок приема: %1</source>
        <translation>RX errors: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="160"/>
        <source>Выдано пакетов: %1</source>
        <translation>TX packets: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="161"/>
        <source>Ошибок выдачи: %1</source>
        <translation>TX errors: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="162"/>
        <source>Просрочено пакетов: %1</source>
        <translation>TX expired: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="163"/>
        <source>Задержка: %1 мс</source>
        <translation>Delay: %1 ms</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="170"/>
        <source>Синхронизация
времени</source>
        <translation>Time
synchronization</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::General</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/russia_general_diagnostic_explorer.cpp" line="24"/>
        <source>Сообщения об отказах</source>
        <translation>Failure report</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="30"/>
        <source>РМО1</source>
        <translation>OWS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="31"/>
        <source>РМО2</source>
        <translation>OWS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="32"/>
        <source>ВРМО</source>
        <translation>ROWS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="33"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="31"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="34"/>
        <source>Сервер</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="32"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="35"/>
        <source>ИБП1</source>
        <translation>UPS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="36"/>
        <source>ИБП2</source>
        <translation>UPS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="37"/>
        <source>АПД1</source>
        <translation>DTE1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="66"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="38"/>
        <source>АПД2</source>
        <translation>DTE2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="73"/>
        <source>Приемник%1</source>
        <translation>Reciever%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="39"/>
        <source>АПД3</source>
        <translation>DTE3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="48"/>
        <source>ЦОС%1</source>
        <translation>DSP%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="50"/>
        <source>Антенна</source>
        <translation>Antenna</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="50"/>
        <source>Эквивалент</source>
        <translation>Equivalent</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="55"/>
        <source>УМ%1</source>
        <translation>PA%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="58"/>
        <source>Приемник%1 (f1)</source>
        <translation>Receiver %1(f1)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="61"/>
        <source>Приемник%1 (f2)</source>
        <translation>Receiver%1 (f2)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="64"/>
        <source>Антенна Az</source>
        <translation>Antenna Az</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="67"/>
        <source>Антенна E</source>
        <translation>Antenna E</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Reciever::Calibration</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="141"/>
        <source>Приемник f1</source>
        <translation>Reciever f1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="85"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="142"/>
        <source>Приемник f2</source>
        <translation>Reciever f2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="87"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="146"/>
        <source>Азимутальные каналы</source>
        <translation>Azimuth channels</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="147"/>
        <source>Угломестные каналы</source>
        <translation>Elevation channels</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <source>Внутр. шум дБ</source>
        <translation>Inner noise dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <source>Внутр. ампл. дБ</source>
        <translation>Innser ampl.dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <source>Внутр. фаза гр</source>
        <translation>Inner phase deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="92"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="151"/>
        <source>Внеш. шум дБ</source>
        <translation>Exter. noise dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="92"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="150"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="151"/>
        <source>Внеш. ампл. дБ</source>
        <translation>Exter. ampl. dB</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Reciever::Tech</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="73"/>
        <source>Мнемосхема шкафа приемника сектора %1 РЭМ 3 5ЦП1-01</source>
        <translation>Receiver sector %1 REM 3 5ЦП1-01 cabinet symb.circuit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="95"/>
        <source>АА%1
5ЦП101Б%2</source>
        <translation>AA%1
5ЦП1Б%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="99"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="100"/>
        <source>Плата управления АА2</source>
        <translation>Control card AA2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="104"/>
        <source>АА%1
Узел питания %2</source>
        <translation>AA%1
power supply %2</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::Transmitter</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="94"/>
        <source>Состояние усилителя мощности сектора %1</source>
        <translation>PA sector state %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="103"/>
        <source>Техническое состояние ТЭЗ УМ</source>
        <translation>PA LRU technical state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>5Ц300Я01</source>
        <translation>5Ц300Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>БКУ 5Ц301Б03</source>
        <translation>CU 5Ц301Б03 </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>БУМ 5Ц301Б01 (верх)</source>
        <translation>PAU 5Ц301Б01(top)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="107"/>
        <source>БУМ 5Ц301Б01 (низ)</source>
        <translation>PAU 5Ц301Б01 (bottom)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>БСДНО 5Ц301Б02</source>
        <translation>SDDCU 5Ц301Б02 </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>БП 5Ц301Б05</source>
        <translation>PSU НПС3000.9.3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>БФГ 5Ц301Б04</source>
        <translation>HFU 5Ц301Б04</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="108"/>
        <source>5Ц301Я01</source>
        <translation>5Ц301Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="109"/>
        <source>Узел распределения пит.</source>
        <translation>Power distribution u.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="109"/>
        <source>Узел воздушного охл.</source>
        <translation>Air cooling u.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="113"/>
        <source>%1 канал</source>
        <translation>%1 channel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="117"/>
        <source>%1А%2</source>
        <translation>%1A%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>1А8</source>
        <translation>1A8</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>1А11</source>
        <translation>1A11</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>1А7</source>
        <translation>1A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>2А7</source>
        <translation>2A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="128"/>
        <source>Функциональное состояние УМ</source>
        <translation>PA functional state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="129"/>
        <source>Шкаф 1</source>
        <translation>Cabinet 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="130"/>
        <source>Шкаф 2</source>
        <translation>Cabinet 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Управление</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Питание</source>
        <translation>Power</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Излучение ЗС1</source>
        <translation>PS1 emission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Излучение ЗС2</source>
        <translation>PS2 emission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>Калибр. ЗС1</source>
        <translation>PS1 calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>Калибр. ЗС2</source>
        <translation>PS2 calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>Нагрузка</source>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="134"/>
        <source>ПЭП</source>
        <translation>ETS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="135"/>
        <source>Фаза ЗС1</source>
        <translation>PS1 phase</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="135"/>
        <source>Фаза ЗС2</source>
        <translation>PS2 phase</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Explorer::VrlState</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="58"/>
        <source>Состояние ВРЛ</source>
        <translation>IFF state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="60"/>
        <source>Источники питания</source>
        <translation>Power supply</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="61"/>
        <source>Панель
 автоматических
 выключателей</source>
        <translation>Auto switch board</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="61"/>
        <source>Вентиляторы</source>
        <translation>Fans</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="95"/>
        <source>41М1.6
(6110)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="97"/>
        <source>KIR-1A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="42"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="62"/>
        <source>СВ КР-02
 Комплект 1</source>
        <translation>CB KP-02
Set 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="43"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="62"/>
        <source>СВ КР-02
 Комплект 2</source>
        <translation>CB KP-02
Set 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>124ПП02
 Комплект 1</source>
        <translation>124ПП02
Set 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>124ПП02
 Комплект 2</source>
        <translation>124ПП02
Set 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>ЗГ-1
 Комплект 1</source>
        <translation>ЗГ-1
Set 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="64"/>
        <source>ЗГ-2
 Комплект 2</source>
        <translation>ЗГ-2
Set 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="67"/>
        <source>ВУМ %1</source>
        <translation>OPA %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="70"/>
        <source>Перекл.
 СВЧ</source>
        <translation>SHF switch</translation>
    </message>
</context>
<context>
    <name>Diagnostic::Scene</name>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/algeria_diagnostic_scene.cpp" line="21"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/egypt_diagnostic_scene.cpp" line="31"/>
        <source>Сектор%1</source>
        <translation>Sector%1</translation>
    </message>
</context>
<context>
    <name>Graphics::Controller::Vrl::Manip</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="15"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="16"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="35"/>
        <source>Манип</source>
        <translation>Manip</translation>
    </message>
</context>
<context>
    <name>Graphics::SceneItem::Grid::SphericalLabel::Text</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/grid/spherical_grid_label_scene_item.cpp" line="29"/>
        <source>Внимание!</source>
        <translation>Warning!</translation>
    </message>
</context>
<context>
    <name>Graphics::View</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="292"/>
        <source>ОТКАЗ</source>
        <translation>Failure</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="295"/>
        <source>ГОТОВ К БОЕВОЙ РАБОТЕ</source>
        <translation>Ready for combat operation</translation>
    </message>
</context>
<context>
    <name>Gui::Dialog::GroundObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="53"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="55"/>
        <source>Отмена</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>Gui::Dialog::Monitoring</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="38"/>
        <source>Вставьте USB диск...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="239"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="307"/>
        <source>Нет данных</source>
        <translation type="unfinished">No data</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="93"/>
        <source>Ошибка монтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="96"/>
        <source>Ошибка копирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="104"/>
        <source>Ошибка размонтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="119"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="200"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="214"/>
        <source>Обработка</source>
        <translation type="unfinished">Procession</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="170"/>
        <source>Подготовка файлов...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="175"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="301"/>
        <source>скриншоты</source>
        <translation type="unfinished">screenshots</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="176"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="303"/>
        <source>переговоры</source>
        <translation type="unfinished">voice</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="177"/>
        <source>базы данных</source>
        <translation type="unfinished">database</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="243"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="308"/>
        <source>Выбрать все</source>
        <translation type="unfinished">Check all</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="244"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="309"/>
        <source>Отменить все</source>
        <translation type="unfinished">Uncheck all</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="245"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="310"/>
        <source>Обновить</source>
        <translation type="unfinished">Refresh</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="260"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="317"/>
        <source>За время</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="265"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="315"/>
        <source>От</source>
        <translation type="unfinished">From</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="266"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="316"/>
        <source>До</source>
        <translation type="unfinished">Up to</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="275"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="311"/>
        <source>Печать</source>
        <translation type="unfinished">Print</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="277"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="312"/>
        <source>Удалить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="279"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="313"/>
        <source>Копировать на USB диск</source>
        <translation type="unfinished">Copy to USB drive</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="282"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="314"/>
        <source>Выход</source>
        <translation type="unfinished">Exit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/monitoring_dialog_gui.cpp" line="305"/>
        <source>база данных</source>
        <translation type="unfinished">database</translation>
    </message>
</context>
<context>
    <name>Gui::Info</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="92"/>
        <source>АЗ: %1</source>
        <translation>AL: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="93"/>
        <source>АС: %1</source>
        <translation>AT: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="94"/>
        <source>КТ: %1</source>
        <translation>CP: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="95"/>
        <source>РЕГ: %1</source>
        <translation>LOG: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="107"/>
        <source>БАЗ: %1</source>
        <translation>ALB: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="131"/>
        <source>СЗИ: %1</source>
        <translation>EBS: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="116"/>
        <source>НзО: %1</source>
        <translation>GrO: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="100"/>
        <source>ИЗЛ:</source>
        <translation>EMIS:</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/info_gui.cpp" line="106"/>
        <source>КТ%1: %2</source>
        <translation>CP%1: %2</translation>
    </message>
</context>
<context>
    <name>Gui::Tabs::Function::Registration</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="80"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="162"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="384"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="460"/>
        <source>Воспроизведение</source>
        <translation>Display</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="278"/>
        <source>Внимание!</source>
        <translation>Warning!</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="278"/>
        <source>Время начала больше времени окончания</source>
        <translation>Time on exceeds time off</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="287"/>
        <source>Ошибка</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="287"/>
        <source>Не выбрано ни одной таблицы данных</source>
        <translation>No data chart chosen</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="322"/>
        <source>От</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="324"/>
        <source>До</source>
        <translation>Up to</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="334"/>
        <source>Точка падения баллистической цели</source>
        <translation>BT point of impact</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="336"/>
        <source>Экстраполяционные точки</source>
        <translation>Extrapolation points</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="338"/>
        <source>Выход комплекса Фундамент1,2</source>
        <translation>Foundation 1,2 complex output</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="341"/>
        <source>Выход комплекса HLCP</source>
        <translation>HLCP complex output</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="343"/>
        <source>Контроль функционирования</source>
        <translation>Operation control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="344"/>
        <source>Координатные точки</source>
        <translation>Coordinate points</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="345"/>
        <source>ЭТ с выхода ВОИ</source>
        <translation>EP from SDP output </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="83"/>
        <source>Восстановление БД</source>
        <translation>DB restore</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="375"/>
        <source>Анализ БД</source>
        <translation>DB analysis</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="122"/>
        <source>Ошибка монтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="125"/>
        <source>Ошибка копирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="133"/>
        <source>Ошибка размонтирования: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="186"/>
        <source>Выберите архив БД</source>
        <translation>Select DB file</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="186"/>
        <source>Архивы БД (*backup)</source>
        <translation>DB files (*backup)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="193"/>
        <source>Распаковка архива БД...</source>
        <translation>Unpacking DB file...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="347"/>
        <source>Запросы на опознавание РЛО MkXA</source>
        <translation>RDR identification requests РЛО MkXA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="348"/>
        <source>Отметка опознавания РЛО MkXA</source>
        <translation>РЛО MkXA identification mark</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="374"/>
        <source>Востановить БД</source>
        <translation>Restore DB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="350"/>
        <source>Запросы на опознавание РЛО RBS</source>
        <translation>RDR identification requests RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="351"/>
        <source>Отметка опознавания РЛО RBS</source>
        <translation>RBS identification mark</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="353"/>
        <source>Команды оператора</source>
        <translation>Operator`s commands</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="355"/>
        <source>Запросы на опознавание Пароль</source>
        <translation>Identification requests Password</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="356"/>
        <source>Отметка опознавания Пароль</source>
        <translation>Identification mark Password</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="361"/>
        <source>Обмен с КСА</source>
        <translation>Exchange with AEC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="363"/>
        <source>Плотность помех по частоте</source>
        <translation>Frequency jamming density</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="81"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="387"/>
        <source>Следующий обзор</source>
        <translation>Next sweep</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="82"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="390"/>
        <source>Стоп</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="478"/>
        <source>Предупреждение</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="479"/>
        <source>Допустимый интервал времени от %1 до %2</source>
        <translation>Allowable time space from %1 to %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="417"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="451"/>
        <source>Пауза</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/registration_function_gui_tab.cpp" line="442"/>
        <source>Продолжить</source>
        <translation>Further</translation>
    </message>
</context>
<context>
    <name>Gui::Tabs::Route</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="179"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="412"/>
        <source>Формуляры</source>
        <translation>RRC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="413"/>
        <source>Наведение</source>
        <translation>Guidance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="134"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="308"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="462"/>
        <source>Ед.№</source>
        <translation>UNo</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="138"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="312"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="466"/>
        <source>РКЦ</source>
        <translation>TCI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="135"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="309"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="463"/>
        <source>К</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="495"/>
        <source>Все</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="154"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="496"/>
        <source>Селекция ИКО</source>
        <translation>PPI selection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="155"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="507"/>
        <source>Класс</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="162"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="526"/>
        <source>Источник</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="548"/>
        <source>Коды ВРЛ</source>
        <translation>IFF codes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="553"/>
        <source>Не отв.</source>
        <translation>No rep.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="139"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="467"/>
        <source>Ампл.дБ
Код обн.</source>
        <translation>Ampl.dB
Code.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="144"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="318"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="472"/>
        <source>№
№
t,мм:сс</source>
        <translation>№
№
t,mm:ss</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="577"/>
        <source>Критерий</source>
        <translation>Criterion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>Новые</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>Навед</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>Цель</source>
        <translation>Target</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="581"/>
        <source>КП</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="170"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="582"/>
        <source>Запрет</source>
        <translation>Ban</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="173"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="593"/>
        <source>Азимут</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="174"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="602"/>
        <source>Дальность</source>
        <translation>Range</translation>
    </message>
</context>
<context>
    <name>Gui::Tabs::Settings::General</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="217"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="412"/>
        <source>Язык интерфейса</source>
        <translation>Interface language</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="217"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="429"/>
        <source>Система измерения</source>
        <translation>Measuring system</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="431"/>
        <source>Метрическая</source>
        <translation>Metric</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="431"/>
        <source>Англо-американская</source>
        <translation>Anglo-American</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="218"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="435"/>
        <source>Высота</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="218"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="435"/>
        <source>Дальность</source>
        <translation>Range</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="218"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="435"/>
        <source>Скорость</source>
        <translation>Velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="523"/>
        <source>Координаты маркера</source>
        <translation>Cursor coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="213"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="525"/>
        <source>Полярные</source>
        <translation>Polar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="213"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="526"/>
        <source>Декартовы</source>
        <translation>Cartesian</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="213"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="527"/>
        <source>Географические</source>
        <translation>Geographic</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="208"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="362"/>
        <source>Непрозрачность</source>
        <translation>Opacity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="364"/>
        <source>Формуляры</source>
        <translation>RRC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="529"/>
        <source>Сетка ПВО</source>
        <translation>AD grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="530"/>
        <source>WGS-84</source>
        <translation>WGS-84</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="531"/>
        <source>ПЗ-90</source>
        <translation>ПЗ-90</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="534"/>
        <source>Эллипсоид</source>
        <translation>Ellipsoid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="293"/>
        <source>Графика</source>
        <translation>Graphics</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="269"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="294"/>
        <source>Длина траекторий</source>
        <translation>Trajectory length</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="270"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="307"/>
        <source>Яркость</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="309"/>
        <source>Сетка</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="317"/>
        <source>КТ</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="327"/>
        <source>ЭТ</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="337"/>
        <source>Статика</source>
        <translation>Statics</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="220"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="347"/>
        <source>Карты</source>
        <translation>Maps</translation>
    </message>
</context>
<context>
    <name>Gui::Widget::DateTime</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="49"/>
        <source>Дата</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="52"/>
        <source>Время</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="59"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>Gui::Widget::GroundObjectCoordinate</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="36"/>
        <source>Широта</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="38"/>
        <source>Долгота</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="42"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>Gui::Widget::Zz</name>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/zz_widget_gui.cpp" line="23"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="95"/>
        <source>Рабочее место оператора</source>
        <translation>OWS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="140"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="533"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="732"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1682"/>
        <source>АФК</source>
        <translation>AFC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="137"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="533"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1682"/>
        <source>ИКО</source>
        <translation>PPI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1554"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1570"/>
        <source>Менее </source>
        <translation>Less</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1565"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1587"/>
        <source>Более </source>
        <translation>More</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="408"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1154"/>
        <source>Сетка ПВО</source>
        <translation>AD grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="538"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="684"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="803"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1366"/>
        <source>КП: ?</source>
        <translation>CC: ?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="692"/>
        <source>Управление</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="514"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="781"/>
        <source>Объекты</source>
        <translation>Objects</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="545"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="792"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1462"/>
        <source>МВ</source>
        <translation>PT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="540"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="916"/>
        <source>Мирное время</source>
        <translation>Peacetime</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="540"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="923"/>
        <source>Военное время</source>
        <translation>Wartime</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1120"/>
        <source>-1200.99 км, -1200.99 км</source>
        <translation>-1200.99 km, -1200.99 km</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="498"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1122"/>
        <source>Полярные координаты</source>
        <translation>Polar coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="498"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1123"/>
        <source>Декартовы координаты</source>
        <translation>Cartesian coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="498"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1124"/>
        <source>Географические координаты</source>
        <translation>Geographic coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="608"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1253"/>
        <source>Короткий</source>
        <translation>Short</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="609"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1259"/>
        <source>Сокращенный</source>
        <translation>Contracted</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="547"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1462"/>
        <source>ВВ</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="895"/>
        <source>9999.9 кфт</source>
        <translation>9999.9 kft</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="558"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="905"/>
        <source>В центр</source>
        <translation>To centre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="504"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="734"/>
        <source>КИ</source>
        <translation>CI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="506"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="744"/>
        <source>КТ</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="506"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="753"/>
        <source>ЭТ</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="510"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="766"/>
        <source>След</source>
        <translation>Trail</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="510"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="767"/>
        <source>ФОРМ</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="516"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1144"/>
        <source>Статика</source>
        <translation>Statics</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1148"/>
        <source>Сетка АД</source>
        <translation>AD grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1163"/>
        <source>Сектора обзора</source>
        <translation>Sweep spectrum</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1164"/>
        <source>Рубежи</source>
        <translation>Mission lines</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="519"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1165"/>
        <source>Зоны отбора</source>
        <translation>Selection zones</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="520"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1166"/>
        <source>Наземные объекты</source>
        <translation>Ground objects</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="522"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1172"/>
        <source>Сектора ЗИ ВРЛ</source>
        <translation>SR EMB sectors</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="530"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="783"/>
        <source>Селекция</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="567"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="736"/>
        <source>ИС</source>
        <translation>SS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="961"/>
        <source>По скорости</source>
        <translation>Velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="982"/>
        <source>По высоте</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="598"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1017"/>
        <source>По классу</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="600"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1030"/>
        <source>По ОГП</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1280"/>
        <source>Машинный</source>
        <translation>Electronic</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1280"/>
        <source>Единый</source>
        <translation>Unified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1282"/>
        <source>КП1</source>
        <translation>CC1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="624"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1282"/>
        <source>КП2</source>
        <translation>CC2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1284"/>
        <source>Оператор</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1284"/>
        <source>КП</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1284"/>
        <source>Универсальный</source>
        <translation>Universal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1274"/>
        <source>Номер</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="600"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1039"/>
        <source>По источникам</source>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="795"/>
        <source>Манип</source>
        <translation>Manip</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="952"/>
        <source>По номерам</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1126"/>
        <source>Координаты сетки ПВО</source>
        <translation>AD grid coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="517"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1189"/>
        <source>Карты</source>
        <translation>Maps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="524"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1175"/>
        <source>Точки падения БЦ</source>
        <translation>BM impact point</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>ОО-VII</source>
        <translation>GI-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>ИО-VII</source>
        <translation>II-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>ИО-3-VII</source>
        <translation>II-3-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="536"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="538"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="814"/>
        <source>КП%1</source>
        <translation>CC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="520"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1167"/>
        <source>Бланки автозахвата</source>
        <translation>ALBs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="611"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="788"/>
        <source>ПРК</source>
        <translation>RCS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="823"/>
        <source>ТЗИ</source>
        <translation>TMI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="836"/>
        <source>2 секунды</source>
        <translation>2 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="508"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="764"/>
        <source>Луч</source>
        <translation>Beam</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="568"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="738"/>
        <source>В/Ч</source>
        <translation>T/U</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="836"/>
        <source>5 секунд</source>
        <translation>5 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="836"/>
        <source>10 секунд</source>
        <translation>10 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="837"/>
        <source>30 секунд</source>
        <translation>30 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="616"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="837"/>
        <source>1 минута</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="617"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="837"/>
        <source>5 минут</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="597"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1245"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1726"/>
        <source>Режим</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="612"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1266"/>
        <source>Номер цели</source>
        <translation>Target number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="614"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1298"/>
        <source>Расстановка</source>
        <translation>Arrangement</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="20"/>
        <source>Номер</source>
        <translation> Number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="21"/>
        <source>Единый номер</source>
        <translation>Unified number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="23"/>
        <source>Номер от КП1</source>
        <translation>CC1 number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="24"/>
        <source>Номер от КП2</source>
        <translation>CC2 number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="26"/>
        <source>Номер борта</source>
        <translation>A/C number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="28"/>
        <source>Дальность</source>
        <translation>Range</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="29"/>
        <source>Азимут</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="30"/>
        <source>Высота измер.</source>
        <translation>Measur. alt.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="32"/>
        <source>Высота баром.</source>
        <translation>Barometr.alt.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="34"/>
        <source>Курс</source>
        <translation>Course</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="35"/>
        <source>Скорость</source>
        <translation>Velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="36"/>
        <source>Класс</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="38"/>
        <source>ОГП</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="41"/>
        <source>Запас топлива</source>
        <translation>Fuel margin</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="43"/>
        <source>Выдача на КП</source>
        <translation>Output to CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="45"/>
        <source>Источники</source>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="48"/>
        <source>Код 1</source>
        <translation>1 code </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="49"/>
        <source>Код 2</source>
        <translation>2 code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="52"/>
        <source>Код 3A</source>
        <translation>ЗА code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/total_explorer_painter.cpp" line="55"/>
        <source>Код S</source>
        <translation>S code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manage_graphics_controller.cpp" line="15"/>
        <source>ЗИ%1</source>
        <translation>EB%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="190"/>
        <source>тчк</source>
        <translation>pnt</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="190"/>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="237"/>
        <source>наз</source>
        <translation>grn</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="16"/>
        <source>Сервер не подключен</source>
        <translation>Server is not connected</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="17"/>
        <source>Действие невозможно для режима воспроизведения информации</source>
        <translation>Action impossible for display mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/process_tools.cpp" line="8"/>
        <source>Невозможно прочитать директорию &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/process_tools.cpp" line="12"/>
        <source>Директория &apos;%1&apos; только для чтения</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Thread::CopyFile</name>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="44"/>
        <source>невозможно прочитать каталог &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="52"/>
        <source>Невозможно открыть файл &apos;%1&apos; для записи</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="53"/>
        <source>Копирование файла &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/thread/copy_file_thread.cpp" line="84"/>
        <source>Ошибка CRC для файла &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Thread::RemoveFile</name>
    <message>
        <location filename="../apps/resonance/indicator/src/thread/remove_file_thread.cpp" line="26"/>
        <source>Удаление файла &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools::Manager::ContextMenu::Area</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="95"/>
        <source>Координаты курсора</source>
        <translation>Cursor coords</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="106"/>
        <source>Полярный</source>
        <translation>Polar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="107"/>
        <source>Прямоугольный</source>
        <translation>Cartesian</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="101"/>
        <source>Измерить расстояние</source>
        <translation>Measure distance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="108"/>
        <source>Бланки автозахвата</source>
        <translation>ALBs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="114"/>
        <source>Распоряжение ТР</source>
        <translation>Call request</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::ContextMenu::GroundObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="63"/>
        <source>Редактировать</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="81"/>
        <source>Наведение</source>
        <translation>Guidance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="91"/>
        <source>Координаты</source>
        <translation>Coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="101"/>
        <source>Удалить</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::ContextMenu::Target</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="144"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="152"/>
        <source>Ошибка</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="144"/>
        <source>Единый номер занят</source>
        <translation>U number busy</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="150"/>
        <source>(Р)</source>
        <translation>(M)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="152"/>
        <source>Номер оператора занят</source>
        <translation>Operator`s number busy</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="242"/>
        <source>Добавить в ТЗИ</source>
        <translation>Add to TMI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="243"/>
        <source>Удалить из ТЗИ</source>
        <translation>Delete from TMI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="270"/>
        <source>Наведение</source>
        <translation>Guidance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="282"/>
        <source>Полный формуляр</source>
        <translation>Full RC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="288"/>
        <source>Назначить класс цели</source>
        <translation>Assigh target class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Самолет</source>
        <translation>Aircraft</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Вертолет</source>
        <translation>Helicopter</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Крылатая ракета</source>
        <translation>Cruise missile</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="291"/>
        <source>Гиперзвуковая крылатая ракета</source>
        <translation>Hypersonic cruise missile</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="292"/>
        <source>Аэростат</source>
        <translation>Balloon</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="292"/>
        <source>Баллистическая цель</source>
        <translation>Ballistic target</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="383"/>
        <source>Распоряжение ТР</source>
        <translation>Call request</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="440"/>
        <source>Назначить госпринадлежность</source>
        <translation>Assign IFF attribute</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="444"/>
        <source>Свой ИО</source>
        <translation>Friendly II</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="444"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="446"/>
        <source>Свой ОО</source>
        <translation>Friend GI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="444"/>
        <source>Свой RBS</source>
        <translation>Friend RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="446"/>
        <source>Свой ГО</source>
        <translation>Friend AI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="448"/>
        <source>Чужой</source>
        <translation>Foe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="461"/>
        <source>Уточнить ГП</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="304"/>
        <source>Передача на КП</source>
        <translation>Delivery to CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="319"/>
        <source>КП %1</source>
        <translation>CC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="336"/>
        <source>Траектория</source>
        <translation>Trajectory</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="349"/>
        <source>Назначить номер</source>
        <translation>Assign number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="358"/>
        <source>Оператора</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="364"/>
        <source>Единый</source>
        <translation>Unified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="370"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="410"/>
        <source>Разъединить</source>
        <translation>Separate</translation>
        <extra-auto>submenu = _menu-&gt;addMenu(tr(&quot;Приоритет источника&quot;));</extra-auto>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="415"/>
        <source>Объединить</source>
        <translation>Unify</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="397"/>
        <source>Сбросить</source>
        <translation>Cancel tracking</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::Shortcut</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="44"/>
        <source>Выбор файла стиля</source>
        <translation>Style file choice</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="44"/>
        <source>Файлы стилей(*.qss)</source>
        <translation>Style files </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="58"/>
        <source>Рабочее место оператора</source>
        <translation>OWS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="59"/>
        <source>Версия сборки: %1</source>
        <translation>Assembly version:%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="69"/>
        <source>Доступные щрифты</source>
        <translation>Available prints</translation>
    </message>
</context>
<context>
    <name>Tools::Manager::Usb</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/usb_manager_tools.cpp" line="89"/>
        <source>операция прервана</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools::Translator</name>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="51"/>
        <source>МУ</source>
        <translation>LC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="49"/>
        <source>ЦУ</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="51"/>
        <source>ВКЛ</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="51"/>
        <source>ВЫКЛ</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="53"/>
        <source>АНТ</source>
        <translation>ANT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="53"/>
        <source>ЭКВ</source>
        <translation>EQV</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="55"/>
        <source>ПЭП1</source>
        <translation>ETS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="55"/>
        <source>ПЭП2</source>
        <translation>ETS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="57"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="57"/>
        <source>180</source>
        <translation>180</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="67"/>
        <source>%1(КП)</source>
        <translation>%1(SC)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="69"/>
        <source>%1(Р)</source>
        <translation>%1(M)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="73"/>
        <source>Т</source>
        <translation>T</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="83"/>
        <source>%1 (%2 дБ)</source>
        <translation>%1 (%2 dB)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="88"/>
        <source>Антенна</source>
        <translation>Antenna</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="88"/>
        <source>Эквивалент</source>
        <translation>Equivalent</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="124"/>
        <source>Произведено включение РЛС
Проверьте время</source>
        <translation>RDR on Check time</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="131"/>
        <source>В РЭУ 5Ц3-Е сектор %1 температура не в норме</source>
        <translation>T not normal in RED 5Ц3-E sector %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="137"/>
        <source>В РЭК 1 температура не в норме</source>
        <translation>T not normal in REC 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="139"/>
        <source>ЛИРА температура не в норме</source>
        <translation>Lira temp. not normal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="143"/>
        <source>Ошибка авторизации: &apos;пользователь не существует&apos;</source>
        <translation>Authorization error:user does not exist</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="145"/>
        <source>Ошибка авторизации: &apos;неверный пароль&apos;</source>
        <translation>Authorization error: &quot;wrong password&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="147"/>
        <source>Вы вошли как &apos;гость&apos;</source>
        <translation>You logged as &quot;guest&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="149"/>
        <source>Вы вошли как &apos;командир&apos;</source>
        <translation>You logged as &quot;commander&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="151"/>
        <source>Вы вошли как &apos;%1&apos;</source>
        <translation>You logged as %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="155"/>
        <source>Тревога</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="162"/>
        <source>Приемник %1 сектора %2</source>
        <translation>Receiver %1 sector %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="256"/>
        <source>Гражданский</source>
        <translation>Civil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="105"/>
        <source>РЛС</source>
        <translation>RDR</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="107"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="246"/>
        <source>Не определен</source>
        <translation>Unidentified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="246"/>
        <source>Х</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="248"/>
        <source>Свой ОО</source>
        <translation>Friend GI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="248"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="254"/>
        <source>П</source>
        <translation>П</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="251"/>
        <source>Свой ГО</source>
        <translation>Friend AI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="251"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="256"/>
        <source>В</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="254"/>
        <source>Свой ИО</source>
        <translation>Friendly II</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="259"/>
        <source>Чужой</source>
        <translation>Foe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="259"/>
        <source>Ч</source>
        <translation>Ч</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="167"/>
        <source>Не распознан</source>
        <translation>Unidentified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="169"/>
        <source>Самолет</source>
        <translation>Aircraft</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="169"/>
        <source>C</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="171"/>
        <source>Вертолет</source>
        <translation>Helicopter</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="173"/>
        <source>Крылатая ракета</source>
        <translation>Cruise missile</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="173"/>
        <source>КР</source>
        <translation>CM</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="175"/>
        <source>Гиперзвуковая крылатая ракета</source>
        <translation>Hypersonic cruise missile</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="175"/>
        <source>ГЗ</source>
        <translation>HCM</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="177"/>
        <source>Аэростат</source>
        <translation>Balloon</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="177"/>
        <source>А</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="179"/>
        <source>Баллистическая цель</source>
        <translation>Ballistic target</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="179"/>
        <source>БЦ</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="184"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="216"/>
        <source>КП: </source>
        <translation>SC:</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="233"/>
        <source>KIR-1A</source>
        <translation>KIR-1A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="235"/>
        <source>41М1.6
(6110)</source>
        <translation>41М1.6
(6110)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="238"/>
        <source>КД используются</source>
        <translation>CC is in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="238"/>
        <source>КД не используются</source>
        <translation>CC is not in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="239"/>
        <source>КП используются</source>
        <translation>PC is in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="239"/>
        <source>КП не используются</source>
        <translation>PC is not in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="205"/>
        <source>АФК</source>
        <translation>AFC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="207"/>
        <source>ПРК</source>
        <translation>RCS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="208"/>
        <source>РЕЖ: %1</source>
        <translation>MODE: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="214"/>
        <source>КП%1: </source>
        <translation>CC%1:</translation>
    </message>
</context>
</TS>
