source="/opt/qt/5.7.1/5.7/gcc_64/bin"
$source/lupdate -pro ../apps/resonance/indicator/pro/indicator.pro -source-language ru -target-language en -no-obsolete -ts indicator.en.ts
$source/lupdate -pro ../apps/resonance/indicator/pro/indicator.pro -source-language ru -target-language fr -no-obsolete -ts indicator.fr.ts

$source/lupdate -pro ../libs/gui/pro/gui.pro -source-language ru -target-language en -no-obsolete -ts gui.en.ts
$source/lupdate -pro ../libs/gui/pro/gui.pro -source-language ru -target-language fr -no-obsolete -ts gui.fr.ts

$source/lupdate -pro ../libs/tools/pro/tools.pro -source-language ru -target-language en -no-obsolete -ts tools.en.ts
$source/lupdate -pro ../libs/tools/pro/tools.pro -source-language ru -target-language fr -no-obsolete -ts tools.fr.ts

$source/lupdate -pro ../libs/communication/pro/communication.pro -source-language ru -target-language en -no-obsolete -ts communication.en.ts
$source/lupdate -pro ../libs/communication/pro/communication.pro -source-language ru -target-language fr -no-obsolete -ts communication.fr.ts

$source/lupdate -pro ../plugins/resonance/pro/plugins.pro -source-language ru -target-language en -no-obsolete -ts plugins.en.ts
$source/lupdate -pro ../plugins/resonance/pro/plugins.pro -source-language ru -target-language fr -no-obsolete -ts plugins.fr.ts
