<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name>QObject</name>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="429"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="490"/>
        <source>км</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="431"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="492"/>
        <source>м</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="433"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="494"/>
        <source>ММ</source>
        <translation>MM</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="427"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="488"/>
        <source>кфт</source>
        <translation>kft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="147"/>
        <source> с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="416"/>
        <source>с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="425"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="484"/>
        <source>фт</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="469"/>
        <source>м/с</source>
        <translation>m/s</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="467"/>
        <source>км/ч</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="471"/>
        <source>ММ/ч</source>
        <translation>MM/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="465"/>
        <source>фт/с</source>
        <translation>ft/s</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="463"/>
        <source>фт/ч</source>
        <translation>ft/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="486"/>
        <source>гм</source>
        <translation>hm</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="114"/>
        <source>Библиотека</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="116"/>
        <source>Имя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="118"/>
        <source>Сохранить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="218"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="231"/>
        <source>Внимание</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="232"/>
        <source>Конфигурация с таким именем уже существует. Заменить?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="475"/>
        <source>Предупреждение</source>
        <translation type="unfinished">Warning</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="476"/>
        <source>Выражение содержит несуществующий фильтр</source>
        <translation type="unfinished">Expression contains non-existent filter</translation>
    </message>
</context>
<context>
    <name>Tools::Filter::Widget</name>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="22"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="128"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="133"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="137"/>
        <source>Предупреждение</source>
        <translation type="unfinished">Warning</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="22"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="133"/>
        <source>Фильтр некорректный</source>
        <translation type="unfinished">Incorrect filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="137"/>
        <source>Фильтр уже существует</source>
        <translation type="unfinished">Filter already exists</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="49"/>
        <source>Выражение</source>
        <translation type="unfinished">Expression</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="55"/>
        <source>Все И</source>
        <translation type="unfinished">All and</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="59"/>
        <source>Все ИЛИ</source>
        <translation type="unfinished">All or</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="73"/>
        <source>Поле</source>
        <translation type="unfinished">Field</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="77"/>
        <source>Оператор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="85"/>
        <source>Значение</source>
        <translation type="unfinished">Value</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="94"/>
        <source>Добавить фильтр</source>
        <translation type="unfinished">Add filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="98"/>
        <source>Обновить фильтр</source>
        <translation type="unfinished">Update filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="102"/>
        <source>Удалить фильтр</source>
        <translation type="unfinished">Delete filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="106"/>
        <source>Clear</source>
        <translation type="unfinished">Clear</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="128"/>
        <source>Фильтр не выбран</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
