<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr" sourcelanguage="ru">
<context>
    <name>QObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manage_graphics_controller.cpp" line="15"/>
        <source>ЗИ%1</source>
        <translatorcomment>Interdiction d&apos;émission</translatorcomment>
        <translation>IE%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="195"/>
        <source>тчк</source>
        <translation>point</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="195"/>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="244"/>
        <source>наз</source>
        <translation>désign</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="21"/>
        <source>Номер</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="371"/>
        <source>Ширина</source>
        <translation>Largeur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="191"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="268"/>
        <source>%1 МГц</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="100"/>
        <source>Действие невозможно</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/bearing/explorer_bearing_monitoring.cpp" line="101"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="193"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="270"/>
        <source>%1 дБ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="238"/>
        <source>Вывести на ИКО</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="239"/>
        <source>Пеленгование</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="243"/>
        <source>Назначить рабочей ЗС1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="247"/>
        <source>Назначить рабочей ЗС2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/peleng/total_peleng_explorer_painter.cpp" line="22"/>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/peleng/total_peleng_explorer_painter.cpp" line="23"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="371"/>
        <source>МГц</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/peleng/total_peleng_explorer_painter.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="373"/>
        <source>дБ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="22"/>
        <source>Единый номер</source>
        <translation>Numéro uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="24"/>
        <source>Номер от КП1</source>
        <translation>Numéro par PC1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="25"/>
        <source>Номер от КП2</source>
        <translation>Numéro par PC2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="27"/>
        <source>Номер борта</source>
        <translation>Numéro de série</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="175"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="629"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="207"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="332"/>
        <source>Дальность</source>
        <translation>Portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="174"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="620"/>
        <source>Азимут</source>
        <translation>Azimut</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="31"/>
        <source>Высота измер.</source>
        <translation>Altitude mesuré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="33"/>
        <source>Высота баром.</source>
        <translation>Altitude barométr.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="35"/>
        <source>Курс</source>
        <translation>Cap</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="36"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="207"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="208"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="332"/>
        <source>Скорость</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="156"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="534"/>
        <source>Класс</source>
        <translation>Classe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="39"/>
        <source>ОГП</source>
        <translatorcomment>Identification de nationalité</translatorcomment>
        <translation>IdN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="42"/>
        <source>Запас топлива</source>
        <translation>Capacité de combustible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="44"/>
        <source>Выдача на КП</source>
        <translation>Fourniture au PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="46"/>
        <source>Источники</source>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="49"/>
        <source>Код 1</source>
        <translation>Code 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="50"/>
        <source>Код 2</source>
        <translation>Code 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="53"/>
        <source>Код 3A</source>
        <translation>Code 3A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="53"/>
        <source>Код S</source>
        <translation>Code S</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="222"/>
        <source>Все действующие бланки АЗ будут удалены. Продолжить?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="288"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="429"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="349"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="387"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="477"/>
        <source>Режим 1</source>
        <translation>Mode 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="289"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="430"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="31"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="95"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="350"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="388"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="477"/>
        <source>Режим 2</source>
        <translation>Mode 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="292"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="464"/>
        <source>ВР</source>
        <translation>Radar secondaire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="294"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="579"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="201"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="426"/>
        <source>Полярные</source>
        <translation>Polaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="295"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="581"/>
        <source>Прямоугольные</source>
        <translation>Rectangulaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="315"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="474"/>
        <source>Питание УМ</source>
        <translation>Alimentation de l&apos;AE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="315"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="476"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="495"/>
        <source>Излучение УМ</source>
        <translation>Émission de l&apos;AE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="501"/>
        <source>У</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="502"/>
        <source>Ш</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1212"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="329"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="491"/>
        <source>Круговой</source>
        <translation>Circulaire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1221"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1225"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="326"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="500"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="504"/>
        <source>Режимы запросов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="319"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="524"/>
        <source>Установка частот</source>
        <translation>Calage de fréquences</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="525"/>
        <source>Назначенные частоты</source>
        <translation>Fréquences dédiées</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="526"/>
        <source>Авт.</source>
        <translation>Auto.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="527"/>
        <source>Ручн.</source>
        <translation>Manu.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="569"/>
        <source>Сортировка частот</source>
        <translation>Tri des fréquences</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="321"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="578"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="646"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1423"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="116"/>
        <source>Бланки автозахвата</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="325"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="570"/>
        <source>шум</source>
        <translation>Bruit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="326"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="572"/>
        <source>номер</source>
        <translation>numéro</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <source>MK1</source>
        <translation>MK1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <source>MK2</source>
        <translation>MK2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1226"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="505"/>
        <source>RBS-3A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1226"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="505"/>
        <source>RBS-C</source>
        <translation>RBS-C</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1226"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="505"/>
        <source>RBS-S</source>
        <translation>RBS-S</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="140"/>
        <source>Вставьте USB диск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="206"/>
        <source>Удаление файлов...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="77"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="251"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="306"/>
        <source>Нет данных</source>
        <translation>Données non disponibles</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="152"/>
        <source>Поиск файлов...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="172"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="300"/>
        <source>скриншоты</source>
        <translation>captures d&apos;écran</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="173"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="302"/>
        <source>переговоры</source>
        <translation>conversations</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="174"/>
        <source>базы данных</source>
        <translation>bases de données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="195"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="83"/>
        <source>Копирование файлов...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="202"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="220"/>
        <source>Извлечение USB диска...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="255"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="307"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="332"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="370"/>
        <source>Выбрать все</source>
        <translation>Sélectionner tout</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="256"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="308"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="333"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="371"/>
        <source>Отменить все</source>
        <translation>Annuler tout</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="257"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="309"/>
        <source>Обновить</source>
        <translation>Rafraîchir</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="274"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="310"/>
        <source>Печать</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="276"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="311"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="318"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="368"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="110"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="87"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="161"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="583"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="434"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="114"/>
        <source>Удалить</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="278"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="312"/>
        <source>Копировать на USB диск</source>
        <translation>Copier sur le disque USB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="281"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="211"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="339"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="373"/>
        <source>Выход</source>
        <translation>Sortie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="304"/>
        <source>база данных</source>
        <translation>base de données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="124"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="646"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1422"/>
        <source>Наземные объекты</source>
        <translation>Objets terrestres</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="166"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="584"/>
        <source>Удалить все</source>
        <translation>Supprimer tout</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="151"/>
        <source>3D-Радар</source>
        <translation>3D-Radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="152"/>
        <source>2D-Радар</source>
        <translation>2D-Radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>КП</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="154"/>
        <source>Аэродром</source>
        <translation>Aérodrome</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="338"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="372"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="437"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="470"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="86"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="298"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="585"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="238"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="109"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="188"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="146"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="227"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="360"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="491"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="305"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="437"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="60"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="42"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/zz_widget_gui.cpp" line="23"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="419"/>
        <source>Применить</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="472"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="186"/>
        <source>Отмена</source>
        <translation>Annulation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="20"/>
        <source>Невозможно в военное время</source>
        <translation>Impossible en période de guerre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="21"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="100"/>
        <source>У вас недостаточно прав</source>
        <translation>Vos droits sont insuffisants</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="22"/>
        <source>Действие невозможно для режима просмотра</source>
        <translation>Cette action est impossible en mode de visualisation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="23"/>
        <source>Сервер не подключен</source>
        <translation>Serveur n&apos;est pas connecté</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="24"/>
        <source>Действие невозможно для режима воспроизведения информации</source>
        <translation>Cette action est impossible en mode de lecture des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="104"/>
        <source>Назад</source>
        <translation>En avant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="106"/>
        <source>Вперед</source>
        <translation>En arrière</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="108"/>
        <source>Домой</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="116"/>
        <source>Общее
состояние</source>
        <translation>État général</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="117"/>
        <source>Состояние
УМ</source>
        <translation>État de l&apos;amplificateur d&apos;émission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="119"/>
        <source>Краткое состояние
приемников</source>
        <translation>État des récepteurs (en bref)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="121"/>
        <source>Полное состояние
приемников</source>
        <translation>État des récepteurs (complet)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="77"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="122"/>
        <source>Панорама шумов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="79"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="124"/>
        <source>Панорама пеленга</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="58"/>
        <source>Канал</source>
        <translation>Voie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="59"/>
        <source>До калибровки</source>
        <translation>Avant étalonnage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="60"/>
        <source>После калибровки</source>
        <translation>Après étalonnage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>Шум, дБ</source>
        <translation>Bruit, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Амплитуда, дБ</source>
        <translation>Amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Фаза, гр</source>
        <translation>Phase, degrés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>СКО ампл., дБ</source>
        <translation>Erreur moyenne quadratique de l&apos;amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>СКО фазы, гр</source>
        <translation>Erreur moyenne quadratique de la phase, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>АФЮС ампл., дБ</source>
        <translation>Oscillateur  autonome de signal d&apos;ajustement d&apos;amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>АФЮС фаза., гр</source>
        <translation>Oscillateur autonome de signal d&apos;ajustement de phase, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон ампл., дБ</source>
        <translation>Mesure-étalon d&apos;amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон фазы., гр</source>
        <translation>Mesure-étalon de phase, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка ампл., дБ</source>
        <translation>Correction amplitude, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка фазы., гр</source>
        <translation>Correction phase, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="59"/>
        <source>Направление</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="69"/>
        <source>Тип</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="71"/>
        <source>Азимутальная</source>
        <translation>Azimutal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="71"/>
        <source>Угломестная</source>
        <translation>de site</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="73"/>
        <source>Контроль по</source>
        <translation>Contôle par</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="697"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="917"/>
        <source>ИС</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="75"/>
        <source>АФЮС</source>
        <translation>Oscillateur  autonome de signal d&apos;ajustement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="77"/>
        <source>Частота, МГц</source>
        <translation>Fréquence, MHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="109"/>
        <source>Рассчитать</source>
        <translation>Calculer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="88"/>
        <source>Сопряжение с КП%1</source>
        <translation>Interfaçage avec le PC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="103"/>
        <source>Состояние АПД</source>
        <translation>État des équipements de transmission des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="104"/>
        <source>Готовность</source>
        <translation>Disponibilité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="107"/>
        <source>Физическое
соединение</source>
        <translation>Connexion physique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="108"/>
        <source>Логическое
соединение</source>
        <translation>Connexion logique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="95"/>
        <source>Состояние связи</source>
        <translation>État de liaison</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="96"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="265"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="387"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="182"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="190"/>
        <source>Командир</source>
        <translation>Commandant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="97"/>
        <source>Инициализация
передачи</source>
        <translation>Initialisation de transmission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="98"/>
        <source>Прием
сообщений</source>
        <translation>Réception des messages</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="60"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="150"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="172"/>
        <source>Синхронизация
времени</source>
        <translation>Synchronisation du temps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="100"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="174"/>
        <source>Доставка
сообщений</source>
        <translation>Acheminement des communications</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="106"/>
        <source>Прием
данных</source>
        <translation>Réception des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="105"/>
        <source>Передача
данных</source>
        <translation>Transmission des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="158"/>
        <source>Тип протокола: %1</source>
        <translation>Type de protocole: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="159"/>
        <source>Принято пакетов: %1</source>
        <translation>Paquets récus : %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="160"/>
        <source>Ошибок приема: %1</source>
        <translation>Erreurs de réception: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="161"/>
        <source>Выдано пакетов: %1</source>
        <translation>Paquets transmis: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="162"/>
        <source>Ошибок выдачи: %1</source>
        <translation>Erreurs de transmission: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="163"/>
        <source>Просрочено пакетов: %1</source>
        <translation>Paquets surannés: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="164"/>
        <source>Задержка: %1 мс</source>
        <translation>Délai: %1 мс</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="79"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/russia_general_diagnostic_explorer.cpp" line="25"/>
        <source>Сообщения об отказах</source>
        <translation>Message de défauts</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="81"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="32"/>
        <source>РМО1</source>
        <translation>PTO1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="82"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="33"/>
        <source>РМО2</source>
        <translation>PTO2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="83"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="34"/>
        <source>ВРМО</source>
        <translation>Console déportée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="31"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="92"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="36"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="749"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1089"/>
        <source>Сервер</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="32"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="95"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="37"/>
        <source>ИБП1</source>
        <translation>Onduleur1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="96"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="38"/>
        <source>ИБП2</source>
        <translation>Onduleur2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="40"/>
        <source>АПД1</source>
        <translation>ETD1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="66"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="41"/>
        <source>АПД2</source>
        <translation>ETD2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="93"/>
        <source>Антенна</source>
        <translation>Antenne</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="93"/>
        <source>Эквивалент</source>
        <translation>Équivalent</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="73"/>
        <source>Приемник%1</source>
        <translation>Récepteur%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="58"/>
        <source>УМ%1</source>
        <translation>AE%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="67"/>
        <source>Антенна Az</source>
        <translation>Antenne Az</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="70"/>
        <source>Антенна E</source>
        <translation>Antenne Gis</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="20"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="291"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="444"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="43"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="112"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="51"/>
        <source>ЦОС%1</source>
        <translation>TNS%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="61"/>
        <source>Приемник%1 (f1)</source>
        <translation>Récepteur%1 (f1)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="64"/>
        <source>Приемник%1 (f2)</source>
        <translation>Récepteur%1 (f2)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="39"/>
        <source>Цифровой
приемник</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="42"/>
        <source>АПД3</source>
        <translation>ETD3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="81"/>
        <source>Мнемосхема шкафа приемника сектора %1 РЭМ 3 5ЦП1-01</source>
        <translation>Diagramme mnémonique du rack du récepteur du secteur %1 РЭМ 3 5ЦП1-01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="103"/>
        <source>АА%1
5ЦП101Б%2</source>
        <translation>АА%1
5ЦП101Б%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="107"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="108"/>
        <source>Плата управления АА2</source>
        <translation>Carte de commande AA2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="112"/>
        <source>АА%1
Узел питания %2</source>
        <translation>АА%1
Bloc d&apos;alimentation %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="120"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="121"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="181"/>
        <source>Азимутальные каналы</source>
        <translation>Voies azimutales</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="122"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="123"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="182"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="183"/>
        <source>Угломестные каналы</source>
        <translation>Voies en site</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <source>Внутр. шум дБ</source>
        <translation>Bruit propre, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <source>Внутр. ампл. дБ</source>
        <translation>Amplitude interne, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <source>Внутр. фаза гр</source>
        <translation>Phase propre, degré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="188"/>
        <source>Внеш. шум дБ</source>
        <translation>Bruit ambiant, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="188"/>
        <source>Внеш. ампл. дБ</source>
        <translation>Amplitude externe, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="97"/>
        <source>Состояние усилителя мощности сектора %1</source>
        <translation>État de l&apos;amplificateur d&apos;émission pour le secteur %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="106"/>
        <source>Техническое состояние ТЭЗ УМ</source>
        <translation>État technique du bloc de réserve de l&apos;AE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>5Ц300Я01</source>
        <translation>5Ц300Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>БКУ 5Ц301Б03</source>
        <translation>BCC 5Ц301Б03</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>БУМ 5Ц301Б01 (верх)</source>
        <translation>BAE 5Ц301Б01 (haut)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>БУМ 5Ц301Б01 (низ)</source>
        <translation>BAE 5Ц301Б01 (bas)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>БСДНО 5Ц301Б02</source>
        <translation>BDSCD 5Ц301Б02</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>БП 5Ц301Б05</source>
        <translation>БП 5Ц301Б05</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>БФГ 5Ц301Б04</source>
        <translation>BFAH 5Ц301Б04</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>5Ц301Я01</source>
        <translation>5Ц301Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="112"/>
        <source>Узел распределения пит.</source>
        <translation>Unité de distribution alim.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="112"/>
        <source>Узел воздушного охл.</source>
        <translation>Unité de refroidissement par air.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="116"/>
        <source>%1 канал</source>
        <translation>%1 voie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>%1А%2</source>
        <translation>%1А%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>1А8</source>
        <translation>1А8</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>1А11</source>
        <translation>1A11</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>1А7</source>
        <translation>1A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>2А7</source>
        <translation>2A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="131"/>
        <source>Функциональное состояние УМ</source>
        <translation>État fonctionnel de l&apos;AE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="132"/>
        <source>Шкаф 1</source>
        <translation>Armoire 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Шкаф 2</source>
        <translation>Armoire 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="870"/>
        <source>Управление</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="324"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="439"/>
        <source>Питание</source>
        <translation>Alimentation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <source>Излучение ЗС1</source>
        <translation>Émission de l&apos;ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <source>Излучение ЗС2</source>
        <translation>Émission de l&apos;ID2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>Калибр. ЗС1</source>
        <translation>Étallonage de l&apos;ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>Калибр. ЗС2</source>
        <translation>Étallonage de l&apos;ID2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>Нагрузка</source>
        <translation>Charge</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>ПЭП</source>
        <translation>Commutation de sous-gammes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="138"/>
        <source>Фаза ЗС1</source>
        <translation>Phase de l&apos;ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="138"/>
        <source>Фаза ЗС2</source>
        <translation>Phase de l&apos;ID2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="61"/>
        <source>Состояние ВРЛ</source>
        <translation>État de IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>Источники питания</source>
        <translation>Alimentateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="64"/>
        <source>Панель
 автоматических
 выключателей</source>
        <translation>Tableau
 des disjoncteurs 
automatiques</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="64"/>
        <source>Вентиляторы</source>
        <translation>Ventilateurs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="99"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="297"/>
        <source>41М1.6
(6110)</source>
        <translation>41М1.6
(6110)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="101"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="295"/>
        <source>KIR-1A</source>
        <translation>KIR-1A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="65"/>
        <source>СВ КР-02
 Комплект 1</source>
        <translation>Calculatrice spéciale КR-02
 Jeu 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="65"/>
        <source>СВ КР-02
 Комплект 2</source>
        <translation>Calculatrice spéciale КR-02
 Jeu 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="66"/>
        <source>124ПП02
 Комплект 1</source>
        <translation>124ПП02
 Jeu 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="66"/>
        <source>124ПП02
 Комплект 2</source>
        <translation>124ПП02
 Jeu 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="66"/>
        <source>ЗГ-1
 Комплект 1</source>
        <translation>OP-1
 Jeu 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="67"/>
        <source>ЗГ-2
 Комплект 2</source>
        <translation>OP-2
Jeu 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="70"/>
        <source>ВУМ %1</source>
        <translation>APT %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="73"/>
        <source>Перекл.
 СВЧ</source>
        <translation>Commutateur
 HF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/algeria_diagnostic_scene.cpp" line="21"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/base_diagnostic_scene.cpp" line="119"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/egypt_diagnostic_scene.cpp" line="40"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="300"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="532"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="190"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="246"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="450"/>
        <source>Сектор%1</source>
        <translation>Secteur%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="17"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="18"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="330"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="493"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="523"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="997"/>
        <source>Манип</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="330"/>
        <source>ОТКАЗ</source>
        <translation>REFUS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="333"/>
        <source>ГОТОВ К БОЕВОЙ РАБОТЕ</source>
        <translation>PRÊT AU FONCTIONNEMENT OPÉRATIONNEL</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/grid/spherical_grid_label_scene_item.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="234"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="309"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="329"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="338"/>
        <source>Внимание!</source>
        <translation>Attention !</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="20"/>
        <source>Редактор карт</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="39"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="608"/>
        <source>Поиск доступных файлов...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="95"/>
        <source>Файл &apos;%1&apos; существует. Заменить?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="192"/>
        <source>Карты для импорта</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="193"/>
        <source>Текущие карты</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="207"/>
        <source>Поиск</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="209"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="319"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="369"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="109"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="85"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="157"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="312"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="431"/>
        <source>Добавить</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="310"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="107"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="191"/>
        <source>Имя</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="339"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="36"/>
        <source>Широта</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="340"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="38"/>
        <source>Долгота</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="260"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="267"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="274"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="321"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="156"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="164"/>
        <source>Ошибка</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="260"/>
        <source>Имя пользователя не может быть пустым</source>
        <translation>Nom d&apos;utilisateur ne peut pas être vide</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="265"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="400"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="134"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="148"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="187"/>
        <source>Гость</source>
        <translation>Visiteur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="267"/>
        <source>Недопустимое имя пользователя</source>
        <translation>Nom d&apos;ulisateur inadmissible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="274"/>
        <source>Пользователь с таким именем уже существует</source>
        <translation>Ce nom d&apos;utilisateur existe déjà</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="305"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="197"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="294"/>
        <source>Пользователи</source>
        <translation>Utilisateurs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="311"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="191"/>
        <source>Пароль</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <source>Права</source>
        <translation>Droits</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="389"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="413"/>
        <source>в сети</source>
        <translation>en ligne</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="431"/>
        <source>Установка параметров обработки сигнала</source>
        <translation>Sélection des paramètres du traitement du signal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="433"/>
        <source>Выбор рабочих частот</source>
        <translation>Sélection des fréquences opérationnelles</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="435"/>
        <source>Включение автоматической смены частот</source>
        <translation>Activation du changement automatique de fréquence</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="437"/>
        <source>Установка режимов защиты от помех</source>
        <translation>Sélection des modes de protection contre les brouillages</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="438"/>
        <source>Установка бланков ЗАЗ</source>
        <translation>Etablissement des zones IAAC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="440"/>
        <source>Присвоение ВО признака госопознавания</source>
        <translation>Attribution de la nationalité à OA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="442"/>
        <source>Присвоение ВО класса цели</source>
        <translation>Attribution de la classe de cible à OA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="443"/>
        <source>Смена номера ВО</source>
        <translation>Changement du numéro de OA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="444"/>
        <source>Работа с БД</source>
        <translation>Travail avec la BD</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="446"/>
        <source>Смена кодов аппаратуры 41М1(6)</source>
        <translation>Changement des codes de l&apos;appareillage 41M1(6)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="447"/>
        <source>Настройка режимов Пароля</source>
        <translation>Réglage des modes de mot de passe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="450"/>
        <source>Выбор комплекта аппаратуры ВРЛ</source>
        <translation>Sélection de l&apos;ensemble d&apos;appareils de IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="130"/>
        <source>Включить</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="130"/>
        <source>Выключить</source>
        <translation>Désactiver</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="380"/>
        <source>От</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="382"/>
        <source>До</source>
        <translation>à</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="393"/>
        <source>Точка падения баллистической цели</source>
        <translation>Point d&apos;impact de cible ballistique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="395"/>
        <source>Экстраполяционные точки</source>
        <translation>Points d&apos;extrapolation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="400"/>
        <source>Выход комплекса HLCP</source>
        <translation>Sortie du complexe  HLCP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="402"/>
        <source>Контроль функционирования</source>
        <translation>Contrôle du fonctionnement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="403"/>
        <source>Координатные точки</source>
        <translation>Points de coordonnées</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="404"/>
        <source>КТ с выхода ВОИ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="409"/>
        <source>Запросы на опознавание РЛО RBS</source>
        <translation>Demandes de l&apos;identification radar RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="410"/>
        <source>Отметка опознавания РЛО RBS</source>
        <translation>Marquer de l&apos;identification radar RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="412"/>
        <source>Команды оператора</source>
        <translation>Commandes de l&apos;opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="420"/>
        <source>Обмен с КСА</source>
        <translation>Échanges avec les automatismes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="422"/>
        <source>Плотность помех по частоте</source>
        <translation>Densité des brouillages sur la fréquence</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="432"/>
        <source>Создать описание</source>
        <translation>Créer une description</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="435"/>
        <source>Описание базы данных</source>
        <translation>Description de la base de données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="439"/>
        <source>Файлы данных</source>
        <translation>Fichiers des données</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="77"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="446"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="541"/>
        <source>Воспроизведение</source>
        <translation>Lecture</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="78"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="449"/>
        <source>Следующий обзор</source>
        <translation>Balayage suivant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="79"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="452"/>
        <source>Стоп</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="148"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="162"/>
        <source>Нет доступных файлов</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="234"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="309"/>
        <source>Время начала больше времени окончания</source>
        <translation>Temps du début est supérieur du temps de la fin</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="321"/>
        <source>Не выбрано ни одной таблицы данных</source>
        <translation>Aucune table de données n&apos;est choisie</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="330"/>
        <source>Хотите создать файл описания?</source>
        <translation>Voulez-vous créer un fichier de description?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="331"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="64"/>
        <source>Да</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="332"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="65"/>
        <source>Нет</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="338"/>
        <source>Файл описания создан не будет</source>
        <translation>Le fichier de description ne sera pas créé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="397"/>
        <source>Выход комплекса Фундамент1,2</source>
        <translation>Sortie du complexe Fondation 1,2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="406"/>
        <source>Запросы на опознавание РЛО MkXA</source>
        <translation>Demandes de l&apos;identification radar MkXA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="407"/>
        <source>Отметка опознавания РЛО MkXA</source>
        <translation>Marquer de l&apos;identification MkXA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="414"/>
        <source>Запросы на опознавание Пароль</source>
        <translation>Demande de l&apos;identification Mot de passe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="415"/>
        <source>Отметка опознавания Пароль</source>
        <translation>Marquer de l&apos;identification Mot de passe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="496"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="532"/>
        <source>Пауза</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="523"/>
        <source>Продолжить</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="560"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="221"/>
        <source>Предупреждение</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="561"/>
        <source>Допустимый интервал времени от %1 до %2</source>
        <translation>Domaine de temps admissible de %1 à %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="592"/>
        <source>Скрипт запущен</source>
        <translation>Script est démarré</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="597"/>
        <source>Скрипт уже выполняется. Дождитесь завершения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="600"/>
        <source>Скрипт прерван</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="602"/>
        <source>Скрипт завершился с ошибкой</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="614"/>
        <source>Получен неизвестный ответ от скрипта &apos;%1&apos;</source>
        <translation>Une réponse inconnue est reçue du script &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="62"/>
        <source>Требуется подтверждение действия</source>
        <translation>Confirmation de l&apos;action est nécessaire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="49"/>
        <source>Вы действительно хотите произвести выключение РМО?</source>
        <translation>Voulez-vous vraiment de déshabiliterle PTO?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="63"/>
        <source>Вы действительно хотите произвести выключение РЛС?</source>
        <translation>Voulez-vous vraiment de déshabiliter le radar?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="159"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="109"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="643"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="972"/>
        <source>Статика</source>
        <translation>Statique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="109"/>
        <source>Регистрация</source>
        <translation>Enregistrement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="109"/>
        <source>Тренаж</source>
        <translation>Entrainement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="97"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="126"/>
        <source>Выключение РЛС</source>
        <translation>Déshabilitation вг radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="124"/>
        <source>Выключение РМО</source>
        <translation>Déshabilitation du PTO</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Режимы</source>
        <translation>Modes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Обработка</source>
        <translation>Traitement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Функции</source>
        <translation>Fonctions</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Настройки</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="188"/>
        <source>Зоны режекции</source>
        <translation>Zones de la réjection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="191"/>
        <source>Доплеровская скорость (0-100), м/с</source>
        <translation>Vitesse Doppler (0-100), m/sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="50"/>
        <source>ЗС%1</source>
        <translation>ID%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="228"/>
        <source>Пороги обнаружения (до 25.5), дБ</source>
        <translation>Limites de détection (jusqu&apos;à 25.5), dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="232"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="247"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="396"/>
        <source>ЗС1</source>
        <translation>ID1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="232"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="248"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="396"/>
        <source>ЗС2</source>
        <translation>ID2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="244"/>
        <source>Защита от активных помех</source>
        <translation>Protection contres les brouillages actifs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="60"/>
        <source>Дальность режекции</source>
        <translation>Portée de la réjection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="252"/>
        <source>РНИП</source>
        <translation>RBIA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="66"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="253"/>
        <source>РП</source>
        <translation>RB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="254"/>
        <source>ШОС</source>
        <translation>ELB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="193"/>
        <source>Дальность режекции (0-1200), км</source>
        <translation>Portée de la réjection (0-1200), km</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="135"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="330"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="488"/>
        <source>Ед.№</source>
        <translation>No uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="331"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="489"/>
        <source>К</source>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="139"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="334"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="492"/>
        <source>РКЦ</source>
        <translation>Identification de classe de la cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="140"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="335"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="493"/>
        <source>Ампл.дБ
Код обн.</source>
        <translation>Ampl.dB
Code raffr.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="145"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="340"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="498"/>
        <source>№
№
t,мм:сс</source>
        <translation>№
№
t,mm:ss</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="154"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="522"/>
        <source>Все</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="155"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="523"/>
        <source>Селекция ИКО</source>
        <translation>Sélection de l&apos;indicateur panoramique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="163"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="553"/>
        <source>Источник</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="169"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="575"/>
        <source>Коды ВРЛ</source>
        <translation>Codes IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="169"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="580"/>
        <source>Не отв.</source>
        <translation>Pas de réps.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="604"/>
        <source>Критерий</source>
        <translation>Critère</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <source>Новые</source>
        <translation>Nouveaux</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <source>Навед</source>
        <translation>Guidage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <source>Цель</source>
        <translation>Cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="609"/>
        <source>Запрет</source>
        <translation>Interdiction</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="437"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/bearing_action_widget_gui.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="147"/>
        <source>Формуляры</source>
        <translation>Formulaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="181"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="438"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="89"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="296"/>
        <source>Наведение</source>
        <translation>Guidage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="100"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="669"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="671"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1017"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="357"/>
        <source>КП%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="161"/>
        <source>Управление АПД</source>
        <translation>Gestion des ETD</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="163"/>
        <source>Скорость передачи</source>
        <translation>Taux de transmission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="170"/>
        <source>Уровень передачи</source>
        <translation>Niveau de transmission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="179"/>
        <source>ПСМ, сек</source>
        <translation>Intervalle de tansmission de marqueur Nord</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="106"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="182"/>
        <source>Скремблирование</source>
        <translation>Scrambling</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="107"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="183"/>
        <source>Адаптация корректора</source>
        <translation>Adaptation de correcteur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="110"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="184"/>
        <source>Переподключение</source>
        <translation>Réconnexion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="142"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="161"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="674"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1152"/>
        <source>Мирное время</source>
        <translation>Temps de paix</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="143"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="164"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="674"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1159"/>
        <source>Военное время</source>
        <translation>Temps de guerre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="144"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="223"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="251"/>
        <source>Запретить</source>
        <translation>Interdire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="145"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="225"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="253"/>
        <source>Разрешить</source>
        <translation>Autoriser</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="198"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="328"/>
        <source>Метрическая</source>
        <translation>Métrique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="198"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="328"/>
        <source>Англо-американская</source>
        <translation>Anglo-américaine</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="201"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="427"/>
        <source>Декартовы</source>
        <translation>Cartésiennes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="201"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="428"/>
        <source>Географические</source>
        <translation>Géographiques</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="205"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="293"/>
        <source>Авторизация</source>
        <translation>Autorisation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="206"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="296"/>
        <source>Наименование подразделения</source>
        <translation>Nom de l&apos;unité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="206"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="308"/>
        <source>Язык интерфейса</source>
        <translation>Langue de l&apos;interface</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="206"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="326"/>
        <source>Система измерения</source>
        <translation>Système de mesure</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="207"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="332"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="341"/>
        <source>Высота</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="208"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="424"/>
        <source>Координаты маркера</source>
        <translation>Coordonnées du marqueur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="141"/>
        <source>Сетка</source>
        <translation>Grille</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="145"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="635"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="925"/>
        <source>КТ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="635"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="934"/>
        <source>ЭТ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="154"/>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="176"/>
        <source>Пеленг</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="163"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="751"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="975"/>
        <source>Карты</source>
        <translation>Cartes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="168"/>
        <source>Непрозрачность формуляров</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="107"/>
        <source>Длина траекторий</source>
        <translation>Longueur des trajectoires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="125"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/bearing_action_widget_gui.cpp" line="118"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="236"/>
        <source>Яркость</source>
        <translation>Luminosité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="430"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="536"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1409"/>
        <source>Сетка ПВО</source>
        <translation>Grille DCA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="432"/>
        <source>WGS-84</source>
        <translation>WGS-84</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="433"/>
        <source>ПЗ-90</source>
        <translation>ПЗ-90</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="436"/>
        <source>Эллипсоид</source>
        <translation>Ellipsoïde</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="80"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="103"/>
        <source>Общие</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="110"/>
        <source>РЛС</source>
        <translation>Radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="73"/>
        <source>Сопряж</source>
        <translation>Interfaçage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="73"/>
        <source>Частоты</source>
        <translation>Fréquences</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="27"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="93"/>
        <source>Модуль ВОИ</source>
        <translation>Module TSD</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="96"/>
        <source>&quot;Короткий&quot;
критерий</source>
        <translation>Critère
&quot;court&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="97"/>
        <source>&quot;Длинный&quot;
критерий</source>
        <translation>Critère
&quot;long&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="34"/>
        <source>Макс. скорость</source>
        <translation>Vitesse maximale</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="99"/>
        <source>Время
экстр-ции, с</source>
        <translation>Délai de
l&apos;extrapolation, sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="101"/>
        <source>из</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="104"/>
        <source>ПБЛ</source>
        <translation>Suppression du lobe latéral</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="102"/>
        <source>Азимут, дБ</source>
        <translation>Azimut, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="103"/>
        <source>Дальность, дБ</source>
        <translation>Portée, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="143"/>
        <source>Макс.
скорость, </source>
        <translation>Vitesse
max., </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="217"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="231"/>
        <source>%1 Гц</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="221"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="233"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="235"/>
        <source>%1 сек</source>
        <translation>%1 sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="374"/>
        <source>Параметры ЗС и обработки</source>
        <translation>Paramètres de l&apos;ID et du traitement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Период</source>
        <translation>Période</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Кол-во ТКН</source>
        <translation>Nobre des CAC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Кол-во ТНН</source>
        <translation>Nobre des CANC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Длительн. ЗС</source>
        <translation>Durée de l&apos;ID</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Перед. фронт</source>
        <translation>Front avant</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="377"/>
        <source>Задний фронт</source>
        <translation>Front arrière</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="377"/>
        <source>Бланк нач. кан. Д</source>
        <translation>Fiche de la voie initiale de la portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="377"/>
        <source>Кол-во кан. Д</source>
        <translation>Nombre des voies de la portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="448"/>
        <source>Амплитуды ЗС</source>
        <translation>Amplitudes de l&apos;ID</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <source>Сектор 1</source>
        <translation>Secteur 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <source>Сектор 2</source>
        <translation>Secteur 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <source>Сектор 3</source>
        <translation>Secteur 3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <source>Сектор 4</source>
        <translation>Secteur 4</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="456"/>
        <source>ЗС1
ПЭП1</source>
        <translation>ID1
CSG1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="457"/>
        <source>ЗС2
ПЭП1</source>
        <translation>ID2
CSG1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="458"/>
        <source>ЗС1
ПЭП2</source>
        <translation>ID1
CSG2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="459"/>
        <source>ЗС2
ПЭП2</source>
        <translation>ID2
CSG2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="347"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="528"/>
        <source>Параметры имитационных сигналов</source>
        <translation>Paramètres des impulsions de simulation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="347"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="538"/>
        <source>ЦОС1</source>
        <translation>TNS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="347"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="539"/>
        <source>ЦОС2</source>
        <translation>TNS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="549"/>
        <source>Задержка</source>
        <translation>Retard</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="549"/>
        <source>Длительность</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="549"/>
        <source>Амплитуда</source>
        <translation>Amplitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="470"/>
        <source>Настройка зоны пеленга</source>
        <translation>Réglage de la zone de relèvement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="349"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="471"/>
        <source>Начало
кан. дальн.</source>
        <translation>Début des voies de portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="349"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="471"/>
        <source>Накопление
кол-во тактов</source>
        <translation>Accumulation
nombre de cadences</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="356"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="358"/>
        <source>ИС1</source>
        <translation>ISim1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="357"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="359"/>
        <source>ИС2</source>
        <translation>ISim2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="390"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="391"/>
        <source>0 Гц</source>
        <translation>0 Hz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="392"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="393"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="394"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="395"/>
        <source>0.0 сек</source>
        <translation>0.0 sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="543"/>
        <source>ИС%1</source>
        <translation>ISim%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="80"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="103"/>
        <source>Позиция</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="83"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="118"/>
        <source>Сохранить</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="119"/>
        <source>Восстановить</source>
        <translation>Restaurer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="118"/>
        <source>A%1</source>
        <translation>A%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="123"/>
        <source>D, %1</source>
        <translation>D, %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="128"/>
        <source>V, %1</source>
        <translation>V, %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="133"/>
        <source>E%1</source>
        <translation>E%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="229"/>
        <source>цос%1.азимут</source>
        <translation>tnc%1.azimut</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="229"/>
        <source>цос%1.дальность</source>
        <translation>tnc%1.portée</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="229"/>
        <source>цос%1.скорость</source>
        <translation>tnc%1.vitesse</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="230"/>
        <source>цос%1.угол места</source>
        <translation>tnc%1.angle de site</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="342"/>
        <source>Север</source>
        <translation>Nord</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="343"/>
        <source>Восток</source>
        <translation>Est</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="344"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="367"/>
        <source>Координаты точки стояния</source>
        <translation>Coordonnées du point de station</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="369"/>
        <source>Сектор</source>
        <translation>Secteur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="370"/>
        <source>Биссектриса</source>
        <translation>Bissecteur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="298"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="380"/>
        <source>Параметры секторов АФУ</source>
        <translation>Paramètres des secteur des FA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="301"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="385"/>
        <source>ЦОС %1</source>
        <translation>TNS %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="302"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="388"/>
        <source>Поправки</source>
        <translation>Corrections</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="302"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="403"/>
        <source>Углы закрытия</source>
        <translation>Angles de masquage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="311"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="404"/>
        <source>Использовать</source>
        <translation>Utiliser</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="314"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="436"/>
        <source>Загрузить</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>Комплекты</source>
        <translation>Jeu</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>ВК обработки</source>
        <translation>SO de traitement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>Приемник</source>
        <translation>Récepteur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>ЗГ</source>
        <translation>OP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="319"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="425"/>
        <source>Вращ</source>
        <translation>Rotateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="427"/>
        <source>Ревун</source>
        <translation>Sirène</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="321"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="429"/>
        <source>КЦ</source>
        <translation>CE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="433"/>
        <source>ВУМ</source>
        <translation>AES</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="437"/>
        <source>Излуч</source>
        <translation>Émission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="327"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="589"/>
        <source>Коды опознавания</source>
        <translation>Codes de l&apos;identification</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="331"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="642"/>
        <source>Сектора запрета излучения</source>
        <translation>Secteurs d&apos;interdiction d&apos;émission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="333"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="646"/>
        <source>Сект %1</source>
        <translation>Sect %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="159"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2137"/>
        <source>Режим</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="562"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2137"/>
        <source>ОО-VII</source>
        <translation>IG-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="562"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2137"/>
        <source>ИО-VII</source>
        <translation>II-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="562"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2137"/>
        <source>ИО-3-VII</source>
        <translation>II-3-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="549"/>
        <source>Коды 41М1(6)</source>
        <translation>Codes 41М1(6)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>Автомат</source>
        <translation>Automate</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>КД</source>
        <translation>CE</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>КД, КП</source>
        <translation>CE, CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="560"/>
        <source>Параметры автоматического опознавания</source>
        <translation>Paramètres de l&apos;identification automatique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="50"/>
        <source>Дата</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="53"/>
        <source>Время</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="87"/>
        <source>РЛС: -</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="88"/>
        <source>ВРЛ: -</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="167"/>
        <source>РЛС: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="122"/>
        <source>РЛС: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="123"/>
        <source>ВРЛ: 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="168"/>
        <source>ВРЛ: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="173"/>
        <source>АЗ: %1</source>
        <translation>AA: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="174"/>
        <source>АС: %1</source>
        <translation>PA: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="175"/>
        <source>КТ: %1</source>
        <translation>PCd: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="179"/>
        <source>РЕГ: %1</source>
        <translation>ENREG: %1% {1?}</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="185"/>
        <source>ИЗЛ:</source>
        <translation>EMISSION:</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="193"/>
        <source>КТ%1: %2</source>
        <translation>PCd%1: %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="194"/>
        <source>БАЗ: %1</source>
        <translation>SAA: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="203"/>
        <source>НзО: %1</source>
        <translation>ObT: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="226"/>
        <source>СЗИ: %1</source>
        <translation>SSE: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/bearing_action_widget_gui.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="153"/>
        <source>Расстановка</source>
        <translation>Disposition</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="171"/>
        <source>Короткий</source>
        <translation>Bref</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="172"/>
        <source>Сокращенный</source>
        <translation>Abrégé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="174"/>
        <source>Номер цели</source>
        <translation>Numéro de cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="176"/>
        <source>Нумерация</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="189"/>
        <source>Машинный</source>
        <translation>Machine</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="190"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="413"/>
        <source>Единый</source>
        <translation>Uni</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="192"/>
        <source>КП1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="193"/>
        <source>КП2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="196"/>
        <source>Оператор</source>
        <translation>Opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="197"/>
        <source>Универсальный</source>
        <translation>Universel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/bearing_action_widget_gui.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="199"/>
        <source>Непроз-сть</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="214"/>
        <source>След</source>
        <translation>Trace</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/action/target_action_widget_gui.cpp" line="219"/>
        <source>Длина</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="101"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="43"/>
        <source>Рабочее место оператора</source>
        <translation>Poste de travail de l&apos;opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="182"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="661"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="772"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="778"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="784"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="808"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2087"/>
        <source>ИКО</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="185"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="661"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="913"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2087"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="253"/>
        <source>АФК</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="625"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1375"/>
        <source>Полярные координаты</source>
        <translation>Coordonnées polaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="625"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1376"/>
        <source>Декартовы координаты</source>
        <translation>Coordonnées cartésiennes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="625"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1377"/>
        <source>Географические координаты</source>
        <translation>Coordonnées géographiques</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="632"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="915"/>
        <source>КИ</source>
        <translation>IC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="635"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="947"/>
        <source>ПЕЛ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="637"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="956"/>
        <source>Луч</source>
        <translation>Faisceau</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="645"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1403"/>
        <source>Сетка АД</source>
        <translation>Grille AzP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="645"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1419"/>
        <source>Сектора обзора</source>
        <translation>Secteurs de balayage</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="645"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1420"/>
        <source>Рубежи</source>
        <translation>Limites</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="645"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1421"/>
        <source>Зоны отбора</source>
        <translation>Zones de sélection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="648"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1428"/>
        <source>Сектора ЗИ ВРЛ</source>
        <translation>Secteurs IE IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="650"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1431"/>
        <source>Точки падения БЦ</source>
        <translation>Points d&apos;impact de la CB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="657"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="978"/>
        <source>Селекция</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="671"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="860"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1006"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1586"/>
        <source>КП: ?</source>
        <translation>PCdt: ?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="679"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="992"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1784"/>
        <source>МВ</source>
        <translation>TP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="681"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1784"/>
        <source>ВВ</source>
        <translation>TG</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="684"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1065"/>
        <source>ТЗИ</source>
        <translation>TSA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="694"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1140"/>
        <source>В центр</source>
        <translation>Vers le centre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="698"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="919"/>
        <source>В/Ч</source>
        <translation>DMil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="728"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1190"/>
        <source>По номерам</source>
        <translation>par numéros</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="728"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1204"/>
        <source>По скорости</source>
        <translation>Par vitesse</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="728"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1226"/>
        <source>По высоте</source>
        <translation>Par altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="728"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1262"/>
        <source>По классу</source>
        <translation>Par classe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="730"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1276"/>
        <source>По ОГП</source>
        <translation>Par nationalité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="730"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1286"/>
        <source>По источникам</source>
        <translation>Par sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="736"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="984"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="255"/>
        <source>ПРК</source>
        <translation>RVR</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="740"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1037"/>
        <source>2 секунды</source>
        <translation>2 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="740"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1037"/>
        <source>5 секунд</source>
        <translation>5 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="740"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1037"/>
        <source>10 секунд</source>
        <translation>10 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="740"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1038"/>
        <source>30 секунд</source>
        <translation>30 secondes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="740"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1038"/>
        <source>1 минута</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="741"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1038"/>
        <source>5 минут</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="750"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1090"/>
        <source>Воспр.инф</source>
        <translation>Affichage de l&apos;info</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1130"/>
        <source>9999.9 кфт</source>
        <translation>9999.9 кфт</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1373"/>
        <source>-1200.99 км, -1200.99 км</source>
        <translation>-1200.99 km, -1200.99 km</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1379"/>
        <source>Координаты сетки ПВО</source>
        <translation>Coordonnées de la grille DCA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1464"/>
        <source>Настройка</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1954"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1971"/>
        <source>Менее </source>
        <translation>Moins de </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1965"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1988"/>
        <source>Более </source>
        <translation>Plus de </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/explorer_jamming_monitoring.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/explorer_jamming_monitoring.cpp" line="47"/>
        <source>Диаграмма шумов в секторе %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/service/frequencychecker_service.cpp" line="16"/>
        <source>Разнос частот в секторе %1 меньше 400 кГц</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/service/frequencychecker_service.cpp" line="24"/>
        <source>Частоты в секторе %1 не принадлежат одному ПЭП</source>
        <translation>Les fréquences dans les secteurs %1 ne sont pas de la même sous-gamme</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/service/frequencychecker_service.cpp" line="36"/>
        <source>Частоты в сеторах %1 и %2 совпадают</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="34"/>
        <source>ftp отключен</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="43"/>
        <source>ftp подключение к &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="45"/>
        <source>ftp авторизация...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="47"/>
        <source>ftp переход в каталог &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="49"/>
        <source>ftp листинг &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="51"/>
        <source>ftp удаление &apos;%1&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="54"/>
        <source>ftp скачивание &apos;%1&apos; --&gt; &apos;%2&apos;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="70"/>
        <source>ftp ошибка подключения</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="73"/>
        <source>ftp подключение установлено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="76"/>
        <source>ftp ошибка авторизации</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="79"/>
        <source>ftp авторизация завершена</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="82"/>
        <source>ftp ошибка перехода в каталог</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="85"/>
        <source>ftp переход в каталог завершен</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="87"/>
        <source>ftp ошибка листинга</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="87"/>
        <source>ftp листинг завершен</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="101"/>
        <source>ftp ошибка удаления</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="101"/>
        <source>ftp удаление завершено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="108"/>
        <source>ftp ошибка скачивания</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="108"/>
        <source>ftp скачивание завершено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/process_ftp_file_server_tool.cpp" line="116"/>
        <source>ошибка ftp: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="101"/>
        <source>Координаты курсора</source>
        <translation>Coordonnées du curseur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="114"/>
        <source>Полярный</source>
        <translation>Polaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="60"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="115"/>
        <source>Прямоугольный</source>
        <translation>Rectangulaires</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="107"/>
        <source>Измерить расстояние</source>
        <translation>Mesurer la distance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="438"/>
        <source>Распоряжение ТР</source>
        <translation>Demande de CT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="65"/>
        <source>Редактировать</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="99"/>
        <source>Координаты</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="156"/>
        <source>Единый номер занят</source>
        <translation>Numéro uni est occupé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="162"/>
        <source>(Р)</source>
        <translation>(P)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="164"/>
        <source>Номер оператора занят</source>
        <translation>Numéro de l&apos;opérateur est occupé</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="268"/>
        <source>Добавить в ТЗИ</source>
        <translation>Ajouter à la table VAN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="269"/>
        <source>Удалить из ТЗИ</source>
        <translation>Rayer de la table VAN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="308"/>
        <source>Полный формуляр</source>
        <translation>Fiche complet</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="319"/>
        <source>Назначить класс цели</source>
        <translation>Attribuer une classe de cible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="217"/>
        <source>Самолет</source>
        <translation>Avion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="219"/>
        <source>Вертолет</source>
        <translation>Hélicoptère</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="221"/>
        <source>Крылатая ракета</source>
        <translation>Missile de croisière</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="223"/>
        <source>Гиперзвуковая крылатая ракета</source>
        <translation>Missile de croisière hypersonique</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="225"/>
        <source>Аэростат</source>
        <translation>Aérostat</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="227"/>
        <source>Баллистическая цель</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="341"/>
        <source>Передача на КП</source>
        <translation>Transmission vers le PC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="376"/>
        <source>Траектория</source>
        <translation>Trajectoire</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="397"/>
        <source>Назначить номер</source>
        <translation>Attribuer un numéro</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="407"/>
        <source>Оператора</source>
        <translation>de l&apos;Opérateur</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="459"/>
        <source>Сбросить</source>
        <translation>Refuser</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="477"/>
        <source>Разъединить</source>
        <translation>Déconnecter</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="482"/>
        <source>Объединить</source>
        <translation>Coupler</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="512"/>
        <source>Назначить госпринадлежность</source>
        <translation>Attribuer une nationalité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="516"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="316"/>
        <source>Свой ИО</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="516"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="518"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="310"/>
        <source>Свой ОО</source>
        <translation>IG &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="516"/>
        <source>Свой RBS</source>
        <translation>RBS &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="518"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="313"/>
        <source>Свой ГО</source>
        <translation>IN &quot;ami&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="520"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="321"/>
        <source>Чужой</source>
        <translation>Ennemi</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="533"/>
        <source>Уточнить ГП</source>
        <translation>Préciser la Nationalité</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="44"/>
        <source>Версия сборки: %1</source>
        <translation>Version de l&apos;assemblage: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="57"/>
        <source>Доступные щрифты</source>
        <translation>Caractères disponibles</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/usb_manager_tool.cpp" line="57"/>
        <source>Отменено пользователем</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="56"/>
        <source>МУ</source>
        <translation>CL</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="54"/>
        <source>ЦУ</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="56"/>
        <source>ВКЛ</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="56"/>
        <source>ВЫКЛ</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="58"/>
        <source>АНТ</source>
        <translation>ANT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="58"/>
        <source>ЭКВ</source>
        <translation>EQV</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="60"/>
        <source>ПЭП1</source>
        <translation>CSG1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="60"/>
        <source>ПЭП2</source>
        <translation>CSG2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="62"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="62"/>
        <source>180</source>
        <translation>180</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="72"/>
        <source>%1(КП)</source>
        <translation>%1(PC)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="74"/>
        <source>%1(Р)</source>
        <translation>%1(R)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="88"/>
        <source>%1 (%2 дБ)</source>
        <translation>%1 (%2 dB)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="129"/>
        <source>Произведено включение РЛС
Проверьте время</source>
        <translation>Radar est mis en fonction
Vérifier le temps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="136"/>
        <source>В РЭУ 5Ц3-Е сектор %1 температура не в норме</source>
        <translation>Température anormale dans le DRE 5Ц3-Е secteur %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="142"/>
        <source>В РЭК 1 температура не в норме</source>
        <translation>Température anormale dans le DRE1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="144"/>
        <source>ЛИРА температура не в норме</source>
        <translation>Température anormale dans LYRA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="149"/>
        <source>Ошибка авторизации: &apos;пользователь не существует&apos;</source>
        <translation>Erreur d&apos;autorisation: &apos;utilisateur inexistant&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="151"/>
        <source>Ошибка авторизации: &apos;неверный пароль&apos;</source>
        <translation>Erreur d&apos;autorisation: &apos;mot de passe incorrect&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="156"/>
        <source>Вы вошли как &apos;гость&apos;</source>
        <translation>Vous êtes entré comme &apos;visiteur&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="157"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="161"/>
        <source>Вы вошли как &apos;%1&apos;</source>
        <translation>Vous êtes entré comme &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="159"/>
        <source>Вы вошли как &apos;командир&apos;</source>
        <translation>Vous êtes entré comme &apos;commandant&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="176"/>
        <source>включено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="176"/>
        <source>выключено</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="169"/>
        <source>КП%1: излучение РЛС %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="177"/>
        <source>КП%1: излучение ВРЛ %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="185"/>
        <source>КП%1: включен режим %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="192"/>
        <source>КП%1: Сброс признаков целей</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="199"/>
        <source>КП%1: Запрос технического состояния</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="203"/>
        <source>Тревога</source>
        <translation>Alarme</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="210"/>
        <source>Приемник %1 сектора %2</source>
        <translation>Récepteur %1 du secteur %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="215"/>
        <source>Не распознан</source>
        <translation>Non identifié</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="217"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="318"/>
        <source>В</source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="221"/>
        <source>КР</source>
        <translation>MC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="223"/>
        <source>ГЗ</source>
        <translation>HS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="225"/>
        <source>А</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="227"/>
        <source>БЦ</source>
        <translation>CB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="232"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="264"/>
        <source>КП: </source>
        <translation>PC: </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="256"/>
        <source>РЕЖ: %1</source>
        <translation>MODE: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="262"/>
        <source>КП%1: </source>
        <translation>PC%1: </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="300"/>
        <source>КД используются</source>
        <translation>CE sont utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="300"/>
        <source>КД не используются</source>
        <translation>CE ne sont pas utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="301"/>
        <source>КП используются</source>
        <translation>CC sont utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="301"/>
        <source>КП не используются</source>
        <translation>CC ne sont pas utilisés</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="308"/>
        <source>Не определен</source>
        <translation>Non défini</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="308"/>
        <source>Х</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="310"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="316"/>
        <source>П</source>
        <translation>П</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="318"/>
        <source>Гражданский</source>
        <translation>Civil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="321"/>
        <source>Ч</source>
        <translation>Ч</translation>
    </message>
</context>
</TS>
