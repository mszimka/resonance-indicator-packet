<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en" sourcelanguage="ru">
<context>
    <name>QObject</name>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="21"/>
        <source>Номер</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="371"/>
        <source>Ширина</source>
        <translation>Width</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="191"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="268"/>
        <source>%1 МГц</source>
        <translation>%1 MHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="100"/>
        <source>Действие невозможно</source>
        <translation>Action is impossible</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/bearing/explorer_bearing_monitoring.cpp" line="101"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="193"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="270"/>
        <source>%1 дБ</source>
        <translation>%1 dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="238"/>
        <source>Вывести на ИКО</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>Show on PPI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="239"/>
        <source>Пеленгование</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>Bearing</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="243"/>
        <source>Назначить рабочей ЗС1</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>Make operating for PS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="247"/>
        <source>Назначить рабочей ЗС2</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>Make operating for PS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/peleng/total_peleng_explorer_painter.cpp" line="22"/>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/peleng/total_peleng_explorer_painter.cpp" line="23"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="371"/>
        <source>МГц</source>
        <translation>MHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/peleng/total_peleng_explorer_painter.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="373"/>
        <source>дБ</source>
        <translation>dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="22"/>
        <source>Единый номер</source>
        <translation>Unified number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="24"/>
        <source>Номер от КП1</source>
        <translation>CC1 number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="25"/>
        <source>Номер от КП2</source>
        <translation>CC2 number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="27"/>
        <source>Номер борта</source>
        <translation>A/C number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="175"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="629"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="207"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="332"/>
        <source>Дальность</source>
        <translation>Range</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="174"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="620"/>
        <source>Азимут</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="31"/>
        <source>Высота измер.</source>
        <translation>Measur. alt.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="33"/>
        <source>Высота баром.</source>
        <translation>Barometr.alt.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="35"/>
        <source>Курс</source>
        <translation>Course</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="36"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="207"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="208"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="332"/>
        <source>Скорость</source>
        <translation>Velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="156"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="534"/>
        <source>Класс</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="39"/>
        <source>ОГП</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="42"/>
        <source>Запас топлива</source>
        <translation>Fuel margin</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="44"/>
        <source>Выдача на КП</source>
        <translation>Output to CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="46"/>
        <source>Источники</source>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="49"/>
        <source>Код 1</source>
        <translation>1 code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="50"/>
        <source>Код 2</source>
        <translation>2 code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="53"/>
        <source>Код 3A</source>
        <translation>ЗА code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/explorer/target/total_target_explorer_painter.cpp" line="53"/>
        <source>Код S</source>
        <translation>S code</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="222"/>
        <source>Все действующие бланки АЗ будут удалены. Продолжить?</source>
        <translation>All current ALBs will be removed. Continue?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="288"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="429"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="349"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="387"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="477"/>
        <source>Режим 1</source>
        <translation>Mode 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="289"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="430"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="31"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="95"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="350"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="388"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="477"/>
        <source>Режим 2</source>
        <translation>Mode 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="292"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="464"/>
        <source>ВР</source>
        <translation>RT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="294"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="579"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="201"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="426"/>
        <source>Полярные</source>
        <translation>Polar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="295"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="581"/>
        <source>Прямоугольные</source>
        <translation>Cartesian</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="315"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="474"/>
        <source>Питание УМ</source>
        <translation>PA power</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="315"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="476"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="495"/>
        <source>Излучение УМ</source>
        <translation>PA emission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="501"/>
        <source>У</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="502"/>
        <source>Ш</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1212"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="329"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="491"/>
        <source>Круговой</source>
        <translation>Circular</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="317"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1221"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1225"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="326"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="500"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="504"/>
        <source>Режимы запросов</source>
        <translation>Interrogation modes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="319"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="524"/>
        <source>Установка частот</source>
        <translation>Frequencies setting</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="525"/>
        <source>Назначенные частоты</source>
        <translation>Assigned frequencies</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="526"/>
        <source>Авт.</source>
        <translation>Aut.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="527"/>
        <source>Ручн.</source>
        <translation>Man.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="569"/>
        <source>Сортировка частот</source>
        <translation>Frequency sorting</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="321"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="578"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="643"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1420"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="116"/>
        <source>Бланки автозахвата</source>
        <translation>ALBs</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="325"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="570"/>
        <source>шум</source>
        <translation>noise</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="326"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="572"/>
        <source>номер</source>
        <translation>number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <source>MK1</source>
        <translation>MK1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <source>MK2</source>
        <translation>MK2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1226"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="505"/>
        <source>RBS-3A</source>
        <translation>RBS-3A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1226"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="505"/>
        <source>RBS-C</source>
        <translation>RBS-C</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1222"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1226"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="501"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="505"/>
        <source>RBS-S</source>
        <translation>RBS-S</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="140"/>
        <source>Вставьте USB диск</source>
        <translation>Insert USB drive</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="206"/>
        <source>Удаление файлов...</source>
        <translation>Removing files...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="77"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="251"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="306"/>
        <source>Нет данных</source>
        <translation>No data</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="152"/>
        <source>Поиск файлов...</source>
        <translation>Searching for files...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="172"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="300"/>
        <source>скриншоты</source>
        <translation>screenshots</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="173"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="302"/>
        <source>переговоры</source>
        <translation>voice</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="174"/>
        <source>базы данных</source>
        <translation>database</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="195"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="83"/>
        <source>Копирование файлов...</source>
        <translation>Coping files...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="202"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="220"/>
        <source>Извлечение USB диска...</source>
        <translation>Unmounting USB drive...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="255"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="307"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="332"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="370"/>
        <source>Выбрать все</source>
        <translation>Check all</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="256"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="308"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="333"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="371"/>
        <source>Отменить все</source>
        <translation>Uncheck all</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="257"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="309"/>
        <source>Обновить</source>
        <translation>Refresh</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="274"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="310"/>
        <source>Печать</source>
        <translation>Print</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="276"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="311"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="318"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="368"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="110"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="87"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="161"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="583"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="434"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="114"/>
        <source>Удалить</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="278"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="312"/>
        <source>Копировать на USB диск</source>
        <translation>Copy to USB drive</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="281"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="211"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="339"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="373"/>
        <source>Выход</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/logging_dialog_gui.cpp" line="304"/>
        <source>база данных</source>
        <translation>database</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="124"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="643"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1419"/>
        <source>Наземные объекты</source>
        <translation>Ground objects</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="166"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="584"/>
        <source>Удалить все</source>
        <translation>Delete all</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="151"/>
        <source>3D-Радар</source>
        <translation>3D-Radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="152"/>
        <source>2D-Радар</source>
        <translation>2D-Radar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>КП</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="154"/>
        <source>Аэродром</source>
        <translation>Airport</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="338"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="372"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="437"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="470"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="86"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="298"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="585"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="238"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="109"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="188"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="146"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="227"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="360"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="491"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="305"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="437"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="60"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="42"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/zz_widget_gui.cpp" line="23"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="419"/>
        <source>Применить</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/ground_object_dialog.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="472"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="186"/>
        <source>Отмена</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manage_graphics_controller.cpp" line="15"/>
        <source>ЗИ%1</source>
        <translation>EB%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="195"/>
        <source>тчк</source>
        <translation>pnt</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="195"/>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/drawable/guidance_drawable_scene_items_graphics.cpp" line="244"/>
        <source>наз</source>
        <translation>grn</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="20"/>
        <source>Невозможно в военное время</source>
        <translation>Impossible in wartime</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="21"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="100"/>
        <source>У вас недостаточно прав</source>
        <translation>You have not sufficient rights</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="22"/>
        <source>Действие невозможно для режима просмотра</source>
        <translation>Action impossible for scan mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="23"/>
        <source>Сервер не подключен</source>
        <translation>Server is not connected</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/disabler_gui.cpp" line="24"/>
        <source>Действие невозможно для режима воспроизведения информации</source>
        <translation>Action impossible for display mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="104"/>
        <source>Назад</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="106"/>
        <source>Вперед</source>
        <translation>Forward</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="108"/>
        <source>Домой</source>
        <translation>Main menu</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="116"/>
        <source>Общее
состояние</source>
        <translation>General state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="117"/>
        <source>Состояние
УМ</source>
        <translation>PA status</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="119"/>
        <source>Краткое состояние
приемников</source>
        <translation>Receiver state summary</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="121"/>
        <source>Полное состояние
приемников</source>
        <translation>Receiver channels parameters</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="77"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="122"/>
        <source>Панорама шумов</source>
        <translation>Jamming panorama</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="79"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/diagnostic_bar.cpp" line="124"/>
        <source>Панорама пеленга</source>
        <translation>Bearing panorama</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="58"/>
        <source>Канал</source>
        <translation>Channel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="59"/>
        <source>До калибровки</source>
        <translation>Before calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="60"/>
        <source>После калибровки</source>
        <translation>After calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>Шум, дБ</source>
        <translation>Noise, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Амплитуда, дБ</source>
        <translation>Amplitude,dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>Фаза, гр</source>
        <translation>Phase, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="62"/>
        <source>СКО ампл., дБ</source>
        <translation>Ampl. RMSE, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>СКО фазы, гр</source>
        <translation>Phase RMSE, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="63"/>
        <source>АФЮС ампл., дБ</source>
        <translation>Ampl. IASS, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>АФЮС фаза., гр</source>
        <translation>Phase. IASS, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон ампл., дБ</source>
        <translation>Ampl. standard. dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="64"/>
        <source>Эталон фазы., гр</source>
        <translation>Phase standard, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка ампл., дБ</source>
        <translation>Ampl. correction, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/calibration_antenna_explorer_diagnostic.cpp" line="65"/>
        <source>Поправка фазы., гр</source>
        <translation>Phase correction, deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="59"/>
        <source>Направление</source>
        <translation>Direction</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="69"/>
        <source>Тип</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="71"/>
        <source>Азимутальная</source>
        <translation>Azimuth</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="71"/>
        <source>Угломестная</source>
        <translation>Elevation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="73"/>
        <source>Контроль по</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="694"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="914"/>
        <source>ИС</source>
        <translation>SS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="75"/>
        <source>АФЮС</source>
        <translation>IASS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="77"/>
        <source>Частота, МГц</source>
        <translation>Frequency, MHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/antenna/requester_antenna_explorer_diagnostic.cpp" line="109"/>
        <source>Рассчитать</source>
        <translation>Calculate</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="88"/>
        <source>Сопряжение с КП%1</source>
        <translation>Interface with CC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="103"/>
        <source>Состояние АПД</source>
        <translation>DTE state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="104"/>
        <source>Готовность</source>
        <translation>Ready</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="107"/>
        <source>Физическое
соединение</source>
        <translation>Physical
connection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="108"/>
        <source>Логическое
соединение</source>
        <translation>Logical
connection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="95"/>
        <source>Состояние связи</source>
        <translation>Communication state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="96"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="265"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="387"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="182"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="190"/>
        <source>Командир</source>
        <translation>Commander</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="97"/>
        <source>Инициализация
передачи</source>
        <translation>Transmission
initialization</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="98"/>
        <source>Прием
сообщений</source>
        <translation>Message
receiving</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="60"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="150"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="172"/>
        <source>Синхронизация
времени</source>
        <translation>Time
synchronization</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="62"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="100"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="174"/>
        <source>Доставка
сообщений</source>
        <translation>Message
delivery</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="106"/>
        <source>Прием
данных</source>
        <translation>Data
receiving</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="105"/>
        <source>Передача
данных</source>
        <translation>Data
delivery</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="158"/>
        <source>Тип протокола: %1</source>
        <translation>Protocol type:%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="70"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="159"/>
        <source>Принято пакетов: %1</source>
        <translation>RX packets: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="160"/>
        <source>Ошибок приема: %1</source>
        <translation>RX errors: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="161"/>
        <source>Выдано пакетов: %1</source>
        <translation>TX packets: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="162"/>
        <source>Ошибок выдачи: %1</source>
        <translation>TX errors: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="163"/>
        <source>Просрочено пакетов: %1</source>
        <translation>TX expired: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="75"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/command_post_communication_explorer_diagnostic.cpp" line="164"/>
        <source>Задержка: %1 мс</source>
        <translation>Delay: %1 ms</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="79"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/russia_general_diagnostic_explorer.cpp" line="25"/>
        <source>Сообщения об отказах</source>
        <translation>Failure report</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="81"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="32"/>
        <source>РМО1</source>
        <translation>OWS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="82"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="33"/>
        <source>РМО2</source>
        <translation>OWS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="83"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="34"/>
        <source>ВРМО</source>
        <translation>ROWS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="31"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="92"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="36"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="746"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1086"/>
        <source>Сервер</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="32"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="95"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="37"/>
        <source>ИБП1</source>
        <translation>UPS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/base_general_diagnostic_explorer.cpp" line="96"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="38"/>
        <source>ИБП2</source>
        <translation>UPS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="40"/>
        <source>АПД1</source>
        <translation>DTE1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="66"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="41"/>
        <source>АПД2</source>
        <translation>DTE2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="93"/>
        <source>Антенна</source>
        <translation>Antenna</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="93"/>
        <source>Эквивалент</source>
        <translation>Equivalent</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="73"/>
        <source>Приемник%1</source>
        <translation>Reciever%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="58"/>
        <source>УМ%1</source>
        <translation>PA%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="67"/>
        <source>Антенна Az</source>
        <translation>Antenna Az</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/algeria_general_diagnostic_explorer.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="70"/>
        <source>Антенна E</source>
        <translation>Antenna E</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="20"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="291"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="444"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="43"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="112"/>
        <source>ВРЛ</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="51"/>
        <source>ЦОС%1</source>
        <translation>DSP%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="61"/>
        <source>Приемник%1 (f1)</source>
        <translation>Receiver %1(f1)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/analog_general_diagnostic_explorer.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="64"/>
        <source>Приемник%1 (f2)</source>
        <translation>Receiver%1 (f2)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="39"/>
        <source>Цифровой
приемник</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>Digital
reciever</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/general/egypt_general_diagnostic_explorer.cpp" line="42"/>
        <source>АПД3</source>
        <translation>DTE3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="81"/>
        <source>Мнемосхема шкафа приемника сектора %1 РЭМ 3 5ЦП1-01</source>
        <translation>Receiver sector %1 REM 3 5ЦП1-01 cabinet symb.circuit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="103"/>
        <source>АА%1
5ЦП101Б%2</source>
        <translation>AA%1
5ЦП1Б%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="107"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="108"/>
        <source>Плата управления АА2</source>
        <translation>Control card AA2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/analog_tech_reciever_explorer_diagnostic.cpp" line="112"/>
        <source>АА%1
Узел питания %2</source>
        <translation>AA%1
power supply %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="120"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="121"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="181"/>
        <source>Азимутальные каналы</source>
        <translation>Azimuth channels</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="122"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="123"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="182"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="183"/>
        <source>Угломестные каналы</source>
        <translation>Elevation channels</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <source>Внутр. шум дБ</source>
        <translation>Inner noise dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <source>Внутр. ампл. дБ</source>
        <translation>Innser ampl.dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <source>Внутр. фаза гр</source>
        <translation>Inner phase deg</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="126"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="186"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="188"/>
        <source>Внеш. шум дБ</source>
        <translation>Exter. noise dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="127"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="187"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/reciever/calibration_reciever_explorer_diagnostic.cpp" line="188"/>
        <source>Внеш. ампл. дБ</source>
        <translation>Exter. ampl. dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="97"/>
        <source>Состояние усилителя мощности сектора %1</source>
        <translation>PA sector state %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="106"/>
        <source>Техническое состояние ТЭЗ УМ</source>
        <translation>PA LRU technical state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>5Ц300Я01</source>
        <translation>5Ц300Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>БКУ 5Ц301Б03</source>
        <translation>CU 5Ц301Б03</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>БУМ 5Ц301Б01 (верх)</source>
        <translation>PAU 5Ц301Б01(top)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="110"/>
        <source>БУМ 5Ц301Б01 (низ)</source>
        <translation>PAU 5Ц301Б01 (bottom)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>БСДНО 5Ц301Б02</source>
        <translation>SDDCU 5Ц301Б02</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>БП 5Ц301Б05</source>
        <translation>PSU НПС3000.9.3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>БФГ 5Ц301Б04</source>
        <translation>HFU 5Ц301Б04</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="111"/>
        <source>5Ц301Я01</source>
        <translation>5Ц301Я01</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="112"/>
        <source>Узел распределения пит.</source>
        <translation>Power distribution u.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="112"/>
        <source>Узел воздушного охл.</source>
        <translation>Air cooling u.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="116"/>
        <source>%1 канал</source>
        <translation>%1 channel</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="120"/>
        <source>%1А%2</source>
        <translation>%1A%2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>1А8</source>
        <translation>1A8</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>1А11</source>
        <translation>1A11</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>1А7</source>
        <translation>1A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="123"/>
        <source>2А7</source>
        <translation>2A7</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="131"/>
        <source>Функциональное состояние УМ</source>
        <translation>PA functional state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="132"/>
        <source>Шкаф 1</source>
        <translation>Cabinet 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="133"/>
        <source>Шкаф 2</source>
        <translation>Cabinet 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="63"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="867"/>
        <source>Управление</source>
        <translation>Control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="324"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="439"/>
        <source>Питание</source>
        <translation>Power</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <source>Излучение ЗС1</source>
        <translation>PS1 emission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="136"/>
        <source>Излучение ЗС2</source>
        <translation>PS2 emission</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>Калибр. ЗС1</source>
        <translation>PS1 calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>Калибр. ЗС2</source>
        <translation>PS2 calibration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>Нагрузка</source>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="137"/>
        <source>ПЭП</source>
        <translation>ETS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="138"/>
        <source>Фаза ЗС1</source>
        <translation>PS1 phase</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/transmitter_diagnostic_explorer.cpp" line="138"/>
        <source>Фаза ЗС2</source>
        <translation>PS2 phase</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="26"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="61"/>
        <source>Состояние ВРЛ</source>
        <translation>IFF state</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="63"/>
        <source>Источники питания</source>
        <translation>Power supply</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="29"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="64"/>
        <source>Панель
 автоматических
 выключателей</source>
        <translation>Auto switch board</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="64"/>
        <source>Вентиляторы</source>
        <translation>Fans</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="33"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="99"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="297"/>
        <source>41М1.6
(6110)</source>
        <translation>41М1.6
(6110)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="101"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="295"/>
        <source>KIR-1A</source>
        <translation>KIR-1A</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="65"/>
        <source>СВ КР-02
 Комплект 1</source>
        <translation>CB KP-02
Set 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="65"/>
        <source>СВ КР-02
 Комплект 2</source>
        <translation>CB KP-02
Set 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="66"/>
        <source>124ПП02
 Комплект 1</source>
        <translation>124ПП02
Set 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="66"/>
        <source>124ПП02
 Комплект 2</source>
        <translation>124ПП02
Set 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="49"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="66"/>
        <source>ЗГ-1
 Комплект 1</source>
        <translation>ЗГ-1
Set 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="67"/>
        <source>ЗГ-2
 Комплект 2</source>
        <translation>ЗГ-2
Set 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="70"/>
        <source>ВУМ %1</source>
        <translation>OPA %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/explorer/vrl_state_diagnostic_explorer.cpp" line="73"/>
        <source>Перекл.
 СВЧ</source>
        <translation>SHF switch</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/algeria_diagnostic_scene.cpp" line="21"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/base_diagnostic_scene.cpp" line="120"/>
        <location filename="../apps/resonance/indicator/src/diagnostic/scene/egypt_diagnostic_scene.cpp" line="40"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="300"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="532"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="47"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="190"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="246"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="450"/>
        <source>Сектор%1</source>
        <translation>Sector%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="17"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="18"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/graphics/controller/vrl/vrl_manip_graphics_controller.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="1214"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="330"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="493"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="523"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="994"/>
        <source>Манип</source>
        <translation>Manip</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="322"/>
        <source>ОТКАЗ</source>
        <translation>Failure</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/graphics_view.cpp" line="325"/>
        <source>ГОТОВ К БОЕВОЙ РАБОТЕ</source>
        <translation>Ready for combat operation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/graphics/scene_items/grid/spherical_grid_label_scene_item.cpp" line="30"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="234"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="309"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="329"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="338"/>
        <source>Внимание!</source>
        <translation>Warning!</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="20"/>
        <source>Редактор карт</source>
        <translation>Map editor</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="39"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="608"/>
        <source>Поиск доступных файлов...</source>
        <translation>Searching for files...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="95"/>
        <source>Файл &apos;%1&apos; существует. Заменить?</source>
        <translation>FIle &apos;%1&apos; already exists. Would you like to replace it??</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="192"/>
        <source>Карты для импорта</source>
        <translation>Maps for import</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="193"/>
        <source>Текущие карты</source>
        <translation>Current maps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="207"/>
        <source>Поиск</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/map_dialog_gui.cpp" line="209"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="319"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="369"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="109"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="85"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/static_function_gui_tab.cpp" line="157"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="312"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="431"/>
        <source>Добавить</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="116"/>
        <source>Имя</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="218"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="231"/>
        <source>Внимание</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="232"/>
        <source>Конфигурация с таким именем уже существует. Заменить?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="476"/>
        <source>Выражение содержит несуществующий фильтр</source>
        <translation type="unfinished">Expression contains non-existent filter</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="339"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="36"/>
        <source>Широта</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="340"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/ground_object_coordinate_widget_gui.cpp" line="38"/>
        <source>Долгота</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="260"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="267"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="274"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="321"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/mode_gui_tab.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="156"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="164"/>
        <source>Ошибка</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="260"/>
        <source>Имя пользователя не может быть пустым</source>
        <translation>User name can`t be empty</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="68"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="265"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="400"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="134"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="148"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="187"/>
        <source>Гость</source>
        <translation>Guest</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="74"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="267"/>
        <source>Недопустимое имя пользователя</source>
        <translation>Inadmissible user name</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="274"/>
        <source>Пользователь с таким именем уже существует</source>
        <translation>User name already exists</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="305"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="197"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="294"/>
        <source>Пользователи</source>
        <translation>Users</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="311"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/user_widget_gui.cpp" line="191"/>
        <source>Пароль</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="374"/>
        <source>Права</source>
        <translation>Rights</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="389"/>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="413"/>
        <source>в сети</source>
        <translation>online</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="431"/>
        <source>Установка параметров обработки сигнала</source>
        <translation>Signal procession parameters setting</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="433"/>
        <source>Выбор рабочих частот</source>
        <translation>Operating frequency choice</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="435"/>
        <source>Включение автоматической смены частот</source>
        <translation>Automatic frequency rotation on</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="437"/>
        <source>Установка режимов защиты от помех</source>
        <translation>Jamming protect mode set</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="438"/>
        <source>Установка бланков ЗАЗ</source>
        <translation>ALZ blank set</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="440"/>
        <source>Присвоение ВО признака госопознавания</source>
        <translation>AO IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="442"/>
        <source>Присвоение ВО класса цели</source>
        <translation>AO type</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="443"/>
        <source>Смена номера ВО</source>
        <translation>AO number change</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="444"/>
        <source>Работа с БД</source>
        <translation>Data base</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="446"/>
        <source>Смена кодов аппаратуры 41М1(6)</source>
        <translation>41M1(6) equipment codes change</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="447"/>
        <source>Настройка режимов Пароля</source>
        <translation>Password modes set</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/dialog/users_dialog_gui.cpp" line="450"/>
        <source>Выбор комплекта аппаратуры ВРЛ</source>
        <translation>IFF equipment set choice</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="46"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="88"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="130"/>
        <source>Включить</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/flight_list_function_gui_tab.cpp" line="130"/>
        <source>Выключить</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="44"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="380"/>
        <source>От</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="382"/>
        <source>До</source>
        <translation>Up to</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="393"/>
        <source>Точка падения баллистической цели</source>
        <translation>BT point of impact</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="395"/>
        <source>Экстраполяционные точки</source>
        <translation>Extrapolation points</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="400"/>
        <source>Выход комплекса HLCP</source>
        <translation>HLCP complex output</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="402"/>
        <source>Контроль функционирования</source>
        <translation>Operation control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="55"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="403"/>
        <source>Координатные точки</source>
        <translation>Coordinate points</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="404"/>
        <source>КТ с выхода ВОИ</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>DSP output CP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="58"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="409"/>
        <source>Запросы на опознавание РЛО RBS</source>
        <translation>RDR identification requests RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="410"/>
        <source>Отметка опознавания РЛО RBS</source>
        <translation>RBS identification mark</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="412"/>
        <source>Команды оператора</source>
        <translation>Operator`s commands</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="420"/>
        <source>Обмен с КСА</source>
        <translation>Exchange with AEC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="422"/>
        <source>Плотность помех по частоте</source>
        <translation>Frequency jamming density</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="432"/>
        <source>Создать описание</source>
        <translation>Create description</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="435"/>
        <source>Описание базы данных</source>
        <translation>Database description</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="76"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="439"/>
        <source>Файлы данных</source>
        <translation>Data files</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="77"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="108"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="446"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="541"/>
        <source>Воспроизведение</source>
        <translation>Display</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="78"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="449"/>
        <source>Следующий обзор</source>
        <translation>Next sweep</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="79"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="452"/>
        <source>Стоп</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="148"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="162"/>
        <source>Нет доступных файлов</source>
        <translation>No available files</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="234"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="309"/>
        <source>Время начала больше времени окончания</source>
        <translation>Time on exceeds time off</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="321"/>
        <source>Не выбрано ни одной таблицы данных</source>
        <translation>No data chart chosen</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="330"/>
        <source>Хотите создать файл описания?</source>
        <translation>Want to create description file?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="331"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="50"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="64"/>
        <source>Да</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="332"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="65"/>
        <source>Нет</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="338"/>
        <source>Файл описания создан не будет</source>
        <translation>Descript.file not created</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="397"/>
        <source>Выход комплекса Фундамент1,2</source>
        <translation>Foundation 1,2 complex output</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="406"/>
        <source>Запросы на опознавание РЛО MkXA</source>
        <translation>RDR identification requests РЛО MkXA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="407"/>
        <source>Отметка опознавания РЛО MkXA</source>
        <translation>РЛО MkXA identification mark</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="414"/>
        <source>Запросы на опознавание Пароль</source>
        <translation>Identification requests Password</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="415"/>
        <source>Отметка опознавания Пароль</source>
        <translation>Identification mark Password</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="496"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="532"/>
        <source>Пауза</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="523"/>
        <source>Продолжить</source>
        <translation>Further</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="475"/>
        <source>Предупреждение</source>
        <translation type="unfinished">Warning</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="561"/>
        <source>Допустимый интервал времени от %1 до %2</source>
        <translation>Allowable time space from %1 to %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="592"/>
        <source>Скрипт запущен</source>
        <translation>Script on</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="597"/>
        <source>Скрипт уже выполняется. Дождитесь завершения</source>
        <translation>Script is running</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="600"/>
        <source>Скрипт прерван</source>
        <translation>Script is terminated</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="602"/>
        <source>Скрипт завершился с ошибкой</source>
        <translation>Script exited with error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/logging_function_gui_tab.cpp" line="614"/>
        <source>Получен неизвестный ответ от скрипта &apos;%1&apos;</source>
        <translation>Unknown reply received from script &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="62"/>
        <source>Требуется подтверждение действия</source>
        <translation>Action confirmation required</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="49"/>
        <source>Вы действительно хотите произвести выключение РМО?</source>
        <translation>You really want to deactivate RDR?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="63"/>
        <source>Вы действительно хотите произвести выключение РЛС?</source>
        <translation>You really want to deactivate RDR?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="159"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="109"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="640"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="969"/>
        <source>Статика</source>
        <translation>Statics</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="109"/>
        <source>Регистрация</source>
        <translation>Logging</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="94"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="109"/>
        <source>Тренаж</source>
        <translation>Training</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="97"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="126"/>
        <source>Выключение РЛС</source>
        <translation>RDR off</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/function/main_function_gui_tab.cpp" line="124"/>
        <source>Выключение РМО</source>
        <translation>OWS off</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Режимы</source>
        <translation>Modes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Обработка</source>
        <translation>Procession</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Функции</source>
        <translation>Functions</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="37"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/main_gui_tab.cpp" line="86"/>
        <source>Настройки</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="188"/>
        <source>Зоны режекции</source>
        <translation>Rejection zones</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="191"/>
        <source>Доплеровская скорость (0-100), м/с</source>
        <translation>Doppler velocity (0-100), m/s</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="50"/>
        <source>ЗС%1</source>
        <translation>PS%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="228"/>
        <source>Пороги обнаружения (до 25.5), дБ</source>
        <translation>Detection thresholds (up to 25.5), dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="232"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="247"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="396"/>
        <source>ЗС1</source>
        <translation>PS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="52"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="56"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="210"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="232"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="248"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="396"/>
        <source>ЗС2</source>
        <translation>PS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="53"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="244"/>
        <source>Защита от активных помех</source>
        <translation>Jamming protection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="60"/>
        <source>Дальность режекции</source>
        <translation>Rejection range</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="252"/>
        <source>РНИП</source>
        <translation>AN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="66"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="253"/>
        <source>РП</source>
        <translation>PN</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="254"/>
        <source>ШОС</source>
        <translation>WSL</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/processing_gui_tab.cpp" line="193"/>
        <source>Дальность режекции (0-1200), км</source>
        <translation>Rejection range (0-1200), km</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="135"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="330"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="488"/>
        <source>Ед.№</source>
        <translation>UNo</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="136"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="331"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="489"/>
        <source>К</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="139"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="334"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="492"/>
        <source>РКЦ</source>
        <translation>TCI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="140"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="335"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="493"/>
        <source>Ампл.дБ
Код обн.</source>
        <translation>Ampl.dB
Code.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="145"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="340"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="498"/>
        <source>№
№
t,мм:сс</source>
        <translation>№
№
t,mm:ss</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="154"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="522"/>
        <source>Все</source>
        <translation>All</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="155"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="523"/>
        <source>Селекция ИКО</source>
        <translation>PPI selection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="163"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="553"/>
        <source>Источник</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="169"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="575"/>
        <source>Коды ВРЛ</source>
        <translation>IFF codes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="169"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="580"/>
        <source>Не отв.</source>
        <translation>No rep.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="604"/>
        <source>Критерий</source>
        <translation>Criterion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <source>Новые</source>
        <translation>New</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <source>Навед</source>
        <translation>Guide</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="608"/>
        <source>Цель</source>
        <translation>Target</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="609"/>
        <source>Запрет</source>
        <translation>Ban</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="437"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/bearing_widget_gui.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="139"/>
        <source>Формуляры</source>
        <translation>RRC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="181"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/route_gui_tab.cpp" line="438"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="89"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="296"/>
        <source>Наведение</source>
        <translation>Guidance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="100"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="153"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="666"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="668"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1014"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="357"/>
        <source>КП%1</source>
        <translation>CC%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="161"/>
        <source>Управление АПД</source>
        <translation>DTE control</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="163"/>
        <source>Скорость передачи</source>
        <translation>Transmis.speed</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="170"/>
        <source>Уровень передачи</source>
        <translation>Transm. level</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="102"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="179"/>
        <source>ПСМ, сек</source>
        <translation>NMP, sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="106"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="182"/>
        <source>Скремблирование</source>
        <translation>Scrambling</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="107"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="183"/>
        <source>Адаптация корректора</source>
        <translation>Corrector adaptation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="110"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/command_post_settings_tab_gui.cpp" line="184"/>
        <source>Переподключение</source>
        <translation>Reconnection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="142"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="161"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="671"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1149"/>
        <source>Мирное время</source>
        <translation>Peacetime</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="143"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="164"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="671"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1156"/>
        <source>Военное время</source>
        <translation>Wartime</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="144"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="223"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="251"/>
        <source>Запретить</source>
        <translation>Ban</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="145"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/frequency_settings_gui_tab.cpp" line="225"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/scene_jamming_monitoring.cpp" line="253"/>
        <source>Разрешить</source>
        <translation>Allow</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="198"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="328"/>
        <source>Метрическая</source>
        <translation>Metric</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="198"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="328"/>
        <source>Англо-американская</source>
        <translation>Anglo-American</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="201"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="427"/>
        <source>Декартовы</source>
        <translation>Cartesian</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="201"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="428"/>
        <source>Географические</source>
        <translation>Geographic</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="205"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="293"/>
        <source>Авторизация</source>
        <translation>Authorization</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="206"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="296"/>
        <source>Наименование подразделения</source>
        <translation>Unit name</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="206"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="308"/>
        <source>Язык интерфейса</source>
        <translation>Interface language</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="206"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="326"/>
        <source>Система измерения</source>
        <translation>Measuring system</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="207"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="332"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="341"/>
        <source>Высота</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="208"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="424"/>
        <source>Координаты маркера</source>
        <translation>Cursor coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="141"/>
        <source>Сетка</source>
        <translation>Grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="145"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="632"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="922"/>
        <source>КТ</source>
        <translation>CP</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="149"/>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="171"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="632"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="931"/>
        <source>ЭТ</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="154"/>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="176"/>
        <source>Пеленг</source>
        <translation>Bearing</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="163"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="748"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="972"/>
        <source>Карты</source>
        <translation>Maps</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="168"/>
        <source>Непрозрачность формуляров</source>
        <translation>RC opacity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="107"/>
        <source>Длина траекторий</source>
        <translation>Trajectory length</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/frame/graphicssettings_frame_gui.cpp" line="125"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/bearing_widget_gui.cpp" line="118"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="226"/>
        <source>Яркость</source>
        <translation>Brightness</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="430"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="533"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1406"/>
        <source>Сетка ПВО</source>
        <translation>AD grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="432"/>
        <source>WGS-84</source>
        <translation>WGS-84</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="433"/>
        <source>ПЗ-90</source>
        <translation>ПЗ-90</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/general_settings_gui_tab.cpp" line="436"/>
        <source>Эллипсоид</source>
        <translation>Ellipsoid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="41"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="69"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="80"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="103"/>
        <source>Общие</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="73"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="110"/>
        <source>РЛС</source>
        <translation>RDR</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="73"/>
        <source>Сопряж</source>
        <translation>Interf</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/main_settings_gui_tab.cpp" line="73"/>
        <source>Частоты</source>
        <translation>Frequencies</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="27"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="93"/>
        <source>Модуль ВОИ</source>
        <translation>SDP module</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="96"/>
        <source>&quot;Короткий&quot;
критерий</source>
        <translation>&quot;Short&quot;
criterion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="34"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="97"/>
        <source>&quot;Длинный&quot;
критерий</source>
        <translation>&quot;Long&quot;
criterion</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="34"/>
        <source>Макс. скорость</source>
        <translation>Max velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="35"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="99"/>
        <source>Время
экстр-ции, с</source>
        <translation>Extrap-ion
time, with</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="45"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="48"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="51"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="101"/>
        <source>из</source>
        <translation>from</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="64"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="104"/>
        <source>ПБЛ</source>
        <translation>SL</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="65"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="102"/>
        <source>Азимут, дБ</source>
        <translation>Azimuth, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="67"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="103"/>
        <source>Дальность, дБ</source>
        <translation>Range, dB</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="143"/>
        <source>Макс.
скорость, </source>
        <translation>Max.
velocity, </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="217"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="231"/>
        <source>%1 Гц</source>
        <translation>%1 Hz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="221"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="233"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="235"/>
        <source>%1 сек</source>
        <translation>%1 sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="374"/>
        <source>Параметры ЗС и обработки</source>
        <translation>PS &amp; procession parameters</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Период</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Кол-во ТКН</source>
        <translation>CIC number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="342"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Кол-во ТНН</source>
        <translation>ICIC number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Длительн. ЗС</source>
        <translation>PS duration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="376"/>
        <source>Перед. фронт</source>
        <translation>Lead.
edge</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="377"/>
        <source>Задний фронт</source>
        <translation>Trailing edge</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="343"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="377"/>
        <source>Бланк нач. кан. Д</source>
        <translation>R chan. start blank</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="344"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="377"/>
        <source>Кол-во кан. Д</source>
        <translation>R chan. number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="448"/>
        <source>Амплитуды ЗС</source>
        <translation>PS amplitudes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <source>Сектор 1</source>
        <translation>Sector 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <source>Сектор 2</source>
        <translation>Sector 2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="345"/>
        <source>Сектор 3</source>
        <translation>Sector 3</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <source>Сектор 4</source>
        <translation>Sector 4</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="456"/>
        <source>ЗС1
ПЭП1</source>
        <translation>PS1
ETS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="457"/>
        <source>ЗС2
ПЭП1</source>
        <translation>PS2
ETS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="458"/>
        <source>ЗС1
ПЭП2</source>
        <translation>PS1
ETS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="346"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="459"/>
        <source>ЗС2
ПЭП2</source>
        <translation>PS2
ETS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="347"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="528"/>
        <source>Параметры имитационных сигналов</source>
        <translation>PS parameters</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="347"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="538"/>
        <source>ЦОС1</source>
        <translation>DSP1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="347"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="539"/>
        <source>ЦОС2</source>
        <translation>DSP2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="549"/>
        <source>Задержка</source>
        <translation>Delay</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="549"/>
        <source>Длительность</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="549"/>
        <source>Амплитуда</source>
        <translation>Amplitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="348"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="470"/>
        <source>Настройка зоны пеленга</source>
        <translation>Bearing zone set</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="349"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="471"/>
        <source>Начало
кан. дальн.</source>
        <translation>Range
chan. start.</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="349"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="471"/>
        <source>Накопление
кол-во тактов</source>
        <translation>Cycle number
integration</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="356"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="358"/>
        <source>ИС1</source>
        <translation>SS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="357"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="359"/>
        <source>ИС2</source>
        <translation>SS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="390"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="391"/>
        <source>0 Гц</source>
        <translation>0 Hz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="392"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="393"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="394"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="395"/>
        <source>0.0 сек</source>
        <translation>0.0 sec</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/general_rls_settings_gui_tab.cpp" line="543"/>
        <source>ИС%1</source>
        <translation>SS%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="80"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="103"/>
        <source>Позиция</source>
        <translation>LP</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="118"/>
        <source>Сохранить</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="84"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/main_rls_settings_gui_tab.cpp" line="119"/>
        <source>Восстановить</source>
        <translation>Recover</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="118"/>
        <source>A%1</source>
        <translation>A%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="123"/>
        <source>D, %1</source>
        <translation>D,%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="128"/>
        <source>V, %1</source>
        <translation>V,%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="133"/>
        <source>E%1</source>
        <translation>E%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="229"/>
        <source>цос%1.азимут</source>
        <translation>DSP %1 azimuth</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="229"/>
        <source>цос%1.дальность</source>
        <translation>DSP %1 range</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="229"/>
        <source>цос%1.скорость</source>
        <translation>DSP %1 velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="230"/>
        <source>цос%1.угол места</source>
        <translation>DSP %1 elevation</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="342"/>
        <source>Север</source>
        <translation>North</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="343"/>
        <source>Восток</source>
        <translation>East</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="296"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="344"/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="367"/>
        <source>Координаты точки стояния</source>
        <translation>Location point coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="369"/>
        <source>Сектор</source>
        <translation>Sector</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="297"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="370"/>
        <source>Биссектриса</source>
        <translation>Bisector</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="298"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="380"/>
        <source>Параметры секторов АФУ</source>
        <translation>AFS sectors parameters</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="301"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="385"/>
        <source>ЦОС %1</source>
        <translation>DSP %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="302"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="388"/>
        <source>Поправки</source>
        <translation>Corrections</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="302"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="403"/>
        <source>Углы закрытия</source>
        <translation>Cutoff angles</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="311"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="404"/>
        <source>Использовать</source>
        <translation>Use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="314"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/rls/position_rls_settings_gui_tab.cpp" line="436"/>
        <source>Загрузить</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>Комплекты</source>
        <translation>Sets</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>ВК обработки</source>
        <translation>Procession CS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>Приемник</source>
        <translation>Receiver</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="316"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="383"/>
        <source>ЗГ</source>
        <translation>MG</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="319"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="425"/>
        <source>Вращ</source>
        <translation>RT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="320"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="427"/>
        <source>Ревун</source>
        <translation>Siren</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="321"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="429"/>
        <source>КЦ</source>
        <translation>CT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="433"/>
        <source>ВУМ</source>
        <translation>OPA</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="437"/>
        <source>Излуч</source>
        <translation>Emis</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="327"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="589"/>
        <source>Коды опознавания</source>
        <translation>Identif codes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="331"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="642"/>
        <source>Сектора запрета излучения</source>
        <translation>Emission ban sector</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="333"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="646"/>
        <source>Сект %1</source>
        <translation>Sect %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="151"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2134"/>
        <source>Режим</source>
        <translation>Mode</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="562"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2134"/>
        <source>ОО-VII</source>
        <translation>GI-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="562"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2134"/>
        <source>ИО-VII</source>
        <translation>II-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="527"/>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="562"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2134"/>
        <source>ИО-3-VII</source>
        <translation>II-3-VII</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="549"/>
        <source>Коды 41М1(6)</source>
        <translation>Codes 41M1(6)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>Автомат</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>КД</source>
        <translation>VC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="553"/>
        <source>КД, КП</source>
        <translation>VC, SC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/tabs/settings/vrl_settings_gui_tab.cpp" line="560"/>
        <source>Параметры автоматического опознавания</source>
        <translation>Auto identification parameters</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="50"/>
        <source>Дата</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="38"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/date_time_widget_gui.cpp" line="53"/>
        <source>Время</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="71"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="87"/>
        <source>РЛС: -</source>
        <translation>RDR: -</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="72"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="88"/>
        <source>ВРЛ: -</source>
        <translation>IFF: -</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="90"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="167"/>
        <source>РЛС: %1</source>
        <translation>RDR: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="122"/>
        <source>РЛС: 0</source>
        <translation>RDR: 0</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="123"/>
        <source>ВРЛ: 0</source>
        <translation>IFF: 0</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="168"/>
        <source>ВРЛ: %1</source>
        <translation>IFF: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="173"/>
        <source>АЗ: %1</source>
        <translation>AL: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="174"/>
        <source>АС: %1</source>
        <translation>AT: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="175"/>
        <source>КТ: %1</source>
        <translation>CP: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="179"/>
        <source>РЕГ: %1</source>
        <translation>LOG: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="185"/>
        <source>ИЗЛ:</source>
        <translation>EMIS:</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="193"/>
        <source>КТ%1: %2</source>
        <translation>CP%1: %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="194"/>
        <source>БАЗ: %1</source>
        <translation>ALB: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="203"/>
        <source>НзО: %1</source>
        <translation>GrO: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/info_widget_gui.cpp" line="226"/>
        <source>СЗИ: %1</source>
        <translation>EBS: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/bearing_widget_gui.cpp" line="91"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="145"/>
        <source>Расстановка</source>
        <translation>Arrangement</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="163"/>
        <source>Короткий</source>
        <translation>Short</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="164"/>
        <source>Сокращенный</source>
        <translation>Contracted</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="166"/>
        <source>Номер цели</source>
        <translation>Target number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="168"/>
        <source>Нумерация</source>
        <translation>Numbering</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="179"/>
        <source>Машинный</source>
        <translation>Electronic</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="180"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="413"/>
        <source>Единый</source>
        <translation>Unified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="182"/>
        <source>КП1</source>
        <translation>CP1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="183"/>
        <source>КП2</source>
        <translation>CP2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="186"/>
        <source>Оператор</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="187"/>
        <source>Универсальный</source>
        <translation>Universal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/bearing_widget_gui.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="189"/>
        <source>Непроз-сть</source>
        <translation>Opacity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="204"/>
        <source>След</source>
        <translation>Trail</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/gui/widget/target_widget_gui.cpp" line="209"/>
        <source>Длина</source>
        <translation>Length</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="98"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="43"/>
        <source>Рабочее место оператора</source>
        <translation>OWS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="179"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="658"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="769"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="775"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="781"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="805"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2084"/>
        <source>ИКО</source>
        <translation>PPI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="182"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="658"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="910"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="2084"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="253"/>
        <source>АФК</source>
        <translation>AFC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="622"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1372"/>
        <source>Полярные координаты</source>
        <translation>Polar coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="622"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1373"/>
        <source>Декартовы координаты</source>
        <translation>Cartesian coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="622"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1374"/>
        <source>Географические координаты</source>
        <translation>Geographic coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="629"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="912"/>
        <source>КИ</source>
        <translation>CI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="632"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="944"/>
        <source>ПЕЛ</source>
        <translation>BEAR</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="634"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="953"/>
        <source>Луч</source>
        <translation>Beam</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="642"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1400"/>
        <source>Сетка АД</source>
        <translation>AD grid</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="642"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1416"/>
        <source>Сектора обзора</source>
        <translation>Sweep spectrum</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="642"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1417"/>
        <source>Рубежи</source>
        <translation>Mission lines</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="642"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1418"/>
        <source>Зоны отбора</source>
        <translation>Selection zones</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="645"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1425"/>
        <source>Сектора ЗИ ВРЛ</source>
        <translation>SR EMB sectors</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="647"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1428"/>
        <source>Точки падения БЦ</source>
        <translation>BM impact point</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="654"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="975"/>
        <source>Селекция</source>
        <translation>Selection</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="668"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="857"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1003"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1583"/>
        <source>КП: ?</source>
        <translation>CC: ?</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="676"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="989"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1781"/>
        <source>МВ</source>
        <translation>PT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="678"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1781"/>
        <source>ВВ</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="681"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1062"/>
        <source>ТЗИ</source>
        <translation>TMI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="691"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1137"/>
        <source>В центр</source>
        <translation>To centre</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="695"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="916"/>
        <source>В/Ч</source>
        <translation>M/U</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="725"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1187"/>
        <source>По номерам</source>
        <translation>Number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="725"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1201"/>
        <source>По скорости</source>
        <translation>Velocity</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="725"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1223"/>
        <source>По высоте</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="725"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1259"/>
        <source>По классу</source>
        <translation>Class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="727"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1273"/>
        <source>По ОГП</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="727"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1283"/>
        <source>По источникам</source>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="733"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="981"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="255"/>
        <source>ПРК</source>
        <translation>RCS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="737"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1034"/>
        <source>2 секунды</source>
        <translation>2 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="737"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1034"/>
        <source>5 секунд</source>
        <translation>5 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="737"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1034"/>
        <source>10 секунд</source>
        <translation>10 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="737"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1035"/>
        <source>30 секунд</source>
        <translation>30 seconds</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="737"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1035"/>
        <source>1 минута</source>
        <translation>1 minute</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="738"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1035"/>
        <source>5 минут</source>
        <translation>5 minutes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="747"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1087"/>
        <source>Воспр.инф</source>
        <translation>Data disp</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1127"/>
        <source>9999.9 кфт</source>
        <translation>9999.9 kft</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1370"/>
        <source>-1200.99 км, -1200.99 км</source>
        <translation>-1200.99 km, -1200.99 km</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1376"/>
        <source>Координаты сетки ПВО</source>
        <translation>AD grid coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1461"/>
        <source>Настройка</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1951"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1968"/>
        <source>Менее </source>
        <translation>Less </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1962"/>
        <location filename="../apps/resonance/indicator/src/main_window.cpp" line="1985"/>
        <source>Более </source>
        <translation>More </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/explorer_jamming_monitoring.cpp" line="28"/>
        <location filename="../apps/resonance/indicator/src/monitoring/jamming/explorer_jamming_monitoring.cpp" line="47"/>
        <source>Диаграмма шумов в секторе %1</source>
        <translation>Sector %1 jamming diagram</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/service/frequency_service.cpp" line="18"/>
        <source>Разнос частот в секторе %1 меньше 400 кГц</source>
        <translation>Frequency spacing in sector %1 is less than 500 kHz</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/service/frequency_service.cpp" line="26"/>
        <source>Частоты в секторе %1 не принадлежат одному ПЭП</source>
        <translation>Frequencies in sector %1 not belong to one ETS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/service/frequency_service.cpp" line="38"/>
        <source>Частоты в сеторах %1 и %2 совпадают</source>
        <translatorcomment>Зимка</translatorcomment>
        <translation>Frequencies in sectors %1 and %2 are equal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="34"/>
        <source>ftp отключен</source>
        <translation>ftp disconnected</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="43"/>
        <source>ftp подключение к &apos;%1&apos;...</source>
        <translation>ftp connecting to &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="45"/>
        <source>ftp авторизация...</source>
        <translation>ftp logging in...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="47"/>
        <source>ftp переход в каталог &apos;%1&apos;...</source>
        <translation>ftp changing directory to &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="49"/>
        <source>ftp листинг &apos;%1&apos;...</source>
        <translation>ftp listing &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="51"/>
        <source>ftp удаление &apos;%1&apos;...</source>
        <translation>ftp removing &apos;%1&apos;...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="54"/>
        <source>ftp скачивание &apos;%1&apos; --&gt; &apos;%2&apos;...</source>
        <translation>ftp downloading &apos;%1&apos; --&gt; &apos;%2&apos;...</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="70"/>
        <source>ftp ошибка подключения</source>
        <translation>ftp connection error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="73"/>
        <source>ftp подключение установлено</source>
        <translation>ftp connected</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="76"/>
        <source>ftp ошибка авторизации</source>
        <translation>ftp logging in error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="79"/>
        <source>ftp авторизация завершена</source>
        <translation>ftp logged in</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="82"/>
        <source>ftp ошибка перехода в каталог</source>
        <translation>ftp changing directory error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="85"/>
        <source>ftp переход в каталог завершен</source>
        <translation>ftp directory changed</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="87"/>
        <source>ftp ошибка листинга</source>
        <translation>ftp listing error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="87"/>
        <source>ftp листинг завершен</source>
        <translation>ftp listed</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="101"/>
        <source>ftp ошибка удаления</source>
        <translation>ftp removing error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="101"/>
        <source>ftp удаление завершено</source>
        <translation>ftp removed</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="108"/>
        <source>ftp ошибка скачивания</source>
        <translation>ftp downloading error</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/native_ftp_file_server_tool.cpp" line="108"/>
        <source>ftp скачивание завершено</source>
        <translation>ftp downloaded</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/logging/process_ftp_file_server_tool.cpp" line="116"/>
        <source>ошибка ftp: %1</source>
        <translation>ftp error: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="57"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="101"/>
        <source>Координаты курсора</source>
        <translation>Cursor coords</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="59"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="114"/>
        <source>Полярный</source>
        <translation>Polar</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="60"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="115"/>
        <source>Прямоугольный</source>
        <translation>Cartesian</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="61"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="107"/>
        <source>Измерить расстояние</source>
        <translation>Measure distance</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/area_context_menu_manager_tool.cpp" line="128"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="438"/>
        <source>Распоряжение ТР</source>
        <translation>Call request</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="65"/>
        <source>Редактировать</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/ground_object_context_menu_manager_tools.cpp" line="99"/>
        <source>Координаты</source>
        <translation>Coordinates</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="156"/>
        <source>Единый номер занят</source>
        <translation>U number busy</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="162"/>
        <source>(Р)</source>
        <translation>(M)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="164"/>
        <source>Номер оператора занят</source>
        <translation>Operator`s number busy</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="268"/>
        <source>Добавить в ТЗИ</source>
        <translation>Add to TMI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="269"/>
        <source>Удалить из ТЗИ</source>
        <translation>Delete from TMI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="308"/>
        <source>Полный формуляр</source>
        <translation>Full RC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="319"/>
        <source>Назначить класс цели</source>
        <translation>Assigh target class</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="217"/>
        <source>Самолет</source>
        <translation>Aircraft</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="219"/>
        <source>Вертолет</source>
        <translation>Helicopter</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="221"/>
        <source>Крылатая ракета</source>
        <translation>Cruise missile</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="322"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="223"/>
        <source>Гиперзвуковая крылатая ракета</source>
        <translation>Hypersonic cruise missile</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="225"/>
        <source>Аэростат</source>
        <translation>Balloon</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="323"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="227"/>
        <source>Баллистическая цель</source>
        <translation>Ballistic target</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="341"/>
        <source>Передача на КП</source>
        <translation>Delivery to CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="376"/>
        <source>Траектория</source>
        <translation>Trajectory</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="397"/>
        <source>Назначить номер</source>
        <translation>Assign number</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="407"/>
        <source>Оператора</source>
        <translation>Operator</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="459"/>
        <source>Сбросить</source>
        <translation>Cancel tracking</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="477"/>
        <source>Разъединить</source>
        <translation>Separate</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="482"/>
        <source>Объединить</source>
        <translation>Unify</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="512"/>
        <source>Назначить госпринадлежность</source>
        <translation>Assign IFF attribute</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="516"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="316"/>
        <source>Свой ИО</source>
        <translation>Friendly II</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="516"/>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="518"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="310"/>
        <source>Свой ОО</source>
        <translation>Friend GI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="516"/>
        <source>Свой RBS</source>
        <translation>Friend RBS</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="518"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="313"/>
        <source>Свой ГО</source>
        <translation>Friend AI</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="520"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="321"/>
        <source>Чужой</source>
        <translation>Foe</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/menu/target_context_menu_manager_tool.cpp" line="533"/>
        <source>Уточнить ГП</source>
        <translation>IFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="44"/>
        <source>Версия сборки: %1</source>
        <translation>Assembly version:%1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/shortcut_manager_tool.cpp" line="57"/>
        <source>Доступные щрифты</source>
        <translation>Available prints</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/manager/usb_manager_tool.cpp" line="57"/>
        <source>Отменено пользователем</source>
        <translation>Canceled by user</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="54"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="56"/>
        <source>МУ</source>
        <translation>LC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="54"/>
        <source>ЦУ</source>
        <translation>CC</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="56"/>
        <source>ВКЛ</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="56"/>
        <source>ВЫКЛ</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="58"/>
        <source>АНТ</source>
        <translation>ANT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="58"/>
        <source>ЭКВ</source>
        <translation>EQV</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="60"/>
        <source>ПЭП1</source>
        <translation>ETS1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="60"/>
        <source>ПЭП2</source>
        <translation>ETS2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="62"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="62"/>
        <source>180</source>
        <translation>180</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="72"/>
        <source>%1(КП)</source>
        <translation>%1(SC)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="74"/>
        <source>%1(Р)</source>
        <translation>%1(M)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="88"/>
        <source>%1 (%2 дБ)</source>
        <translation>%1 (%2 dB)</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="129"/>
        <source>Произведено включение РЛС
Проверьте время</source>
        <translation>RDR on Check time</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="136"/>
        <source>В РЭУ 5Ц3-Е сектор %1 температура не в норме</source>
        <translation>T not normal in RED 5Ц3-E sector %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="142"/>
        <source>В РЭК 1 температура не в норме</source>
        <translation>T not normal in REC 1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="144"/>
        <source>ЛИРА температура не в норме</source>
        <translation>Lira temp. not normal</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="149"/>
        <source>Ошибка авторизации: &apos;пользователь не существует&apos;</source>
        <translation>Authorization error:user does not exist</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="151"/>
        <source>Ошибка авторизации: &apos;неверный пароль&apos;</source>
        <translation>Authorization error: &quot;wrong password&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="156"/>
        <source>Вы вошли как &apos;гость&apos;</source>
        <translation>You logged as &quot;guest&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="157"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="161"/>
        <source>Вы вошли как &apos;%1&apos;</source>
        <translation>You logged as %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="159"/>
        <source>Вы вошли как &apos;командир&apos;</source>
        <translation>You logged as &quot;commander&quot;</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="176"/>
        <source>включено</source>
        <translation>ON</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="168"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="176"/>
        <source>выключено</source>
        <translation>OFF</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="169"/>
        <source>КП%1: излучение РЛС %2</source>
        <translation>CC%1: RDR emission is %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="177"/>
        <source>КП%1: излучение ВРЛ %2</source>
        <translation>CC%1: IFF emission is %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="185"/>
        <source>КП%1: включен режим %2</source>
        <translation>CP%1: mode %2 is set</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="192"/>
        <source>КП%1: Сброс признаков целей</source>
        <translation>CC%1: Cancel target attributes</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="199"/>
        <source>КП%1: Запрос технического состояния</source>
        <translation>CC%1: Technical state request</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="203"/>
        <source>Тревога</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="210"/>
        <source>Приемник %1 сектора %2</source>
        <translation>Receiver %1 sector %2</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="215"/>
        <source>Не распознан</source>
        <translation>Unidentified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="217"/>
        <source>C</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="219"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="313"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="318"/>
        <source>В</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="221"/>
        <source>КР</source>
        <translation>CM</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="223"/>
        <source>ГЗ</source>
        <translation>HCM</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="225"/>
        <source>А</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="227"/>
        <source>БЦ</source>
        <translation>BT</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="232"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="264"/>
        <source>КП: </source>
        <translation>SC: </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="256"/>
        <source>РЕЖ: %1</source>
        <translation>MODE: %1</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="262"/>
        <source>КП%1: </source>
        <translation>CC%1: </translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="300"/>
        <source>КД используются</source>
        <translation>CC is in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="300"/>
        <source>КД не используются</source>
        <translation>CC is not in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="301"/>
        <source>КП используются</source>
        <translation>PC is in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="301"/>
        <source>КП не используются</source>
        <translation>PC is not in use</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="308"/>
        <source>Не определен</source>
        <translation>Unidentified</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="308"/>
        <source>Х</source>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="310"/>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="316"/>
        <source>П</source>
        <translation>П</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="318"/>
        <source>Гражданский</source>
        <translation>Civil</translation>
    </message>
    <message>
        <location filename="../apps/resonance/indicator/src/tools/translator_tools.cpp" line="321"/>
        <source>Ч</source>
        <translation>Ч</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="147"/>
        <source> с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="416"/>
        <source>с</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="425"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="484"/>
        <source>фт</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="427"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="488"/>
        <source>кфт</source>
        <translation>kft</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="429"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="490"/>
        <source>км</source>
        <translation>km</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="431"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="492"/>
        <source>м</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="433"/>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="494"/>
        <source>ММ</source>
        <translation>MM</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="463"/>
        <source>фт/ч</source>
        <translation>ft/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="465"/>
        <source>фт/с</source>
        <translation>ft/s</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="467"/>
        <source>км/ч</source>
        <translation>km/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="469"/>
        <source>м/с</source>
        <translation>m/s</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="471"/>
        <source>ММ/ч</source>
        <translation>MM/h</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/data_format/data_format.cpp" line="486"/>
        <source>гм</source>
        <translation>hm</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="114"/>
        <source>Библиотека</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Glass</name>
    <message>
        <location filename="../libs/gui/src/glass.cpp" line="13"/>
        <location filename="../libs/gui/src/glass.cpp" line="15"/>
        <source>Отмена</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../libs/gui/src/glass.cpp" line="37"/>
        <source>Обработка...</source>
        <translation>Procession...</translation>
    </message>
</context>
<context>
    <name>GuiDisabler</name>
    <message>
        <location filename="../libs/gui/src/gui_disabler.cpp" line="35"/>
        <location filename="../libs/gui/src/gui_disabler.cpp" line="39"/>
        <source>Внимание</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools::Filter::Widget</name>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="22"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="128"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="133"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="137"/>
        <source>Предупреждение</source>
        <translation type="unfinished">Warning</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="22"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="133"/>
        <source>Фильтр некорректный</source>
        <translation type="unfinished">Incorrect filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="26"/>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="137"/>
        <source>Фильтр уже существует</source>
        <translation type="unfinished">Filter already exists</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="49"/>
        <source>Выражение</source>
        <translation type="unfinished">Expression</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="55"/>
        <source>Все И</source>
        <translation type="unfinished">All and</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="59"/>
        <source>Все ИЛИ</source>
        <translation type="unfinished">All or</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="73"/>
        <source>Поле</source>
        <translation type="unfinished">Field</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="77"/>
        <source>Оператор</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="85"/>
        <source>Значение</source>
        <translation type="unfinished">Value</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="94"/>
        <source>Добавить фильтр</source>
        <translation type="unfinished">Add filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="98"/>
        <source>Обновить фильтр</source>
        <translation type="unfinished">Update filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="102"/>
        <source>Удалить фильтр</source>
        <translation type="unfinished">Delete filter</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="106"/>
        <source>Clear</source>
        <translation type="unfinished">Clear</translation>
    </message>
    <message>
        <location filename="../libs/tools/src/filter/widget_filter_tools.cpp" line="128"/>
        <source>Фильтр не выбран</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InterfaceController</name>
    <message>
        <location filename="../libs/communication/src/controller/interface_controller.cpp" line="101"/>
        <source>получено %1 %2, отправлено %3 %4</source>
        <translation>received %1 %2, transmitted %3 %4</translation>
    </message>
</context>
<context>
    <name>InterfaceIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="91"/>
        <source>Мб</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="94"/>
        <source>Кб</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/interface_indicator.cpp" line="96"/>
        <source>байт</source>
        <translation>B</translation>
    </message>
</context>
<context>
    <name>TcpController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="43"/>
        <source>подключение к %1:%2...</source>
        <translation>connection to %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="57"/>
        <source>соединение с %1:%2 не установлено</source>
        <translation>connection to %1:%2 not established</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="61"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connection to %1:%2 broken</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="66"/>
        <source> по ошибке &apos;%1&apos;</source>
        <translation>by mistake %1</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_controller.cpp" line="74"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connection to %1:%2 OK</translation>
    </message>
</context>
<context>
    <name>TcpIndicator</name>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="23"/>
        <source>Удаленный хост</source>
        <translation>Remote host</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/indicator/tcp_indicator.cpp" line="32"/>
        <source>Удаленный порт</source>
        <translation>Remote port</translation>
    </message>
</context>
<context>
    <name>TcpServerController</name>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="34"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="84"/>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="103"/>
        <source>ожидание соединения по %1:%2...</source>
        <translation>connection waiting via %1:%2...</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="50"/>
        <source>соединение с %1:%2 установлено</source>
        <translation>connection to %1:%2 OK</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/tcp_server_controller.cpp" line="73"/>
        <source>соединение с %1:%2 разорвано</source>
        <translation>connectionh to %1:%2 broken </translation>
    </message>
</context>
<context>
    <name>UdpController</name>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="27"/>
        <source>привязка к %1:%2 выполнена</source>
        <translation>binding to %1:%2 performed</translation>
    </message>
    <message>
        <location filename="../libs/communication/src/controller/udp_controller.cpp" line="29"/>
        <source>ошибка привязки к %1:%2 - %3</source>
        <translation>binding error to%1:%2 - %3 </translation>
    </message>
</context>
</TS>
